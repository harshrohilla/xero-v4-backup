/**
 * @File Name          : XR_MockHTTPResponseGenerator.cls
 * @Description        :
 * @Author             : Vanguard
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    06/01/2020      Vanguard                Initial Version
 */
@isTest
public class XR_MockHTTPResponseGenerator implements HttpCalloutMock
{
	protected String bodyAsString;
	protected Integer code;
	/**
	 * VG_MockHttpResponseGenerator initialize response code and body for testing callouts
	 * @param                        code
	 * @param                        body
	 * @return                       return null
	 */
	public XR_MockHTTPResponseGenerator(Integer code, String bodyAsString){
		this.code = code;
		this.bodyAsString = bodyAsString;
	}
	/**
	 * respond     get HTTP response based on request
	 * @param      request
	 * @return     return mock response
	 */
	public HTTPResponse respond(HTTPRequest req) {
		HttpResponse response = new HttpResponse();
		response.setHeader('Content-Type', 'application/json');
		response.setBody(bodyAsString);
		response.setStatusCode(code);
		return response;
	}
}