/**
 * When an Opportunity is Pushed to Xero, this class is called by the button
 */
public with sharing class XR_InvoiceController
{

	XR_InvoiceHandler handler;
	List<Opportunity> oppList {get;set;}
	/**
	 * XR_InvoiceController  This constructor takes single Opportunity id and sync it to xero
	                      The Opportunity Id is fetched when push to xero button is clicked from Opportunity record detail page
	 * @param             takes single record id
	 * @return            returns void but initializes that record id in opportunityList
	 */
	public XR_InvoiceController(ApexPages.StandardController controller){
		String oppId = ApexPages.CurrentPage().getparameters().get('id');
		init(new Set<Id> {oppId});
	}
	/**
	 * XR_InvoiceController This constructor takes set of Opportunity ids from list view and initializes them into oppList
	                      The opp Ids are fetched when Push To Xero button is clicked from opp list view with multiple opportunities selected
	 * @param             Takes set of opportunity ids
	 * @return            return void but initializes the opportunity records selected in opportunityList
	 */
	public XR_InvoiceController(ApexPages.StandardSetController controller) {
		Set<Id> oppIdSet = new Set<Id>();
		for (Opportunity opp : (List<Opportunity>)controller.getSelected()) {
			oppIdSet.add(opp.Id);
		}
		init(oppIdSet); 
	}
	/**
	 * XR_InvoiceController Takes oppIds from invocable apex (XR_InvoiceAutoSyncViaProcess) and synce them to xero
	 * @param  oppId Set of Ids
	 * @return       returns nothing, just initializes the Ids and send them to InvoiceHandler for Syncing to Xero
	 */
	public XR_InvoiceController (Set<Id> oppId){
		//to sanitize 
		Set<Id> oppIdSet = new Set<Id>(oppId);
		init(oppIdSet);
	}
	/**
	 * init    Takes set of Opportunity ids from constructor, perform query and stores the records in opportunityList to be synced
	 * @param  takes set of Opportunity ids
	 */
	private void init(Set<Id> oppIds){
		this.oppList = [SELECT
		                Id,
		                AccountId,
		                Account.Name,
		                Account.Xero_Id__c,
		                Xero_Account_Id__c,
		                Xero_Expected_Payment_Date__c,
		                Xero_Fully_Paid_On_Date__c,
		                Xero_Id__c,
		                Xero_Invoice_Number__c,
		                Xero_Invoice_Status__c,
		                Xero_Invoice_Type__c,
		                Xero_Invoice_URL__c,
		                Xero_Last_Sync_Date__c,
		                Xero_Planned_Payment_Date__c,
		                Xero_Sync_Details__c,
		                Xero_Sync_Status__c,
		                HasOpportunityLineItem,
		                CloseDate,
		                Amount,
		                Invoice_Due_Date__c,
				(
					SELECT
					Description,
					Id,
					Quantity,
					UnitPrice,
					Xero_Account_Code__c,
					Xero_Id__c,
					Xero_Tax_Amount__c,
					Xero_Tax_Type__c,
					Discount,
					SubTotal,
					TotalPrice,
					Name
					FROM OpportunityLineItems
		                )
		                FROM Opportunity
		                WHERE Id IN: oppIds];
		handler = new XR_InvoiceHandler();
	}
	public PageReference doPush(){
		handler.doPush(this.oppList);

		//if sync took place from detail page, redirect back to that detail page
		if(oppList.size() == 1) {
			//pagereference will take place from vfPage, refreshing the record
			return new PageReference('/' + oppList[0].Id);
			//return null;
		}
		//if sync took place from list view button, redirect back to invoice object
		else{
			return new PageReference('/' + Opportunity.sObjectType.getDescribe().getKeyPrefix());
		}
	}
}