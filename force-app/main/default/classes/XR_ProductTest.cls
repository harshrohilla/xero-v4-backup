@isTest
public with sharing class XR_ProductTest
{
	@TestSetup
	static void makeData(){

		//insert a single product to be used in testMethods
		Product2 product = DataFactoryTest.getProduct('Test Product','XERO010',1)[0];
		product.StockKeepingUnit = 'XERO010';
		product.Xero_Is_Sold__c = True;
		insert product;

		//insert a single product with incomplete details
		Product2 incompleteProduct = DataFactoryTest.getProduct('Fail Product','XEROF010',1)[0];
		//Price List Item Code must be supplied for successful sync
		incompleteProduct.StockKeepingUnit = 'XEROF010';
		//IsSold cannot be set to false when SalesDetails or Description values are provided.
		incompleteProduct.Xero_Is_Sold__c = False;
		insert incompleteProduct;

		//create standard pricebook
		Pricebook2 pricebook = new Pricebook2(Name = 'Standard', isActive = True);
		insert pricebook;

		//create custom pricebook
		Pricebook2 customPricebook = new Pricebook2(Name = 'Custom', isActive = True);
		insert customPricebook;

		//user for runAs
		User user = DataFactoryTest.getUsers(1,null)[0];

		//fill customSetting with XERO creds and some default settings
		Xero_Settings__c cs = new Xero_Settings__c();
		cs.Webhook_Id__c = 'QjPGOyzdFl9ecVUSb1Vyxs1gKHu+ET+rSMFzzyOPzGfOCcgGdtdGMhXl5npjk/GRlDB5Q1Wfatvur4LOh1mIbw==';
		cs.Sync_Price__c = False;
		cs.SetupOwnerId = UserInfo.getOrganizationId();
		insert cs;
	}
	/**
	 * pushSingleProduct a single product is pushed and updated values are asserted
	 */
	static testMethod void pushSingleProduct(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			Id pricebookId = Test.getStandardPricebookId();
			Pricebook2 customPricebook = [SELECT Id,Name FROM Pricebook2 WHERE isStandard = False LIMIT 1];
			Product2 product = [SELECT Id, Name, Xero_Id__c, Xero_Sync_Details__c, Xero_Sync_Status__c FROM Product2 WHERE Name =: 'Test Product'];

			PricebookEntry pricebookEntry = new PricebookEntry(Pricebook2Id = pricebookId,
			                                                   Product2Id = product.Id,
			                                                   UnitPrice = 988,
			                                                   IsActive = true);
			Insert pricebookEntry;
			PricebookEntry cpricebookEntry = new PricebookEntry(Pricebook2Id = customPricebook.id,
			                                                    Product2Id = product.Id,
			                                                    UnitPrice = 928,
			                                                    IsActive = true);
			Insert cpricebookEntry;
			//before sync
			System.assertEquals(null, product.Xero_Id__c, 'Initial Xero Id Sync incorrect');
			System.assertEquals(null, product.Xero_Sync_Status__c, 'Initial Sync Status incorrect');

			Test.startTest();
			//for mock response
			XR_MockDataProductTest.mockDataPushSingleProduct();
			//provide the productId to controller in order to start SYNC
			PageReference pageRef = Page.XR_ProductDetailPageButton;
			//set page
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id',product.id);
			ApexPages.StandardController stController = new ApexPages.StandardController(product);
			XR_ProductController controller = new XR_ProductController(stController);
			//push the product
			controller.doPush();
			Test.stopTest();

			Product2 updatedProduct = [SELECT Id, Name, Xero_Id__c, Xero_Sync_Details__c, Xero_Sync_Status__c FROM Product2 WHERE Name =: 'Test Product'];
			System.debug('updatedProduct'+updatedProduct);
			//after sync
			System.assertEquals('38ad86e5-2896-4b2b-9b84-74bec1495ea1', updatedProduct.Xero_Id__c, 'Xero Id Sync failed');
			System.assertEquals('Success', updatedProduct.Xero_Sync_Status__c, 'Sync Status failed to update');
		}
	}
	/**
	 * pushMultiProducts push 5 products at once, imitating sync from list view
	 */
	static testMethod void pushMultiProducts(){
		//multiple products
		List<Product2> prodList = DataFactoryTest.getProduct('Bulk Products','XERO ',5);
		insert prodList;
		Id pricebookId = Test.getStandardPricebookId();
		Pricebook2 customPricebook = [SELECT Id,Name FROM Pricebook2 WHERE isStandard = False LIMIT 1];
		List<PricebookEntry> prices = new List<PricebookEntry>();
		//add standard pricebook to products
		for(Product2 product : prodList) {
			PricebookEntry pricebookEntry = new PricebookEntry(Pricebook2Id = pricebookId,
			                                                   Product2Id = product.Id,
			                                                   UnitPrice = 988,
			                                                   IsActive = true);
			prices.add(pricebookEntry);
		}
		insert prices;
		List<PricebookEntry> cPrices = new List<PricebookEntry>();
		//add custom pricebook to products
		for(Product2 product : prodList) {
			PricebookEntry pricebookEntry = new PricebookEntry(Pricebook2Id = customPricebook.Id,
			                                                   Product2Id = product.Id,
			                                                   UnitPrice = 988,
			                                                   IsActive = true);
			cPrices.add(pricebookEntry);
		}
		insert cPrices;

		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){

			Test.startTest();
			//mock data
			XR_MockDataProductTest.mockDataPushMultipleProducts();

			PageReference pageRef = Page.XR_ProductListViewButton;
			Test.setCurrentPage(pageRef);

			ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(prodList);
			stdSetController.setSelected(prodList);
			XR_ProductController ext = new XR_ProductController(stdSetController);
			//push products
			ext.doPush();

			Test.stopTest();
			//fetch updated products
			List<Product2> updatedProducts = [SELECT Id, Name, Xero_Id__c, Xero_Sync_Details__c, Xero_Sync_Status__c FROM Product2 WHERE Name =: 'Bulk Products' LIMIT 5];
			//assert Xero Ids
			System.assertEquals('e1473f97-82c6-430b-882c-929b06306e9a', updatedProducts[0].Xero_Id__c, 'Multiple Products failed to Sync');
			System.assertEquals('dba3f955-4ebd-4fce-98f7-a757e6579e3e', updatedProducts[1].Xero_Id__c, 'Multiple Products failed to Sync');
			System.assertEquals('b7d1935d-2dd3-4ffb-b5cd-16b0a53b85a6', updatedProducts[2].Xero_Id__c, 'Multiple Products failed to Sync');
			System.assertEquals('8a706480-9ec5-467d-a45b-17d0c3090309', updatedProducts[3].Xero_Id__c, 'Multiple Products failed to Sync');
			System.assertEquals('1e04eab4-c0ad-4cda-b03d-21d9498abf44', updatedProducts[4].Xero_Id__c, 'Multiple Products failed to Sync');
			//assert Sync Status
			System.assertEquals('Success', updatedProducts[0].Xero_Sync_Status__c, 'Sync Status incorrect');
			System.assertEquals('Success', updatedProducts[1].Xero_Sync_Status__c, 'Sync Status incorrect');
			System.assertEquals('Success', updatedProducts[2].Xero_Sync_Status__c, 'Sync Status incorrect');
			System.assertEquals('Success', updatedProducts[3].Xero_Sync_Status__c, 'Sync Status incorrect');
			System.assertEquals('Success', updatedProducts[4].Xero_Sync_Status__c, 'Sync Status incorrect');
		}
	}
	/**
	 * pushProductWithError when a product is synced with incomplete details, it should fail to sync
	 *                      how that failure is handled is tested in this method
	 */
	static testMethod void pushProductWithError(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			Id pricebookId = Test.getStandardPricebookId();
			Pricebook2 customPricebook = [SELECT Id,Name FROM Pricebook2 WHERE isStandard = False LIMIT 1];
			Product2 product = [SELECT Id, Name, Xero_Id__c, Xero_Sync_Details__c, Xero_Sync_Status__c FROM Product2 WHERE Name =: 'Fail Product'];

			PricebookEntry pricebookEntry = new PricebookEntry(Pricebook2Id = pricebookId,
			                                                   Product2Id = product.Id,
			                                                   UnitPrice = 988,
			                                                   IsActive = true);
			Insert pricebookEntry;
			PricebookEntry cpricebookEntry = new PricebookEntry(Pricebook2Id = customPricebook.id,
			                                                    Product2Id = product.Id,
			                                                    UnitPrice = 928,
			                                                    IsActive = true);
			Insert cpricebookEntry;
			//before sync
			System.assertEquals(null, product.Xero_Id__c, 'Initial Xero Id Sync incorrect');
			System.assertEquals(null, product.Xero_Sync_Status__c, 'Initial Sync Status incorrect');

			test.startTest();
			//mock data
			XR_MockDataProductTest.mockDataPushFailedProduct();

			PageReference pageRef = Page.XR_ProductDetailPageButton;
			//set page
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id',product.id);
			ApexPages.StandardController stController = new ApexPages.StandardController(product);
			XR_ProductController controller = new XR_ProductController(stController);
			//push the product
			controller.doPush();
			test.stopTest();
			//assert product updated with error details
			Product2 updatedProduct = [SELECT Id, Name, Xero_Id__c, Xero_Sync_Details__c, Xero_Sync_Status__c FROM Product2 WHERE Name =: 'Fail Product'];
			System.assertEquals('Price List Item Code must be supplied'+'\n'+'IsSold cannot be set to false when SalesDetails or Description values are provided.',updatedProduct.Xero_Sync_Details__c,'Error Details incorrect');
			System.assertEquals('Failed',updatedProduct.Xero_Sync_Status__c,'Sync Status incorrect');
		}
	}
}