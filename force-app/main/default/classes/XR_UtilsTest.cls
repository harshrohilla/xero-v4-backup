@isTest
/**
 * Test Class for Xero Utility Methods
 */ 
public with sharing class XR_UtilsTest
{
    /**
     * testDateMethodSuccess test date conversion
     */
	static testMethod void testDateMethodSuccess(){

        Test.startTest();
        
        Date resultDate = XR_Utils.getDate('2009-05-27T00:00:00');
        
        Test.stopTest();
        
        System.assertEquals(resultDate, Date.newInstance(2009, 05, 27), 'date conversion failed');
        System.assertNotEquals(resultDate, Date.newInstance(2009, 27, 05), 'date conversion failed');
    }/**
     * testDateMethodFail test date conversion error handling mechanism
     */ 
    static testMethod void testDateMethodFail(){

        Test.startTest();
        
        Date resultDate = XR_Utils.getDate('some-random-T00:00:00');
        
        Test.stopTest();
        
		System.assertEquals(resultDate, null, 'date conversion failed');
    }
    /**
     * testBooleanMethodFalse test null to boolean conversion logic
     */
    static testMethod void testBooleanMethodFalse(){

        Test.startTest();
        
        Boolean result = XR_Utils.getBoolean(null);
        
        Test.stopTest();
        
		System.assertEquals(result, False, 'boolean conversion failed');
    }
    /**
     * testBooleanMethodTrue test how null to boolean conversion logic handles a boolean input 
     */ 
    static testMethod void testBooleanMethodTrue(){

        Test.startTest();
        
        Boolean result = XR_Utils.getBoolean(True);
        
        Test.stopTest();
        
		System.assertEquals(result, True, 'boolean conversion failed');
	}

}
