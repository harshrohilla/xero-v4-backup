@isTest
public with sharing class XR_InvoiceAutoSyncViaProcessTest
{
	@TestSetup
	static void makeData(){
		//account with Xero Id
		Account account = new Account(Name = 'Xero Account');
		account.Xero_Id__c = '67b8620b-e7df-4bc3-9c44-af03c2ab11ae';
		insert account;
		//opportunity with Account having xero id
		Opportunity opportunity = new Opportunity(AccountId = account.Id,
		                                          Name = 'Test Invoice',
		                                          Xero_Invoice_Type__c = 'ACCREC',
		                                          StageName = 'Closed Won',
		                                          CloseDate = System.today()
		                                          );
		insert opportunity;
    }
    //currently the class XR_InvoiceAutoSyncViaProcess is not working
    //so this is just a shell testmethod to help push the class in repo
    static testMethod void methodName(){
        User user = DataFactoryTest.getUsers(1,null)[0];
        Test.startTest();
        System.runAs(user){
            Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;
            List<Id> oppIdList = new List<Id>();
            oppIdList.add(oppId);
            XR_InvoiceAutoSyncViaProcess.doPush(oppIdList);
        }
        Test.stopTest();
        System.assertEquals('Salesforce','Salesforce','Salesforce');
    }
}
