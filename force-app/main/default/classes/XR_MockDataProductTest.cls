@isTest
public with sharing class XR_MockDataProductTest
{
	@testVisible
	static void mockDataPushSingleProduct(){
		//for connection to xero
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,connectionJSON());
		//for single product sync
		XR_MockHTTPResponseGenerator productMock = new XR_MockHTTPResponseGenerator(200,singleProductPush());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();

		testRespMap.put('callout:Xero/connections',connectionMock);

		testRespMap.put('callout:Xero/api.xro/2.0/Items',productMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	@testVisible
	static void mockDataPushMultipleProducts(){
		//for connection to xero
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,connectionJSON());
		//for multiple product sync
		XR_MockHTTPResponseGenerator productMock = new XR_MockHTTPResponseGenerator(200,pushMultipleProducts());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();

		testRespMap.put('callout:Xero/connections',connectionMock);

		testRespMap.put('callout:Xero/api.xro/2.0/Items',productMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	@testVisible
	static void mockDataPushFailedProduct(){
		//for connection to xero
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,connectionJSON());
		//for single incomplete product sync
		XR_MockHTTPResponseGenerator productMock = new XR_MockHTTPResponseGenerator(200,failedSyncResponse());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();

		testRespMap.put('callout:Xero/connections',connectionMock);

		testRespMap.put('callout:Xero/api.xro/2.0/Items',productMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	/**
	 * getConnectionJSON mock Connection response from Xero
	 * @return   return String of JSON containing tenantId
	 */
	@testVisible
	static String connectionJSON(){
		return '[' +
		       '{' +
		       '"id": "0316baa0-c4ce-43e9-832f-428bcf65f4eb",' +
		       '"tenantId": "6d683406-923d-4015-85e0-546106b85db6",' +
		       '"tenantType": "ORGANISATION",' +
		       '"createdDateUtc": "2019-12-18T08:34:30.2848460",' +
		       '"updatedDateUtc": "2019-12-18T08:35:46.1741210"' +
		       '}' +
		       ']';
	}

	@testVisible
	/**
	 * singleProductPush mock json body for response when single product is pushed
	 * @return   return description
	 */
	static String singleProductPush(){
		return '{'+
		       '  "Id": "8f75e7d9-2232-4877-94aa-d7a3f99fab4e",'+
		       '  "Status": "OK",'+
		       '  "ProviderName": "Salesforce",'+
		       '  "DateTimeUTC": "Date(1586771094351)",'+
		       '  "Items": ['+
		       '    {'+
		       '      "ItemID": "38ad86e5-2896-4b2b-9b84-74bec1495ea1",'+
		       '      "Code": "XERO010",'+
		       '      "UpdatedDateUTC": "Date(1586771094429)",'+
		       '      "PurchaseDetails": {},'+
		       '      "SalesDetails": {'+
		       '        "UnitPrice": 0.0000,'+
		       '        "AccountCode": "200",'+
		       '        "TaxType": "OUTPUT"'+
		       '      },'+
		       '      "Name": "Test Product",'+
		       '      "IsTrackedAsInventory": false,'+
		       '      "IsSold": true,'+
		       '      "IsPurchased": false,'+
		       '      "ValidationErrors": []'+
		       '    }'+
		       '  ]'+
		       '}';
	}
	@testVisible
	static String pushMultipleProducts(){
		return '{'+
		       '  "Id": "7d6952f4-a7ff-44c8-b9c3-4d200a779142",'+
		       '  "Status": "OK",'+
		       '  "ProviderName": "Salesforce",'+
		       '  "DateTimeUTC": "Date(1586780871761)",'+
		       '  "Items": ['+
		       '    {'+
		       '      "ItemID": "e1473f97-82c6-430b-882c-929b06306e9a",'+
		       '      "Code": "XERO0",'+
		       '      "Description": "dddd",'+
		       '      "UpdatedDateUTC": "Date(1586780871967)",'+
		       '      "PurchaseDetails": {},'+
		       '      "SalesDetails": {'+
		       '        "UnitPrice": 0.0000,'+
		       '        "AccountCode": "200",'+
		       '        "TaxType": "NONE"'+
		       '      },'+
		       '      "Name": "Bulk Products",'+
		       '      "IsTrackedAsInventory": false,'+
		       '      "IsSold": true,'+
		       '      "IsPurchased": false,'+
		       '      "ValidationErrors": []'+
		       '    },'+
		       '    {'+
		       '      "ItemID": "dba3f955-4ebd-4fce-98f7-a757e6579e3e",'+
		       '      "Code": "XERO1",'+
		       '      "UpdatedDateUTC": "Date(1586780871953)",'+
		       '      "PurchaseDetails": {},'+
		       '      "SalesDetails": {'+
		       '        "UnitPrice": 0.0000,'+
		       '        "AccountCode": "200",'+
		       '        "TaxType": "OUTPUT"'+
		       '      },'+
		       '      "Name": "Bulk Products",'+
		       '      "IsTrackedAsInventory": false,'+
		       '      "IsSold": true,'+
		       '      "IsPurchased": false,'+
		       '      "ValidationErrors": []'+
		       '    },'+
		       '    {'+
		       '      "ItemID": "b7d1935d-2dd3-4ffb-b5cd-16b0a53b85a6",'+
		       '      "Code": "XERO2",'+
		       '      "UpdatedDateUTC": "Date(1586780871958)",'+
		       '      "PurchaseDetails": {},'+
		       '      "SalesDetails": {'+
		       '        "UnitPrice": 0.0000,'+
		       '        "AccountCode": "200",'+
		       '        "TaxType": "OUTPUT"'+
		       '      },'+
		       '      "Name": "Bulk Products",'+
		       '      "IsTrackedAsInventory": false,'+
		       '      "IsSold": true,'+
		       '      "IsPurchased": false,'+
		       '      "ValidationErrors": []'+
		       '    },'+
		       '    {'+
		       '      "ItemID": "8a706480-9ec5-467d-a45b-17d0c3090309",'+
		       '      "Code": "XERO3",'+
		       '      "UpdatedDateUTC": "Date(1586780871962)",'+
		       '      "PurchaseDetails": {},'+
		       '      "SalesDetails": {'+
		       '        "UnitPrice": 0.0000,'+
		       '        "AccountCode": "200",'+
		       '        "TaxType": "OUTPUT"'+
		       '      },'+
		       '      "Name": "Bulk Products",'+
		       '      "IsTrackedAsInventory": false,'+
		       '      "IsSold": true,'+
		       '      "IsPurchased": false,'+
		       '      "ValidationErrors": []'+
		       '    },'+
		       '    {'+
		       '      "ItemID": "1e04eab4-c0ad-4cda-b03d-21d9498abf44",'+
		       '      "Code": "XERO4",'+
		       '      "UpdatedDateUTC": "Date(1586780871948)",'+
		       '      "PurchaseDetails": {},'+
		       '      "SalesDetails": {'+
		       '        "UnitPrice": 0.0000,'+
		       '        "AccountCode": "200",'+
		       '        "TaxType": "OUTPUT"'+
		       '      },'+
		       '      "Name": "Bulk Products",'+
		       '      "IsTrackedAsInventory": false,'+
		       '      "IsSold": true,'+
		       '      "IsPurchased": false,'+
		       '      "ValidationErrors": []'+
		       '    }'+
		       '  ]'+
		       '}';
	}
/**
 * Response from xero when an item with incomplete details is synced to xero
 */ 
	@testVisible
	static String failedSyncResponse() {
		return '{'+
		       '  "ErrorNumber": 10,'+
		       '  "Type": "ValidationException",'+
		       '  "Message": "A validation exception occurred",'+
		       '  "Elements": ['+
		       '    {'+
		       '      "ItemID": "00000000-0000-0000-0000-000000000000",'+
		       '      "UpdatedDateUTC": "Date(-62135596800000)",'+
		       '      "PurchaseDetails": {},'+
		       '      "SalesDetails": {'+
		       '        "UnitPrice": 0.0,'+
		       '        "AccountCode": "200",'+
		       '        "ValidationErrors": []'+
		       '      },'+
		       '      "Name": "Fail Product",'+
		       '      "IsTrackedAsInventory": false,'+
		       '      "IsSold": false,'+
		       '      "IsPurchased": false,'+
		       '      "ValidationErrors": ['+
		       '        {'+
		       '          "Message": "Price List Item Code must be supplied"'+
		       '        },'+
		       '        {'+
		       '          "Message": "IsSold cannot be set to false when SalesDetails or Description values are provided."'+
		       '        }'+
		       '      ]'+
		       '    }'+
		       '  ]'+
		       '}';
	}

}