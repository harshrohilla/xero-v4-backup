/**
 * @File Name          : .cls
 * @Description        :
 * @Author             : Gulshan
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    06/01/2020      Gulshan                Initial Version
 */
public virtual class XR_AuthHandler {

	public static String tenantId;
	
	String status {get; set;}
	String message {get; set;}
	
	/**
	 * XeroHelper as soon as helper is called, authenticate with xero and fetch tenantid
	 * @return       void
	 */
	public XR_AuthHandler(){
		try{
			if(tenantId == null){
				tenantId = getTennantId();
			}			
		}
		catch(Exception e)
		{
			this.status = 'Failed';
			this.message = e.getMessage();
		}
	}
	/**
	 * getTennantId Authenticate with xero and fetch tenantId
	 * @return      return tenantId fethced from xero
	 */
	public String getTennantId(){

		HTTPHandler handler = new HTTPHandler();
		HTTPHandler.Response response = handler.doGet('callout:' + XR_Const.connectionEndpoint,null);
		if(response.status == 'Success' && !response.body.contains('AuthenticationUnsuccessful')) {
			List<Object> responseList = (List<Object>) JSON.deserializeUntyped(response.body);
			if(responseList != null && responseList.size() > 0) {
				Map<String, Object> connectionMap = (Map<String, Object>)responseList[0];
				return (String)connectionMap.get('tenantId');
			}
		}else {
			this.status = 'Failed';
			this.message = response.message;
		}
		return null;
	}
	
	
}