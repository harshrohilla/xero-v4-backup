@isTest
/**
 * Test the HTTP methods
 */
public with sharing class HTTPHandlerTest
{
	@TestSetup
	static void makeData(){
		User user = DataFactoryTest.getUsers(1,null)[0];
	}
	static testMethod void testHTTPMethods(){
		User user = [SELECT Id FROM User LIMIT 1];
		system.runAs(user){
			Test.startTest();
			HTTPHandler handler = new HTTPHandler();
			Map<String,String> headerMap = new Map<String,String>();
			handler.doPut('endpoint','body',headerMap);
			Test.stopTest();
			System.assertEquals('test', 'test', 'HTTPMethod testMethod failed');
		}
	}
}
