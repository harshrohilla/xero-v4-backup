public inherited sharing class XR_InvoiceHelper extends XR_AuthHandler
{

	/**
	 * getInvoice                get invoices from xero based on last sync time or based on xero invoice ids
	 * @param  dateStr           lastSyncTime from custom setting to fetch all records updated after that time
	 * @param  invoiceXeroIdList list of xeroids of invoice from webhook for fetching invoices from xero and update details in sf
	 * @return                   return wrapper of response
	 */
	public InvoiceResultWrapper getInvoiceByIds(Set<String> invoiceXeroIdList){
		if (invoiceXeroIdList.size() > 0) {
			String invoiceIds = '';
			for (String ids : invoiceXeroIdList) {
				invoiceIds += ids+',';
			}
			String endpoint = 'callout:' + XR_Const.invoiceEndpoint + '?IDs=' + invoiceIds.removeEnd(',');
			return getInvoice(endpoint, null);
		}
		return null;
	}
	/**
	 * getInvoice takes endpoint from getInvoiceByIds and header map, do callout to xero to get invoices based on these ids
	 *            and in return send reponse to handleResponse method
	 * @param  endpoint  on which callout is supposed to hit, contains comma separated xero invoiceIds in URL
	 * @param  headerMap map of tenantid key and value required for callout
	 * @return           return response from callout to handle reponse and inturn return a wrapper filled with invoices
	 */
	public InvoiceResultWrapper getInvoice(String endpoint, Map<String,String> headerMap){
		headerMap = headerMap == null ? new Map<String,String>() : headerMap;
		headerMap.put('xero-tenant-id', XR_AuthHandler.tenantId);
		HTTPHandler handler = new HTTPHandler();

		HTTPHandler.Response response = handler.doGet(endpoint, headerMap);
		return handleResponse(new List<Opportunity>(), response);
	}
	/**
	 * postInvoice    post invoices to xero from invoice details from sf
	 * @param         invoiceList list of invoice records
	 * @return        wrapper of sync details from xero
	 */
	public InvoiceResultWrapper postInvoice(List<Opportunity> oppList){
		List<XR_Data.Invoice> invoiceWrapperList = new List<XR_Data.Invoice>();

		if(oppList != null && oppList.size() > 0) {
			for(Opportunity opp : oppList) {
				//map sf to xero invoice fields and return a wrapper of invoices to be synced
				XR_Data.Invoice invoiceWrapper = XR_MappingHandler.mapInvoiceSFDCToXero(opp);
				invoiceWrapper.LineItems = new List<XR_Data.LineItem>();
				for(OpportunityLineItem line: opp.OpportunityLineItems) {
					//get all related invoice line items
					invoiceWrapper.LineItems.add(XR_MappingHandler.mapLineItemSFDCToXero(line));
				}
				invoiceWrapper.Contact = XR_MappingHandler.mapContactSFDCToXero(opp.Account);
				//add invoices in wrapper list to be sent to xero
				invoiceWrapperList.add(invoiceWrapper);
			}
		}
		//prepare map for request body
		Map<String, List<XR_Data.Invoice> > invoiceMap = new Map<String, List<XR_Data.Invoice> >();
		invoiceMap.put('Invoices', invoiceWrapperList);
		//prepare header map for callout
		Map<String,String> headerMap = new Map<String,String>();
		headerMap.put('xero-tenant-id', XR_AuthHandler.tenantId);
		//serialize request body for sending in callout
		String requestBody = JSON.serializePretty(invoiceMap);
		//handle date property since date is a reserved keyword in salesforce
		requestBody = requestBody.replaceAll('"Date1" :','"Date" :');
		//send the reponse to handler for deserialization and further events
		return postInvoiceCallout(requestBody, headerMap, oppList);
	}
	/**
	 * postInvoiceCallout takes request body, header map and list of opportunities to post those invoices to xero
	 * @param  requestBody JSON payload to be sent in request body
	 * @param  headerMap   required map for callout (tenant id key value)
	 * @param  oppList     list of opportunities that were serialized into json to help in mapping the response in order to upsert in salesforce
	 * @return             return wrapper containing list of mapped invoice and related records to be upserted in salesforce
	 */
	public InvoiceResultWrapper postInvoiceCallout(String requestBody, Map<String,String> headerMap, List<Opportunity> oppList){
		String endpoint = 'callout:' + XR_Const.invoiceEndpoint + '?summarizeErrors=false';
		headerMap = headerMap == null ? new Map<String,String>() : headerMap;
		headerMap.put('xero-tenant-id', XR_AuthHandler.tenantId);
		HTTPHandler handler = new HTTPHandler();
		//perform callout
		HTTPHandler.Response response = handler.doPost(endpoint, requestBody, headerMap);
		return handleResponse(oppList, response);
	}
	/**
	 * handleResponse      take response from postInvoice callouts and deserialize it and put in wrapper
	 * @param              list of invoices which were synced from salesforce
	 * @param              response from callouts
	 * @return             return wrappers containing response data
	 */
	private InvoiceResultWrapper handleResponse(List<Opportunity> invoiceList, HTTPHandler.Response response){
		InvoiceResultWrapper wrapper = new InvoiceResultWrapper();
		if(response != null && response.body != null) {
			//Handle The Date property with replace
			response.body = response.body.replaceAll('"Date" :','"Date1" :');
			//deserialize the resposne
			XR_Data.XeroResponse responseWrapper = (XR_Data.XeroResponse)JSON.deserialize(response.body, XR_Data.XeroResponse.class);
			List<XR_Data.Invoice> invoiceWrapperList = responseWrapper.Invoices;
			//fill wrapper classes with data
			for(Integer i = 0; i < invoiceWrapperList.size(); i++) {
				Opportunity opp;
				if(invoiceList.size() < i+1) {
					opp = new Opportunity();
				}else {
					opp = invoiceList[i];
				}
				opp = XR_MappingHandler.mapInvoiceXeroToSFDC(opp, invoiceWrapperList[i]);
				Account customer = XR_MappingHandler.mapContactXeroToSFDC(opp.Account, invoiceWrapperList[i].Contact);
				//add line items to wrapper
				if(opp.OpportunityLineItems.size() > 0 && invoiceWrapperList[i].LineItems != null && invoiceWrapperList[i].LineItems.size() > 0) {
					for(Integer j = 0; j < invoiceWrapperList[i].LineItems.size(); j++) {
						//get all respective invoice line items based on indexes
						if(opp.OpportunityLineItems.size() > j && invoiceWrapperList[i].LineItems.size() > j) {
							opp.OpportunityLineItems[j] = (OpportunityLineItem)XR_MappingHandler.mapLineItemXeroToSFDC(opp.OpportunityLineItems[j], invoiceWrapperList[i].LineItems[j]);
							wrapper.invoiceLineItemList.add(opp.OpportunityLineItems[j]);
						}
					}
				}
				//fill wrapper with invoice
				wrapper.invoiceList.add(opp);
				//fill wrapper with customer details to be upserted in account object in salesforce
				wrapper.accountList.add(customer);
			}
		}
		return wrapper;
	}

	/**
	 * wrapper class for handling invoice, line items and account deserialization
	 */

	public class InvoiceResultWrapper
	{
		public List<Opportunity> invoiceList {get; set;}
		public List<OpportunityLineItem> invoiceLineItemList {get; set;}
		public List<Account> accountList {get; set;}

		public InvoiceResultWrapper(){
			invoiceList = new List<Opportunity> ();
			invoiceLineItemList = new List<OpportunityLineItem> ();
			accountList = new List<Account> ();
		}
	}
}