/*
 * This Class is used to no add Dummy Action for not doing anything, when need to bypass any process builder.
 * Related Test Class : ProcessBuilderNoActionTest
 * @date    2020-04-14
 * @author  Harsh
 */
public class ProcessBuilderNoAction {
    @InvocableMethod
    public static void doNothing() {
        /*
         This Class is used to no add Dummy Action for not doing anything, when need to bypass any process builder
         and you do not want to perform any action in certian condtion.
         See referenced Idea
         https://success.salesforce.com/ideaView?id=08730000000DqGRAA0
         No code required for this method
        */
    }
}