@isTest
public with sharing class XR_MockDataTest
{
	@testVisible
	static void prepareMockDataForGetCustomers(){
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,getConnectionJSON());
		XR_MockHTTPResponseGenerator getCustomerMock = new XR_MockHTTPResponseGenerator(200,getCustomerJSON());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();
		testRespMap.put('callout:Xero/connections',connectionMock);
		String whereClause = 'Name=="Xero Account"';
		testRespMap.put('callout:Xero/api.xro/2.0/contacts?where=' + EncodingUtil.urlEncode(whereClause, 'UTF-8'),getCustomerMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	/**
	 * prepareMockDataForNoCustomerFound mock when an opportunity is tried to sync but corresponding account was absent in xero
	 */
	@testVisible
	static void prepareMockDataForNoCustomerFound(){
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,getConnectionJSON());
		XR_MockHTTPResponseGenerator getCustomerMock = new XR_MockHTTPResponseGenerator(200,responseNoContactsFound());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();
		testRespMap.put('callout:Xero/connections',connectionMock);
		String whereClause = 'Name=="Xero Account Missing Id"';
		testRespMap.put('callout:Xero/api.xro/2.0/contacts?where=' + EncodingUtil.urlEncode(whereClause, 'UTF-8'),getCustomerMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);

	}
	@testVisible
	/**
	 * mockForAccountXeroIdMissingButPresentInXero Mock when account is present in xero, but xero id is blank in salesforce
	 */ 
	static void mockForAccountXeroIdMissingButPresentInXero(){
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,getConnectionJSON());

		XR_MockHTTPResponseGenerator customerMock = new XR_MockHTTPResponseGenerator(200,getCustomerJSONWithXeroIdMissingInSalesforce());

		XR_MockHTTPResponseGenerator invoiceMock = new XR_MockHTTPResponseGenerator(200,getInvoiceJSONForMissingXeroId());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();

		testRespMap.put('callout:Xero/connections',connectionMock);

		testRespMap.put('callout:Xero/api.xro/2.0/Invoices?summarizeErrors=false',invoiceMock);

		String whereClause = 'Name=="Xero Account2"';
		testRespMap.put('callout:Xero/api.xro/2.0/contacts?where=' + EncodingUtil.urlEncode(whereClause, 'UTF-8'),customerMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	/**
	 * prepareMockDataToPushInvoice description
	 */
	@testVisible
	static void prepareMockDataToPushInvoice(List<Opportunity> invoiceList){

		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,getConnectionJSON());

		//for single invoice sync
		XR_MockHTTPResponseGenerator invoiceMock = new XR_MockHTTPResponseGenerator(200,getInvoiceJSON());

		//for multiple invoices sync
		XR_MockHTTPResponseGenerator multipleInvoices = new XR_MockHTTPResponseGenerator(200,postMultipleInvoiceJson());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();

		testRespMap.put('callout:Xero/connections',connectionMock);

		if(invoiceList.size() <= 1) {
			testRespMap.put('callout:Xero/api.xro/2.0/Invoices?summarizeErrors=false',invoiceMock);
		}else {
			testRespMap.put('callout:Xero/api.xro/2.0/Invoices?summarizeErrors=false',multipleInvoices);
		}

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	@testVisible
	static void prepareMockDataToGetInvoiceFromXeroId(){
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,getConnectionJSON());
		XR_MockHTTPResponseGenerator getInvoiceMock = new XR_MockHTTPResponseGenerator(200,getInvoiceResponse());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();
		testRespMap.put('callout:Xero/connections',connectionMock);
		testRespMap.put('callout:Xero/api.xro/2.0/Invoices?IDs=2bd5f5ee-fde5-4550-bc9e-5dd8f208668b',getInvoiceMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);
		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	@testVisible
	static void prepareMockDataToGetInvoice(){
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,getConnectionJSON());
		XR_MockHTTPResponseGenerator getInvoiceMock = new XR_MockHTTPResponseGenerator(200,getInvoiceResponse());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();
		testRespMap.put('callout:Xero/connections',connectionMock);
		testRespMap.put('callout:Xero/api.xro/2.0/Invoices',getInvoiceMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);
		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	@testVisible
	static void prepareMockDataAuthFailure(){
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,responseAuthFailure());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();

		testRespMap.put('callout:Xero/connections',connectionMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	/**
	 * getConnectionJSON description
	 * @return   return description
	 */
	@testVisible
	static String getConnectionJSON(){
		return '[' +
		       '{' +
		       '"id": "0316baa0-c4ce-43e9-832f-428bcf65f4eb",' +
		       '"tenantId": "6d683406-923d-4015-85e0-546106b85db6",' +
		       '"tenantType": "ORGANISATION",' +
		       '"createdDateUtc": "2019-12-18T08:34:30.2848460",' +
		       '"updatedDateUtc": "2019-12-18T08:35:46.1741210"' +
		       '}' +
		       ']';
	}

	/**
	 * getInvoiceJSON Contains one Invoice with one Line Item
	 * @return   return description
	 */
		@testVisible
	static String getInvoiceJSON(){
		String json =  '{' +
		              '"Invoices": [' +
		              '{' +
					  '"Type": "ACCREC",' +
					  '"SentToContact":false,'+
		              '"Contact": {' +
		              '"ContactID": "67b8620b-e7df-4bc3-9c44-af03c2ab11ae",' +
		              '"ContactStatus": "ACTIVE",' +
		              '"Name": "City Agency",' +
		              '"Addresses": [' +
		              '{ "AddressType": "STREET" },' +
		              '{' +
		              '"AddressType": "POBOX",' +
		              '"AddressLine1": "L4, CA House",' +
		              '"AddressLine2": "14 Boulevard Quay",' +
		              '"City": "Wellington",' +
		              '"PostalCode": "6012"' +
		              '}' +
		              '],' +
		              '"Phones": [' +
		              '{ "PhoneType": "DEFAULT" },' +
		              '{ "PhoneType": "DDI" },' +
		              '{ "PhoneType": "MOBILE" },' +
		              '{ "PhoneType": "FAX" }' +
		              '],' +
		              '"UpdatedDateUTC": "Date(1518685950940+0000)",' +
		              '"IsSupplier": "false",' +
		              '"IsCustomer": "true"' +
		              '},' +
		              '"Date1": "Date(1518685950940+0000)",' +
		              '"DueDate": "Date(1518685950940+0000)",' +
		              '"DateString": "2009-05-27T00:00:00",' +
		              '"DueDateString": "2009-06-06T00:00:00",' +
		              '"Status": "AUTHORISED",' +
		              '"LineAmountTypes": "Exclusive",' +
		              '"LineItems": [' +
		              '{' +
		              '"Description": "Onsite project management ",' +
		              '"Quantity": "1.0000",' +
		              '"UnitAmount": "1800.00",' +
		              '"TaxType": "",' +
		              '"TaxAmount": "225.00",' +
		              '"LineAmount": "1800.00",' +
		              '"AccountCode": "200",' +
		              '"Tracking": [' +
		              '{' +
		              '"TrackingCategoryID": "e2f2f732-e92a-4f3a9c4d-ee4da0182a13",' +
		              '"Name": "Activity/Workstream",' +
		              '"Option": "Onsite consultancy"' +
		              '}' +
		              '],' +
		              '"LineItemID": "52208ff9-528a-4985-a9ad-b2b1d4210e38"' +
		              '}' +
		              '],' +
		              '"SubTotal": "1800.00",' +
		              '"TotalTax": "225.00",' +
		              '"Total": "2025.00",' +
		              '"UpdatedDateUTC": "Date(1518685950940+0000)",' +
		              '"CurrencyCode": "GBP",' +
		              '"InvoiceID": "243216c5-369e-4056-ac67-05388f86dc81",' +
		              '"InvoiceNumber": "OIT00546",' +
		              '"Payments": [' +
		              '{' +
		              '"Date": "Date(1518685950940+0000)",' +
		              '"Amount": "1000.00",' +
		              '"PaymentID": "0d666415-cf77-43fa-80c7-56775591d426"' +
		              '}' +
		              '],' +
		              '"AmountDue": "1025.00",' +
		              '"AmountPaid": "1000.00",' +
		              '"AmountCredited": "0.00"' +
		              '}' +
		              ']' +
		              '}';
		return json;
	}
	/**
	 * getInvoiceJSON Contains one Invoice with one Line Item, for Invoice with Xero Account2, xero id missing
	 * @return   return description
	 */
	@testVisible
	static String getInvoiceJSONForMissingXeroId(){
		String json =  '{' +
		              '"Invoices": [' +
		              '{' +
					  '"Type": "ACCREC",' +
					  '"SentToContact":false,'+
		              '"Contact": {' +
		              '"ContactID": "67b8620b-e7df-4bc3-9c44-af03c2ab11aH",' +
		              '"ContactStatus": "ACTIVE",' +
		              '"Name": "Xero Account2",' +
		              '"Addresses": [' +
		              '{ "AddressType": "STREET" },' +
		              '{' +
		              '"AddressType": "POBOX",' +
		              '"AddressLine1": "L4, CA House",' +
		              '"AddressLine2": "14 Boulevard Quay",' +
		              '"City": "Wellington",' +
		              '"PostalCode": "6012"' +
		              '}' +
		              '],' +
		              '"Phones": [' +
		              '{ "PhoneType": "DEFAULT" },' +
		              '{ "PhoneType": "DDI" },' +
		              '{ "PhoneType": "MOBILE" },' +
		              '{ "PhoneType": "FAX" }' +
		              '],' +
		              '"UpdatedDateUTC": "Date(1518685950940+0000)",' +
		              '"IsSupplier": "false",' +
		              '"IsCustomer": "true"' +
		              '},' +
		              '"Date1": "Date(1518685950940+0000)",' +
		              '"DueDate": "Date(1518685950940+0000)",' +
		              '"DateString": "2009-05-27T00:00:00",' +
		              '"DueDateString": "2009-06-06T00:00:00",' +
		              '"Status": "AUTHORISED",' +
		              '"LineAmountTypes": "Exclusive",' +
		              '"LineItems": [' +
		              '{' +
		              '"Description": "Onsite project management ",' +
		              '"Quantity": "1.0000",' +
		              '"UnitAmount": "1800.00",' +
		              '"TaxType": "",' +
		              '"TaxAmount": "225.00",' +
		              '"LineAmount": "1800.00",' +
		              '"AccountCode": "200",' +
		              '"Tracking": [' +
		              '{' +
		              '"TrackingCategoryID": "e2f2f732-e92a-4f3a9c4d-ee4da0182a13",' +
		              '"Name": "Activity/Workstream",' +
		              '"Option": "Onsite consultancy"' +
		              '}' +
		              '],' +
		              '"LineItemID": "52208ff9-528a-4985-a9ad-b2b1d4210e38"' +
		              '}' +
		              '],' +
		              '"SubTotal": "1800.00",' +
		              '"TotalTax": "225.00",' +
		              '"Total": "2025.00",' +
		              '"UpdatedDateUTC": "Date(1518685950940+0000)",' +
		              '"CurrencyCode": "GBP",' +
		              '"InvoiceID": "243216c5-369e-4056-ac67-05388f86dc8H",' +
		              '"InvoiceNumber": "OIT00546",' +
		              '"Payments": [' +
		              '{' +
		              '"Date": "Date(1518685950940+0000)",' +
		              '"Amount": "1000.00",' +
		              '"PaymentID": "0d666415-cf77-43fa-80c7-56775591d426"' +
		              '}' +
		              '],' +
		              '"AmountDue": "1025.00",' +
		              '"AmountPaid": "1000.00",' +
		              '"AmountCredited": "0.00"' +
		              '}' +
		              ']' +
		              '}';
		return json;
	}
	@testVisible
	//response contains 1 invoice with 2 line items && another 1 invoice with 3 line items
	static string postMultipleInvoiceJson(){
		string res = '{'+
		             '"Id": "2b03c257-ea16-46a2-a1d5-c0aad62d2400",'+
		             '"Status": "OK",'+
		             '"ProviderName": "SalesforceDemo",'+
		             '"DateTimeUTC": "Date(1577686813448)",'+
		             '"Invoices": ['+
		             '{'+
		             ' "Type": "ACCREC",'+
		             ' "InvoiceID": "1d8c6e1a-2351-467b-b187-c2adec5303c8",'+
		             ' "InvoiceNumber": "INV-0049",'+
		             ' "Reference": "",'+
		             '"Prepayments": [],'+
		             ' "Overpayments": [],'+
		             ' "AmountDue": 4320.00,'+
		             '"AmountPaid": 0.00,'+
		             ' "SentToContact": false,'+
		             ' "CurrencyRate": 1.000000,'+
		             ' "IsDiscounted": false,'+
		             '"HasErrors": false,'+
		             ' "Contact": {'+
		             '"ContactID": "67b8620b-e7df-4bc3-9c44-af03c2ab11ae",'+
		             ' "ContactStatus": "ACTIVE",'+
		             '  "Name": "Xero Account",'+
		             ' "FirstName": "",'+
		             ' "LastName": "",'+
		             ' "EmailAddress": "",'+
		             ' "BankAccountDetails": "",'+
		             '  "Addresses": ['+
		             '  {'+
		             '  "AddressType": "STREET",'+
		             ' "City": "",'+
		             ' "Region": "",'+
		             ' "PostalCode": "",'+
		             ' "Country": "",'+
		             ' "AttentionTo": ""'+
		             ' },'+
		             ' {'+
		             ' "AddressType": "POBOX",'+
		             '  "City": "",'+
		             ' "Region": "",'+
		             ' "PostalCode": "",'+
		             ' "Country": "",'+
		             '"AttentionTo": ""'+
		             ' }'+
		             ' ],'+
		             '  "Phones": ['+
		             '  {'+
		             ' "PhoneType": "DEFAULT",'+
		             ' "PhoneNumber": "",'+
		             '"PhoneAreaCode": "",'+
		             ' "PhoneCountryCode": ""'+
		             '  },'+
		             ' {'+
		             '  "PhoneType": "DDI",'+
		             '  "PhoneNumber": "",'+
		             '   "PhoneAreaCode": "",'+
		             '  "PhoneCountryCode": ""'+
		             '  },'+
		             ' {'+
		             '  "PhoneType": "FAX",'+
		             ' "PhoneNumber": "",'+
		             '  "PhoneAreaCode": "",'+
		             ' "PhoneCountryCode": ""'+
		             ' },'+
		             ' {'+
		             ' "PhoneType": "MOBILE",'+
		             ' "PhoneNumber": "",'+
		             ' "PhoneAreaCode": "",'+
		             ' "PhoneCountryCode": ""'+
		             ' }'+
		             ' ],'+
		             '"UpdatedDateUTC": "Date(1577603727273+0000)",'+
		             ' "ContactGroups": [],'+
		             ' "IsSupplier": false,'+
		             '  "IsCustomer": true,'+
		             ' "SalesTrackingCategories": [],'+
		             ' "PurchasesTrackingCategories": [],'+
		             ' "ContactPersons": [],'+
		             '"HasValidationErrors": false'+
		             ' },'+
		             ' "DateString": "2019-12-30T00:00:00",'+
		             ' "Date": "Date(1577664000000+0000)",'+
		             ' "Status": "DRAFT",'+
		             ' "LineAmountTypes": "Exclusive",'+
		             '  "LineItems": ['+
		             ' {'+
		             '"Description": "Onsite project management",'+
		             '"UnitAmount": 1800.00,'+
		             '"TaxType": "OUTPUT2",'+
		             ' "TaxAmount": 360.00,'+
		             ' "LineAmount": 1800.00,'+
		             ' "AccountCode": "200",'+
		             ' "Tracking": [],'+
		             ' "Quantity": 1.0000,'+
		             ' "LineItemID": "dd159a57-9bc9-43a8-b6bc-11af1ec91612",'+
		             ' "ValidationErrors": []'+
		             ' },'+
		             ' {'+
		             ' "Description": "Onsite project management",'+
		             ' "UnitAmount": 1800.00,'+
		             ' "TaxType": "OUTPUT2",'+
		             ' "TaxAmount": 360.00,'+
		             ' "LineAmount": 1800.00,'+
		             ' "AccountCode": "200",'+
		             ' "Tracking": [],'+
		             ' "Quantity": 1.0000,'+
		             ' "LineItemID": "d550e75c-bc4b-4a7c-9c74-fdee53aa3d96",'+
		             ' "ValidationErrors": []'+
		             ' }'+
		             ' ],'+
		             ' "SubTotal": 3600.00,'+
		             ' "TotalTax": 720.00,'+
		             '  "Total": 4320.00,'+
		             ' "UpdatedDateUTC": "Date(1577686813293+0000)",'+
		             '"CurrencyCode": "GBP",'+
		             ' "StatusAttributeString": "WARNING",'+
		             ' "Warnings": ['+
		             ' {'+
		             ' "Message": "Only AUTHORISED invoices may have SentToContact updated."'+
		             '  }'+
		             '   ]'+
		             '  },'+
		             ' {'+
		             ' "Type": "ACCREC",'+
		             ' "InvoiceID": "2bd5f5ee-fde5-4550-bc9e-5dd8f208668b",'+
		             ' "InvoiceNumber": "INV-0048",'+
		             ' "Reference": "",'+
		             '  "Prepayments": [],'+
		             '"Overpayments": [],'+
		             ' "AmountDue": 6480.00,'+
		             ' "AmountPaid": 0.00,'+
		             ' "SentToContact": false,'+
		             ' "CurrencyRate": 1.000000,'+
		             '  "IsDiscounted": false,'+
		             ' "HasErrors": false,'+
		             '  "Contact": {'+
		             ' "ContactID": "67b8620b-e7df-4bc3-9c44-af03c2ab11ae",'+
		             ' "ContactStatus": "ACTIVE",'+
		             ' "Name": "GenePoint",'+
		             ' "FirstName": "",'+
		             ' "LastName": "",'+
		             '  "EmailAddress": "",'+
		             '  "BankAccountDetails": "",'+
		             '  "Addresses": ['+
		             ' {'+
		             '  "AddressType": "STREET",'+
		             '  "City": "",'+
		             ' "Region": "",'+
		             '  "PostalCode": "",'+
		             ' "Country": "",'+
		             '  "AttentionTo": ""'+
		             ' },'+
		             ' {'+
		             '  "AddressType": "POBOX",'+
		             ' "City": "",'+
		             ' "Region": "",'+
		             ' "PostalCode": "",'+
		             ' "Country": "",'+
		             '  "AttentionTo": ""'+
		             '  }'+
		             ' ],'+
		             ' "Phones": ['+
		             ' {'+
		             '  "PhoneType": "DEFAULT",'+
		             '  "PhoneNumber": "",'+
		             '  "PhoneAreaCode": "",'+
		             '  "PhoneCountryCode": ""'+
		             '  },'+
		             ' {'+
		             ' "PhoneType": "DDI",'+
		             ' "PhoneNumber": "",'+
		             ' "PhoneAreaCode": "",'+
		             ' "PhoneCountryCode": ""'+
		             '  },'+
		             '  {'+
		             '  "PhoneType": "FAX",'+
		             '  "PhoneNumber": "",'+
		             '  "PhoneAreaCode": "",'+
		             '  "PhoneCountryCode": ""'+
		             '  },'+
		             '  {'+
		             '  "PhoneType": "MOBILE",'+
		             '   "PhoneNumber": "",'+
		             '   "PhoneAreaCode": "",'+
		             '   "PhoneCountryCode": ""'+
		             '  }'+
		             '],'+
		             ' "UpdatedDateUTC": "Date(1577603727273+0000)",'+
		             ' "ContactGroups": [],'+
		             ' "IsSupplier": false,'+
		             ' "IsCustomer": true,'+
		             ' "SalesTrackingCategories": [],'+
		             ' "PurchasesTrackingCategories": [],'+
		             '  "ContactPersons": [],'+
		             ' "HasValidationErrors": false'+
		             ' },'+
		             ' "DateString": "2019-12-30T00:00:00",'+
		             ' "Date": "Date(1577664000000+0000)",'+
		             ' "Status": "DRAFT",'+
		             ' "LineAmountTypes": "Exclusive",'+
		             ' "LineItems": ['+
		             '{'+
		             '  "Description": "Onsite project management",'+
		             ' "UnitAmount": 1800.00,'+
		             ' "TaxType": "OUTPUT2",'+
		             '  "TaxAmount": 360.00,'+
		             ' "LineAmount": 1800.00,'+
		             ' "AccountCode": "200",'+
		             '  "Tracking": [],'+
		             '  "Quantity": 1.0000,'+
		             '  "LineItemID": "2aad6420-420b-4a97-bfa2-83108d0dbd8b",'+
		             '  "ValidationErrors": []'+
		             '},'+
		             ' {'+
		             ' "Description": "Onsite project management",'+
		             ' "UnitAmount": 1800.00,'+
		             ' "TaxType": "OUTPUT2",'+
		             '"TaxAmount": 360.00,'+
		             ' "LineAmount": 1800.00,'+
		             ' "AccountCode": "200",'+
		             ' "Tracking": [],'+
		             ' "Quantity": 1.0000,'+
		             ' "LineItemID": "388dbd37-6f35-4327-a5e7-7fe0dcbe096e",'+
		             ' "ValidationErrors": []'+
		             ' },'+
		             ' {'+
		             ' "Description": "Onsite project management",'+
		             ' "UnitAmount": 1800.00,'+
		             ' "TaxType": "OUTPUT2",'+
		             '  "TaxAmount": 360.00,'+
		             '  "LineAmount": 1800.00,'+
		             '  "AccountCode": "200",'+
		             '  "Tracking": [],'+
		             '  "Quantity": 1.0000,'+
		             '  "LineItemID": "d6c9e234-574d-4b93-859d-557d3cebff3e",'+
		             '   "ValidationErrors": []'+
		             '  }'+
		             ' ],'+
		             ' "SubTotal": 5400.00,'+
		             ' "TotalTax": 1080.00,'+
		             ' "Total": 6480.00,'+
		             ' "UpdatedDateUTC": "Date(1577686813417+0000)",'+
		             ' "CurrencyCode": "GBP",'+
		             ' "StatusAttributeString": "WARNING",'+
		             ' "Warnings": ['+
		             ' {'+
		             '   "Message": "Only AUTHORISED invoices may have SentToContact updated."'+
		             ' }'+
		             '  ]'+
		             ' }'+
		             ' ]'+
		             '}';
		return res;
	}
	@testVisible
	static String getInvoiceResponse(){
		String resBody = '{'+
		                 '\t"Id": "6df4385a-6c5c-47df-a728-ac6389bd97d9",'+
		                 '\t"Status": "OK",'+
		                 '\t"ProviderName": "SalesforceDemo",'+
		                 '\t"DateTimeUTC": "Date(1577705054739)",'+
		                 '\t"Invoices": [{'+
		                 '\t\t"Type": "ACCREC",'+
		                 '\t\t"InvoiceID": "2bd5f5ee-fde5-4550-bc9e-5dd8f208668b",'+
		                 '\t\t"InvoiceNumber": "INV-0048",'+
		                 '\t\t"Reference": "",'+
		                 '\t\t"Payments": [{'+
		                 '\t\t\t"PaymentID": "702375ae-8111-4be3-a178-8fe6eac9ed3c",'+
		                 '\t\t\t"Date": "Date(1577664000000+0000)",'+
		                 '\t\t\t"Amount": 6480.00,'+
		                 '\t\t\t"CurrencyRate": 250.000000,'+
		                 '\t\t\t"HasAccount": false,'+
		                 '\t\t\t"HasValidationErrors": false'+
		                 '\t\t}],'+
		                 '\t\t"CreditNotes": [],'+
		                 '\t\t"Prepayments": [],'+
		                 '\t\t"Overpayments": [],'+
		                 '\t\t"AmountDue": 0.00,'+
		                 '\t\t"AmountPaid": 6480.00,'+
		                 '\t\t"AmountCredited": 0.00,'+
		                 '\t\t"SentToContact": true,'+
		                 '\t\t"CurrencyRate": 1.171540,'+
		                 '\t\t"IsDiscounted": false,'+
		                 '\t\t"HasAttachments": false,'+
		                 '\t\t"HasErrors": false,'+
		                 '\t\t"Contact": {'+
		                 '\t\t\t"ContactID": "67b8620b-e7df-4bc3-9c44-af03c2ab11ae",'+
		                 '\t\t\t"Name": "Xero Account",'+
		                 '\t\t\t"Addresses": [],'+
		                 '\t\t\t"Phones": [],'+
		                 '\t\t\t"ContactGroups": [],'+
		                 '\t\t\t"ContactPersons": [],'+
		                 '\t\t\t"HasValidationErrors": false'+
		                 '\t\t},'+
		                 '\t\t"DateString": "2019-12-30T00:00:00",'+
		                 '\t\t"Date1": "Date(1577664000000+0000)",'+
		                 '\t\t"DueDateString": "2019-12-30T00:00:00",'+
		                 '\t\t"DueDate": "Date(1577664000000+0000)",'+
		                 '\t\t"BrandingThemeID": "3aa40736-32ee-4d83-894a-375c3f3ff480",'+
		                 '\t\t"Status": "PAID",'+
		                 '\t\t"LineAmountTypes": "Exclusive",'+
		                 '\t\t"LineItems": [],'+
		                 '\t\t"SubTotal": 5400.00,'+
		                 '\t\t"TotalTax": 1080.00,'+
		                 '\t\t"Total": 6480.00,'+
		                 '\t\t"UpdatedDateUTC": "Date(1577705030347+0000)",'+
		                 '\t\t"CurrencyCode": "EUR",'+
		                 '\t\t"FullyPaidOnDate": "Date(1577664000000+0000)"'+
		                 '\t}]'+
		                 '}';

		return resBody;
	}
	@testVisible
	static String getCustomerJSON(){

		String resp = '{'+
		              '\t"Contacts": [{' +
		              '\t\t"ContactID": "bd2270c3-8706-4c11-9cfb-000b551c3f51",'+
		              '\t\t"ContactStatus": "ACTIVE",'+
		              '\t\t"Name": "Xero Account",'+
		              '\t\t"FirstName": "Xero",'+
		              '\t\t"LastName": "Account",'+
		              '\t\t"EmailAddress": "a.dutchess@abclimited.com",'+
		              '\t\t"SkypeUserName": "skype.dutchess@abclimited.com",'+
		              '\t\t"BankAccountDetails": "45465844",'+
		              '\t\t"TaxNumber": "415465456454",'+
		              '\t\t"AccountsReceivableTaxType": "INPUT2",'+
		              '\t\t"AccountsPayableTaxType": "OUTPUT2",'+
		              '\t\t"Addresses": [{'+
		              '\t\t\t"AddressType": "POBOX",'+
		              '\t\t\t"AddressLine1": "P O Box 123",'+
		              '\t\t\t"City": "Wellington",'+
		              '\t\t\t"PostalCode": "6011",'+
		              '\t\t\t"AttentionTo": "Andrea"'+
		              '\t\t}, {'+
		              '\t\t\t"AddressType": "STREET"'+
		              '\t\t}],'+
		              '\t\t"Phones": [{'+
		              '\t\t\t"PhoneType": "DEFAULT",'+
		              '\t\t\t"PhoneNumber": "1111111",'+
		              '\t\t\t"PhoneAreaCode": "04",'+
		              '\t\t\t"PhoneCountryCode": "64"'+
		              '\t\t}, {'+
		              '\t\t\t"PhoneType": "FAX"'+
		              '\t\t}, {'+
		              '\t\t\t"PhoneType": "MOBILE"'+
		              '\t\t}, {'+
		              '\t\t\t"PhoneType": "DDI"'+
		              '\t\t}],'+
		              '\t\t"UpdatedDateUTC": "Date(1488391422280+0000)",'+
		              '\t\t"IsSupplier": false,'+
		              '\t\t"IsCustomer": true,'+
		              '\t\t"DefaultCurrency": "NZD"'+
		              '\t}]' +
		              '}';

		return resp;
	}
	@testVisible
	/**
	 * getCustomerJSONWithXeroIdMissingInSalesforce for Xero Account2 (Xero Id missing in salesforce)
	 * @return   return description
	 */ 
	static String getCustomerJSONWithXeroIdMissingInSalesforce(){

		String resp = '{'+
		              '\t"Contacts": [{' +
		              '\t\t"ContactID": "67b8620b-e7df-4bc3-9c44-af03c2ab11aH",'+
		              '\t\t"ContactStatus": "ACTIVE",'+
		              '\t\t"Name": "Xero Account2",'+
		              '\t\t"FirstName": "Xero",'+
		              '\t\t"LastName": "Account2",'+
		              '\t\t"EmailAddress": "a.dutchess@abclimited.com",'+
		              '\t\t"SkypeUserName": "skype.dutchess@abclimited.com",'+
		              '\t\t"BankAccountDetails": "45465844",'+
		              '\t\t"TaxNumber": "415465456454",'+
		              '\t\t"AccountsReceivableTaxType": "INPUT2",'+
		              '\t\t"AccountsPayableTaxType": "OUTPUT2",'+
		              '\t\t"Addresses": [{'+
		              '\t\t\t"AddressType": "POBOX",'+
		              '\t\t\t"AddressLine1": "P O Box 123",'+
		              '\t\t\t"City": "Wellington",'+
		              '\t\t\t"PostalCode": "6011",'+
		              '\t\t\t"AttentionTo": "Andrea"'+
		              '\t\t}, {'+
		              '\t\t\t"AddressType": "STREET"'+
		              '\t\t}],'+
		              '\t\t"Phones": [{'+
		              '\t\t\t"PhoneType": "DEFAULT",'+
		              '\t\t\t"PhoneNumber": "1111111",'+
		              '\t\t\t"PhoneAreaCode": "04",'+
		              '\t\t\t"PhoneCountryCode": "64"'+
		              '\t\t}, {'+
		              '\t\t\t"PhoneType": "FAX"'+
		              '\t\t}, {'+
		              '\t\t\t"PhoneType": "MOBILE"'+
		              '\t\t}, {'+
		              '\t\t\t"PhoneType": "DDI"'+
		              '\t\t}],'+
		              '\t\t"UpdatedDateUTC": "Date(1488391422280+0000)",'+
		              '\t\t"IsSupplier": false,'+
		              '\t\t"IsCustomer": true,'+
		              '\t\t"DefaultCurrency": "NZD"'+
		              '\t}]' +
		              '}';

		return resp;
	}
	@testVisible
	static String responseFromWebhook(){
		String res = '{'+
		             '   "events": ['+
		             '      {'+
		             '         "resourceUrl": "https://api.xero.com/api.xro/2.0/Contacts/2bd5f5ee-fde5-4550-bc9e-5dd8f208668b",'+
		             '         "resourceId": "2bd5f5ee-fde5-4550-bc9e-5dd8f208668b",'+
		             '         "eventDateUtc": "2017-06-21T01:15:39.902",'+
		             '         "eventType": "UPDATE",'+
		             '         "eventCategory": "INVOICE",'+
		             '         "tenantId": "c2cc9b6e-9458-4c7d-93cc-f02b81b0594f",'+
		             '         "tenantType": "ORGANISATION"'+
		             '      }'+
		             '   ],'+
		             '   "lastEventSequence": 1,'+
		             '   "firstEventSequence": 1,'+
		             '   "entropy": "S0m3r4Nd0mt3xt"'+
		             ''+
		             '}';
		return res;
	}
	@testVisible
	static String responseAuthFailure(){
		String res = '{'+
		             '"title": "Forbidden",'+
		             '"status": 403,'+
		             '"detail": "AuthenticationUnsuccessful",'+
		             '"instance": "65e420cd-796c-493b-8f52-5eae2ee667ce"'+
		             '}';
		return res;
	}
	/**
	 * responseNoContactsFound response when no contact is found in xero
	 * @return   return response
	 */
	@testVisible
	static String responseNoContactsFound(){
		String res = '{'+
		             '  "Id": "5602684c-4047-448f-9c53-5fe405d382f5",'+
		             '  "Status": "OK",'+
		             '  "ProviderName": "Salesforce",'+
		             '  "DateTimeUTC": "Date(1586437401006)",'+
		             '  "Contacts": []'+
		             '}';
		return res;
	}
}
