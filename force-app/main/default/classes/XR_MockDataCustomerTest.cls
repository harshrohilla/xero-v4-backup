@isTest
public with sharing class XR_MockDataCustomerTest
{
	//mock data when an account is pushed to xero which was not present in xero
	@testVisible
	static void mockDataPushSingleProduct(){
		//for connection to xero
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,connectionJSON());
		//for single account sync
		XR_MockHTTPResponseGenerator contactMock = new XR_MockHTTPResponseGenerator(200,singleAccountPush());
		//for no customer with this name found in xero
		XR_MockHTTPResponseGenerator noContactFoundMock = new XR_MockHTTPResponseGenerator(200,noCustomerFoundJson());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();
		//for authentication with xero
		testRespMap.put('callout:Xero/connections',connectionMock);
		//because before push, all accounts are checked whether they are in xero or not
		//so this will mimic as if account was not found from xero
		String whereClause = 'Name=="Xero Account Missing Id"';
		testRespMap.put('callout:Xero/api.xro/2.0/contacts?where=' + EncodingUtil.urlEncode(whereClause, 'UTF-8'),noContactFoundMock);
		//when account was not found in xero, it will be pushed to xero and this will be the response
		testRespMap.put('callout:Xero/api.xro/2.0/contacts',contactMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	//mock data when an account is pushed to xero which was already present in xero
	@testVisible
	static void mockDataPushSingleProductPresentInXero(){
		//for connection to xero
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,connectionJSON());
		//for when customer with this name found in xero, so update the record and do not push further
		//singleAccountPush() json is used because xero sends the same response when customer is found
		XR_MockHTTPResponseGenerator contactFoundMock = new XR_MockHTTPResponseGenerator(200,singleAccountPush());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();
		//for authentication with xero
		testRespMap.put('callout:Xero/connections',connectionMock);
		//because before push, all accounts are checked whether they are in xero or not
		//so this will mimic as if account was found from xero and further this account wont be pushed
		String whereClause = 'Name=="Xero Account Missing Id"';
		testRespMap.put('callout:Xero/api.xro/2.0/contacts?where=' + EncodingUtil.urlEncode(whereClause, 'UTF-8'),contactFoundMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
	}
	//mock data when multiple accounts are pushed to xero which were not present in xero
	@testVisible
	static void mockDataPushMultipleProducts(){
		//for connection to xero
		XR_MockHTTPResponseGenerator connectionMock = new XR_MockHTTPResponseGenerator(200,connectionJSON());
		//for single account sync
		XR_MockHTTPResponseGenerator contactMock = new XR_MockHTTPResponseGenerator(200,multipleAccountsPush());
		//for no customer with these names found in xero
		XR_MockHTTPResponseGenerator noContactFoundMock = new XR_MockHTTPResponseGenerator(200,noCustomerFoundJson());

		Map<String, HttpCalloutMock> testRespMap  = new Map<String,HttpCalloutMock>();
		//for authentication with xero
		testRespMap.put('callout:Xero/connections',connectionMock);
		//because before push, all accounts are checked whether they are in xero or not
		//so this will mimic as if account was not found from xero
		String whereClause = 'Name=="Xero Account Missing Id" OR Name=="Xero Account Missing Id 2"';
		testRespMap.put('callout:Xero/api.xro/2.0/contacts?where=' + EncodingUtil.urlEncode(whereClause, 'UTF-8'),noContactFoundMock);
		//when account was not found in xero, it will be pushed to xero and this will be the response
		testRespMap.put('callout:Xero/api.xro/2.0/contacts',contactMock);

		HttpCalloutMock multiCalloutMock = new XR_MultiRequestMock(testRespMap);

		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
    }
	/**
	 * getConnectionJSON mock Connection response from Xero
	 * @return   return String of JSON containing tenantId
	 */
	@testVisible
	static String connectionJSON(){
		return '[' +
		       '{' +
		       '"id": "0316baa0-c4ce-43e9-832f-428bcf65f4eb",' +
		       '"tenantId": "6d683406-923d-4015-85e0-546106b85db6",' +
		       '"tenantType": "ORGANISATION",' +
		       '"createdDateUtc": "2019-12-18T08:34:30.2848460",' +
		       '"updatedDateUtc": "2019-12-18T08:35:46.1741210"' +
		       '}' +
		       ']';
	}
	@testVisible
	static String singleAccountPush(){
		return '{'+
		       '  "Id": "8db12af9-9229-4a5f-9a73-4c89cecf2742",'+
		       '  "Status": "OK",'+
		       '  "ProviderName": "Salesforce",'+
		       '  "DateTimeUTC": "Date(1587029582722)",'+
		       '  "Contacts": ['+
		       '    {'+
		       '      "ContactID": "eb518be4-26f3-4e52-af92-ce5ac3e24f09",'+
		       '      "ContactStatus": "ACTIVE",'+
		       '      "Name": "Xero Account Missing Id",'+
		       '      "EmailAddress": "",'+
		       '      "BankAccountDetails": "",'+
		       '      "Addresses": ['+
		       '        {'+
		       '          "AddressType": "STREET",'+
		       '          "City": "",'+
		       '          "Region": "",'+
		       '          "PostalCode": "",'+
		       '          "Country": ""'+
		       '        },'+
		       '        {'+
		       '          "AddressType": "POBOX",'+
		       '          "City": "",'+
		       '          "Region": "",'+
		       '          "PostalCode": "",'+
		       '          "Country": ""'+
		       '        }'+
		       '      ],'+
		       '      "Phones": ['+
		       '        {'+
		       '          "PhoneType": "DEFAULT",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "DDI",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "FAX",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "MOBILE",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        }'+
		       '      ],'+
		       '      "UpdatedDateUTC": "Date(1587029582667+0000)",'+
		       '      "ContactGroups": [],'+
		       '      "IsSupplier": false,'+
		       '      "IsCustomer": false,'+
		       '      "SalesTrackingCategories": [],'+
		       '      "PurchasesTrackingCategories": [],'+
		       '      "ContactPersons": [],'+
		       '      "HasValidationErrors": false'+
		       '    }'+
		       '  ]'+
		       '}';
	}
	@testVisible
	static String noCustomerFoundJson(){
		return '{'+
		       '  "Id": "85ab9cbe-c8d3-4a21-8a81-fc5bb64e8269",'+
		       '  "Status": "OK",'+
		       '  "ProviderName": "Salesforce_Crossdoor",'+
		       '  "DateTimeUTC": "Date(1587030776357)",'+
		       '  "Contacts": []'+
		       '}';
	}
	@testVisible
	static String multipleAccountsPush(){
		return '{'+
		       '  "Id": "01b5d16a-566c-448e-91cd-b4e2756842dc",'+
		       '  "Status": "OK",'+
		       '  "ProviderName": "Salesforce",'+
		       '  "DateTimeUTC": "Date(1587032477057)",'+
		       '  "Contacts": ['+
		       '    {'+
		       '      "ContactID": "ba6814c9-2670-42ad-acf0-a1d9ef465856",'+
		       '      "ContactStatus": "ACTIVE",'+
		       '      "Name": "Xero Account Missing Id",'+
		       '      "EmailAddress": "",'+
		       '      "BankAccountDetails": "",'+
		       '      "Addresses": ['+
		       '        {'+
		       '          "AddressType": "STREET",'+
		       '          "City": "",'+
		       '          "Region": "",'+
		       '          "PostalCode": "",'+
		       '          "Country": ""'+
		       '        },'+
		       '        {'+
		       '          "AddressType": "POBOX",'+
		       '          "City": "",'+
		       '          "Region": "",'+
		       '          "PostalCode": "",'+
		       '          "Country": ""'+
		       '        }'+
		       '      ],'+
		       '      "Phones": ['+
		       '        {'+
		       '          "PhoneType": "DEFAULT",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "DDI",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "FAX",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "MOBILE",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        }'+
		       '      ],'+
		       '      "UpdatedDateUTC": "Date(1587032476940+0000)",'+
		       '      "ContactGroups": [],'+
		       '      "IsSupplier": false,'+
		       '      "IsCustomer": false,'+
		       '      "SalesTrackingCategories": [],'+
		       '      "PurchasesTrackingCategories": [],'+
		       '      "ContactPersons": [],'+
		       '      "HasValidationErrors": false'+
		       '    },'+
		       '    {'+
		       '      "ContactID": "8b4d19c3-8fc9-475e-b962-165eac2b1974",'+
		       '      "ContactStatus": "ACTIVE",'+
		       '      "Name": "Xero Account Missing Id 2",'+
		       '      "EmailAddress": "",'+
		       '      "BankAccountDetails": "",'+
		       '      "Addresses": ['+
		       '        {'+
		       '          "AddressType": "STREET",'+
		       '          "City": "",'+
		       '          "Region": "",'+
		       '          "PostalCode": "",'+
		       '          "Country": ""'+
		       '        },'+
		       '        {'+
		       '          "AddressType": "POBOX",'+
		       '          "City": "",'+
		       '          "Region": "",'+
		       '          "PostalCode": "",'+
		       '          "Country": ""'+
		       '        }'+
		       '      ],'+
		       '      "Phones": ['+
		       '        {'+
		       '          "PhoneType": "DEFAULT",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "DDI",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "FAX",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        },'+
		       '        {'+
		       '          "PhoneType": "MOBILE",'+
		       '          "PhoneNumber": "",'+
		       '          "PhoneAreaCode": "",'+
		       '          "PhoneCountryCode": ""'+
		       '        }'+
		       '      ],'+
		       '      "UpdatedDateUTC": "Date(1587032477000+0000)",'+
		       '      "ContactGroups": [],'+
		       '      "IsSupplier": false,'+
		       '      "IsCustomer": false,'+
		       '      "SalesTrackingCategories": [],'+
		       '      "PurchasesTrackingCategories": [],'+
		       '      "ContactPersons": [],'+
		       '      "HasValidationErrors": false'+
		       '    }'+
		       '  ]'+
		       '}';
	}
}
