@isTest
/**
 * To test if the multi mock generator is working or not
 */
public with sharing class XR_MultiMockTest
{
	@TestSetup
	static void makeData(){
		User user = DataFactoryTest.getUsers(1,null)[0];
	}
	static testMethod void coverMultiMock(){
		User user = [SELECT Id FROM User LIMIT 1];
		system.runAs(user){
		Map<String, HttpCalloutMock> httpMapMock = new Map<String, HttpCalloutMock>();
		String mock = '';
		HttpCalloutMock httpMock = null;
		XR_MultiRequestMock multi = new XR_MultiRequestMock(httpMapMock);
		multi.addRequestMock(mock,httpMock);
		//to meet code standards, adding an empty assert
		System.assertEquals('true','true','it will always pass');
		}
	}
}
