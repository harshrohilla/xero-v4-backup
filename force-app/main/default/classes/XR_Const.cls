/**
 *  Any constants or hard coded values where custom settings cannot be used are in this class
 */
public with sharing class XR_Const
{
	public static String connectionEndpoint = 'Xero/connections';
	public static String customerEndpoint = 'Xero/api.xro/2.0/contacts';
	public static String invoiceEndpoint = 'Xero/api.xro/2.0/Invoices';
	public static String productEndpoint = 'Xero/api.xro/2.0/Items';
	public static String opportunitySyncFailed = 'Failed';
	public static String failed = 'Failed';
	public static String success = 'Success';
	public static String currencyISOCode = 'GBP';
	public static String xeroSyncDetailsWhenAccountNotFound = 'Missing Account in Xero with this Name.';
	public static String xeroSyncStatusWhenAccountNotFound = 'Failed';
	public static String accountNotAttachedToOpportunity = 'Please add an Account in order to Sync to Xero';
	public static String defaultPricebookName = 'Standard';
	public static String xeroInvoiceType = 'ACCREC';
	public static String unknownException = 'Unknown exception occured. Please contact you administrator';
	//set when sending Account from Salesforce
	//can be true/false
	public static String isCustomer = 'True';
	public static String isSupplier = 'False';
	//when a webhook inserts an opp in salesforce, stageName is a required field
	public static String defaultStageName = 'Closed Won';
	//when a webhook inserts an opp in salesforce, closeDate is a required field
	public static Date defaultCloseDate = System.today();
	public static String priceBookEntryNotFound = 'Please add a pricebook entry to product';
}