@isTest
/**
 * Test class to support LogUtils Test Coverage
 */ 
public with sharing class MetadataCoverageTest {
/**
 * testMetadataCoverageMethod Test Metadata Coverage
 */ 
   static testmethod void testMetadataCoverageMethod()
   {
       List<Sobject> metadataCoverageRecord;
       test.startTest();
       metadataCoverageRecord =new CustomMetadataDAO().getMetadataCoverageRecord(' SELECT Level__c,Log_to_Object__c,Active__c,Send_Email_To__c FROM Log_Setting__mdt');
       test.stopTest();
       System.assertEquals([SELECT Level__c,Log_to_Object__c,Active__c,Send_Email_To__c FROM Log_Setting__mdt ].size(),metadataCoverageRecord.size(),'Both size should be matched');
   }
    public static void setMetadata(String query,List<Sobject> records)
       {
        CustomMetadataDAO.MetadataCoverageRecordMap.put(query,records);
       }
}