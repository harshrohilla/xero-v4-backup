public inherited sharing class XR_InvoiceHandler
{
	XR_InvoiceHelper helper;

	public XR_InvoiceHandler(){
		helper = new XR_InvoiceHelper();
	}
	/**
	 * getCustomerMapFromXero On basis of invoices present in SF, fetch their customer details from xero
	 * @param                 Takes list of invoice records of which customer record is needed to be fetched on basis of account name
	 * @return                Returns map of customers from xero
	 */
	@testVisible
	/**
	 * getCustomerMapFromXero takes a list of opportunities, and return a corresponding map of related accounts in format<SalesforceId,XeroId>
	 * @param  oppList list of opportunities
	 * @return         return map<SalesforceId,XeroId> of related account
	 */
	private Map<String, String> getCustomerMapFromXero(List<Opportunity> oppList){
		//accounts whose xeroId is blank in salesforce will be put in this set
		Set<String> accountNameSet = new Set<String>();
		//accounts which were present in xero would be put in format<SalesforceId,XeroId>
		Map<String, String> accountNameMap = new Map<String, String>();
		//iterating through list of opportunities
		for(Opportunity opp: oppList) {
			if(String.isBlank(opp.Account.Xero_Id__c)) {
				accountNameSet.add(opp.Account.Name);
			}else {
				//if the xeroId was already present in account simply put them into map
				accountNameMap.put(opp.Account.Name, opp.Account.Xero_Id__c);
			}
		}
		if(accountNameSet.size() > 0) {
			//those accounts which do not have xero id are synced through helper and a map is returned
			accountNameMap.putAll(XR_CustomerHandler.getCustomerMapFromXero(accountNameSet));
		}
		return accountNameMap;
	}
	/**
	 * doPush takes a list of opportunities(invoices) and syncs them to xero
	 * @param  oppList list of opportunities to be synced
	 */
	public void doPush(List<Opportunity> oppList){
		List<Opportunity> validInvoiceList = new List<Opportunity>();
		List<Opportunity> invalidInvoiceList = new List<Opportunity>();
		//fetching Xero Settings custom setting to check whther push customer is checked or not
		Xero_Settings__c xeroCustomSetting = XR_Utils.fetchXeroSettings();
		Boolean insertContact = xeroCustomSetting.Auto_Insert_Contact__c;
		//Accounts already having Xero Ids are not included in this Map
		Map<String, String> customerMap = getCustomerMapFromXero(oppList);
		//when auto insert custom setting is false, do not insert the account if it is not already present in xero
		if(insertContact == False) {
			for(Opportunity opp: oppList) {
				//update last sync date in invoice record
				opp.Xero_Last_Sync_Date__c = System.now();
				//if customer is not found from name, add the record to invalidInvoiceList
				if(String.isBlank(opp.Xero_Account_Id__c) && !customerMap.containsKey(opp.Account.Name)) {
					opp.Xero_Sync_Details__c = XR_Const.xeroSyncDetailsWhenAccountNotFound;
					opp.Xero_Sync_Status__c = XR_Const.xeroSyncStatusWhenAccountNotFound;
					opp.Xero_Invoice_Type__c = XR_Const.xeroInvoiceType;
					invalidInvoiceList.add(opp);
					//else if customer found from name, add the record to validInvoiceList
				}else {
					opp.Xero_Invoice_Type__c = opp.Xero_Invoice_Type__c == null ? XR_Const.xeroInvoiceType : opp.Xero_Invoice_Type__c;
					validInvoiceList.add(opp);
				}
			}
			//when auto insert contact custom setting in True, insert the account as well if not present in xero
		}else{
			for(Opportunity opp: oppList) {
				//checking if the opportunity has an account attached, if not throw error to attach an account
				if (opp.Account != null) {
					opp.Xero_Last_Sync_Date__c = System.now();
					opp.Xero_Invoice_Type__c = opp.Xero_Invoice_Type__c == null ? XR_Const.xeroInvoiceType : opp.Xero_Invoice_Type__c;
					validInvoiceList.add(opp);
				}else {
					opp.Xero_Invoice_Type__c = opp.Xero_Invoice_Type__c == null ? XR_Const.xeroInvoiceType : opp.Xero_Invoice_Type__c;
					opp.Xero_Sync_Status__c = XR_Const.failed;
					opp.Xero_Sync_Details__c = XR_Const.accountNotAttachedToOpportunity;
					invalidInvoiceList.add(opp);
				}
			}
		}
		//will contain the list of line items to be updated in salesforce
		List<OpportunityLineItem> lineItemListUpdate = new List<OpportunityLineItem>();
		//will contain the list of line items to be upserted in salesforce
		List<OpportunityLineItem> lineItemListUpsert = new List<OpportunityLineItem>();

		Map<Id, Account> accountUpdateList = new Map<Id, Account>();

		if(validInvoiceList.size() > 0) {
			//post the valid invoices whose customer details were successfully fetched from xero, to xero and return response in wrapper
			XR_InvoiceHelper.InvoiceResultWrapper helperWrapper = helper.postInvoice(validInvoiceList);
			//if push is successful, and xero id is fetched, add that invoice to update list
			for(OpportunityLineItem line: helperWrapper.invoiceLineItemList) {
				if(line.Id != null) {
					lineItemListUpdate.add(line);
				}else if(line.Xero_Id__c != null) {
					lineItemListUpsert.add(line);
				}
			}

			for(Account acc: helperWrapper.accountList) {
				//when auto insert contact xero setting is set to false
				if(insertContact == False) {
					//Update only those Accounts whose Xero Id is now to be updated
					//Do not update, if it already exists
					if(customerMap.containsKey(acc.Name)) {
						accountUpdateList.put(acc.Id, acc);
					}
				}
				//when auto insert contact xero setting is set to true
				//update accounts
				else{
					accountUpdateList.put(acc.Id, acc);
				}
			}

			//update those accounts whose Xero ID is to be updated so fields are updated
			List<Database.SaveResult> saveResultAccountUpdateList = Database.update(accountUpdateList.values(), false);
			for(Database.SaveResult result: saveResultAccountUpdateList) {
				if (!result.success) {
					LogUtils.log(LoggingLevel.DEBUG, 'doPush', 'Invoice SYNC failed. '+result.getErrors(), result.getId());
				}
			}
			//update invoices in helper wrapper
			List<Database.SaveResult> saveResultHelperWrapper = Database.update(helperWrapper.invoiceList, false);
			for(Database.SaveResult result: saveResultHelperWrapper) {
				if (!result.success) {
					LogUtils.log(LoggingLevel.DEBUG, 'doPush', 'Invoice SYNC failed. ' + result.getErrors(), result.getId());
				}
			}
			//update invoice line item records
			List<Database.SaveResult> saveResultLineItemListUpdate = Database.update(lineItemListUpdate, false);
			for(Database.SaveResult result: saveResultLineItemListUpdate) {
				if (!result.success) {
					LogUtils.log(LoggingLevel.DEBUG, 'doPush', 'Invoice SYNC failed. '+result.getErrors(), result.getId());
				}
			}
			//upsert line items
			List<Database.UpsertResult> saveResultLineItemListUpsert = Database.upsert(lineItemListUpsert, OpportunityLineItem.Xero_Id__c, false);
			for(Database.UpsertResult result: saveResultLineItemListUpsert) {
				if (!result.success) {
					LogUtils.log(LoggingLevel.DEBUG, 'doPush', 'Invoice SYNC failed. '+result.getErrors(), result.getId());
				}
			}

			LogUtils.flush();
		}
		//once all valid invoices are synced, update the invalid invoices whose account was not found with error details from response
		if(Opportunity.SObject.sObjectType.getDescribe().isUpdateable()) {
			update invalidInvoiceList;
		}
	}
	/**
	 * doGet takes a list of xero invoiceIdsIds and sync them from xero to salesforce
	 * @param  invoiceXeroIdList list of xero invoiceIds
	 */
	public void doGet(Set<String> invoiceXeroIdList){
		XR_InvoiceHelper.InvoiceResultWrapper helperWrapper = new XR_InvoiceHelper.InvoiceResultWrapper();
		//to get invoices based on xero invoice id
		if (invoiceXeroIdList.size() >= 0) {
			helperWrapper = helper.getInvoiceByIds(invoiceXeroIdList);
		}
		List<Opportunity> oppList = new List<Opportunity>();
		//if records are fetched from xero, update them into salesforce accordingly
		//when the opportunity is not found in salesforce to update, a new opportunity would be inserted along with its related account
		Map<String,Opportunity> oppMap = new Map<String,Opportunity>();
		//For Account to be inserted to newly created opportunity, this account will be used to add new account in salesforce
		Account account = new Account();
		//this map will be used to add Name,StageName,CloseDate into opportunity to be inserted since these are required fields for insert
		for (Opportunity opp : XR_Utils.getOppFromSalesforce(invoiceXeroIdList)) {
			oppMap.put(opp.Xero_Id__c,opp);
		}
		//fetch xero settings
		Xero_Settings__c xeroCustomSetting = XR_Utils.fetchXeroSettings();
		Boolean insertContact = xeroCustomSetting.Insert_Contact_From_Webhook__c;

		if (helperWrapper != null) {
			//if account with the name from xero exists in salesforce, query that account to use further
			if(XR_Utils.getAccountsFromSalesforce(helperWrapper.accountList[0].Xero_Id__c) != null) {
				account = XR_Utils.getAccountsFromSalesforce(helperWrapper.accountList[0].Xero_Id__c);
				//if the auto insert contact custom setting is set to true, then only create a new account otherwise upsert invoice without an account
			} else if(insertContact == True) {
				//if account with that name doesnt exits, create a new account with same name as from xero
				account = new Account(Name = helperWrapper.accountList[0].Name);
				insert account;
			}
			//iterating through invoice wrapper
			for (Integer i = 0; i < helperWrapper.invoiceList.size(); i++) {
				//mapping opportunity record
				Opportunity oppRecord = new Opportunity();
				oppRecord.Xero_ID__c = helperWrapper.invoiceList[i].Xero_ID__c;
				oppRecord.Xero_Invoice_Status__c = helperWrapper.invoiceList[i].Xero_Invoice_Status__c;
				oppRecord.Xero_Sync_Status__c = helperWrapper.invoiceList[i].Xero_Sync_Status__c;
				oppRecord.Xero_Sync_Details__c = helperWrapper.invoiceList[i].Xero_Sync_Details__c;
				oppRecord.Xero_Sent_To_Contact__c = helperWrapper.invoiceList[i].Xero_Sent_To_Contact__c;
				//adding required fields for insert
				//if the opportunity was not found in Salesforce, a new opp will be inserted
				//if the opp doesnt exist in salesforce, it will enter in this if loop otherwise wont
				if(oppMap.size() == 0 || (oppMap.size() > 0 && !oppMap.containsKey(helperWrapper.invoiceList[i].Xero_Id__c))) {
					//in case of insert stagename is set to a default value set in XR_Const.apxc
					oppRecord.StageName =  XR_Const.defaultStageName;
					oppRecord.CloseDate = XR_Const.defaultCloseDate;
					//in case of insert, the invoice number from xero becomes the default name of invoice
					oppRecord.Name = helperWrapper.invoiceList[i].Xero_Invoice_Number__c;
					//now attaching account in to-be inserted opportunity
					//attaching account if not present in salesforce
					if(account != null) {
						oppRecord.AccountId = account.Id;
					}
				}
				oppList.add(oppRecord);
			}
			System.debug('oppList'+oppList);
			LogUtils.Log(LoggingLevel.DEBUG, 'Upserting Invoice ', oppList.size() + ' => ' + oppList);
			//fetch results from upserting records and log if any record gets fails to get updated
			XR_Utils.upsertByXeroId('Opportunity', oppList);

			//log upsert invoice succeeded
			LogUtils.Log(LoggingLevel.DEBUG, 'Upserting Invoice SUCCESS', '');
		}
	}
	/**
	 * updateInvoice fetch the invoice record from xero based on id came from payload and further update it in salesforce for real time sync
	 * @param        payLoad from xero webhook
	 */
	@future(callout = true)
	public static void updateInvoice(String payLoad){
		Set<String> invoiceListToUpdate = new Set<String>();
		//deserialze payload from xero and get response in wrapper
		XR_Data.WebhookResponse responseWrapper = (XR_Data.WebhookResponse)JSON.deserialize(payLoad, XR_Data.WebhookResponse.class);
		if (responseWrapper != null) {
			List<XR_Data.Events> eventsWrapperList = responseWrapper.Events;
			if(eventsWrapperList.size() > 0) {
				for(XR_Data.Events event : eventsWrapperList) {
					// if event is related to invoice and of update type
					if (event.eventCategory == 'INVOICE' && event.eventType == 'UPDATE') {
						//add the invoice id in list to be fetched from salesforce
						invoiceListToUpdate.add(event.resourceId);
					}
				}
				if (invoiceListToUpdate.size() > 0) {
					try {
						//get these invoices only and update in salesforce
						XR_InvoiceHandler handler = new XR_InvoiceHandler();
						handler.doGet(invoiceListToUpdate);
					}
					catch (Exception e)
					{
						LogUtils.log('Webhook', e);
					}
				}
			}
		}
		LogUtils.flush();
	}
}