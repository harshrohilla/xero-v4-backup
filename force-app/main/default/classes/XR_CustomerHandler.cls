/**
 * Handler class for XR_CustomerController
 */
public with sharing class XR_CustomerHandler
{
	XR_CustomerHelper helper;

	public XR_CustomerHandler(){
		helper = new XR_CustomerHelper();
	}
	/**
	 * getCustomerMapFromXero takes Account Names and searches for those Accounts in Xero,
	 *                        if found returns a map of Account Name and respetive Xero Id
	 * @param  accountNameSet Set of AccountNames to be searched for in Xero
	 * @return                returns map of AccountName and corresponding Xero Id
	 */
	public static Map<String, String> getCustomerMapFromXero(Set<String> accountNameSet){
		Map<String, String> accountNameMap = new Map<String, String>();
		//if given AccountNameSet is not empty
		if(accountNameSet.size() > 0) {
			XR_CustomerHelper helper = new XR_CustomerHelper();
			List<XR_Data.Contact> customerWrapperList = helper.getCustomersByName(accountNameSet);
			if(customerWrapperList != null && customerWrapperList.size() > 0) {
				for(XR_Data.Contact cWRapper: customerWrapperList) {
					//put values of account name and respective account xero id in map to return
					accountNameMap.put(cWRapper.Name, cWRapper.ContactID);
				}
			}
		}
		return accountNameMap;
	}
	/**
	 * doPush take list of accounts from controller and push them to xero
	 * @param  accList list of accounts
	 */
	public void doPush(List<Account> accList){
		//this set will contain the AccountNames whose Xero_ID is missing in SF but account in present in Xero
		Set<String> accNameSet = new Set<String>();
		//this map will contain the AccountName and XeroID 
		Map<String,String> accountNameMap = new Map<String,String>();
		//this List will contain accounts that will be pushed to xero 
		List<Account> accountListToPush = new List<Account>();
		//this List will contain accounts updated with xeroid without pushing account to xero
		//meaning account was already present in xero
		List<Account> accountListToUpdate = new List<Account>();
		//this list will contain the updated accounts those were pushed to xero
		List<Account>  accountListToUpdateFromXero = new List<Account> ();
		if(accList.size() > 0) {
			for (Account acc : accList) {
				if(String.isBlank(acc.Xero_Id__c)) {
					accNameSet.add(acc.Name);
				}else {
					accountNameMap.put(acc.Name, acc.Xero_Id__c);
				}
			}
			if(accNameSet.size() > 0) {
				//get accountName,XeroId map from xero 
				accountNameMap.putAll(getCustomerMapFromXero(accNameSet));
			}

			for(Account acc : accList) {
				//if neither xeroid is present in account nor account is present in xero,
				//these accounts will be pushed to xero 
				if(String.isBlank(acc.Xero_Id__c) && String.isBlank(accountNameMap.get(acc.Name))) {
					accountListToPush.add(acc);
				}else {
					//if either xeroId was there in account or account was present in xero,
					//simply update those accounts by adding xeroid to it
					acc.Xero_Id__c = accountNameMap.get(acc.Name);
					acc.Xero_Sync_Status__c = XR_Const.success;
					acc.Xero_Sync_Details__c = '';
					accountListToUpdate.add(acc);
				}
			}
			//push the accounts to xero
			if (accountListToPush.size() > 0) {
				accountListToUpdateFromXero = helper.doPush(accountListToPush);
			}
            //update the accounts those were not pushed to xero but their xero id was added to them
			List<Database.SaveResult> saveResultAccountUpdateList = Database.update(accountListToUpdate, false);
			for(Database.SaveResult result: saveResultAccountUpdateList) {
				if (!result.success) {
					LogUtils.log(LoggingLevel.DEBUG, 'doPush', 'Customer SYNC failed. '+result.getErrors(), result.getId());
				}
			}
			//update the accounts those were pushed to xero and response was fetched back
			List<Database.SaveResult> saveResultAccountUpdateListFromXero = Database.update(accountListToUpdateFromXero, false);
			for(Database.SaveResult result: saveResultAccountUpdateListFromXero) {
				if (!result.success) {
					LogUtils.log(LoggingLevel.DEBUG, 'doPush', 'Customer SYNC failed. '+result.getErrors(), result.getId());
				}
			}
		}
	}
}