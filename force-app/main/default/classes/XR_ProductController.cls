/**
 * @File Name          : XR_ProductController.cls
 * @Description        : Controller class for Products sync to Xero
 * @Author             : Harsh
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    13/04/2020      Harsh                Initial Version
 */
public with sharing class XR_ProductController
{
	XR_ProductHandler handler;
	List<Product2> prodList {get;
		                 set;
	}
	//fetch Product Id if sync initiated from Detail page i.e. single product is to be synced
	public XR_ProductController(ApexPages.StandardController controller) {
		String prodId = ApexPages.CurrentPage().getparameters().get('id');
		init(new Set<Id> {prodId});
	}
	//fetch the List of productids to be synced to xero, from List View
	public XR_ProductController(ApexPages.StandardSetController controller) {
		Set<Id> productIdSet = new Set<Id>();
		for (Product2 prod : (List<Product2>)controller.getSelected()) {
			productIdSet.add(prod.Id);
		}
		init(productIdSet);
	}
	/**
	 * init initialize the Products captuired from StandardController i.e. query them into a list of products
	 * @param  prodIds Set if Product2 ids
	 */
	private void init(Set<Id> prodIds){
		//fetching pricebook to be used from custom setting
		Xero_Settings__c xeroCustomSetting = Xero_Settings__c.getInstance();
		//if priceBook name is absent in custom setting, use default pricebook specified in XR_Const.apxc
		String priceBookName = xeroCustomSetting.Pricebook_Name__c == null ? XR_Const.defaultPricebookName : xeroCustomSetting.Pricebook_Name__c;
		//check if this pricebook exists
		List<Pricebook2> pbList = [SELECT Name FROM Pricebook2 WHERE Name =: priceBookName AND isActive =: True LIMIT 1];
		//if pricebook exists, continue sync
		if(pbList.size() > 0) {
			//query the product from Salesforce and continue sync process
			this.prodList = [SELECT Id,Name,StockKeepingUnit,Description,
			                 Account_Code__c,Xero_Is_Purchased__c,Xero_Is_Sold__c,
			                 Xero_Is_Tracked_As_Inventory__c,Xero_Quantity_On_Hand__c,
			                 Tax_Type__c,Xero_Id__c,Xero_Sync_Details__c,Xero_Sync_Status__c,
			                 (SELECT Id, Name, pricebook2.Id, pricebook2.name,UnitPrice
			                  FROM PricebookEntries
			                  WHERE pricebook2.name =: priceBookName
			                                          AND isActive =:True )
			                 FROM Product2
			                 WHERE Id IN: prodIds];
			handler = new XR_ProductHandler();
		} else{
			//if pricebook specified in custom setting doesnt exist in salesforce, log and halt
			this.prodList = new List<Product2>();
			LogUtils.log(LoggingLevel.ERROR, 'Pricebook Error', 'Pricebook in Xero Custom Setting do not exist');
		}
	}
	public PageReference doPush(){
		if (prodList.size() > 0) {
			//send initialized products to Handler in order to handle the SYNC
			System.debug('this.prodList'+this.prodList);
			handler.doPush(this.prodList);
		}
		//return the functionality to a specific page to land user somewhere
		//if sync took place from detail page, redirect back to that detail page
		if(prodList.size() == 1) {
			//pagereference will take place from vfPage, refreshing the record
			return new PageReference('/' + prodList[0].Id);
		}
		//if sync took place from list view button, redirect back to Product2 object List View
		else{
			return new PageReference('/' + Product2.sObjectType.getDescribe().getKeyPrefix());
		}
	}
}