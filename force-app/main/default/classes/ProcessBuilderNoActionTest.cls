/*
 * Test class for ProcessBuilderNoAction
 * 
 * @date    2020-04-15
 * @author  vanguard
 */

@isTest
public class ProcessBuilderNoActionTest {
    /**
     * @method doNothing
     * Its a blank method 
     */
    
    @isTest
    static void doNothingTest(){
        ProcessBuilderNoAction.doNothing();
        //for PMD code quality
        System.assertEquals('Salesforce','Salesforce','Salesforce');
    }
}