@isTest
public with sharing class XR_AuthenticationTest
{
	@TestSetup
	static void makeData(){
		User user = DataFactoryTest.getUsers(1,null)[0];
	}
	static testMethod void testAuthFailure(){
		User user = [SELECT Id FROM User LIMIT 1];
		system.runAs(user){
			Test.startTest();
			XR_MockDataTest.prepareMockDataAuthFailure();
			XR_AuthHandler handler = new XR_AuthHandler();
			String tenantId = handler.getTennantId();
			Test.stopTest();
			System.assertEquals(tenantId, null,'AuthFailure testMethod failed');
		}
	}
}
