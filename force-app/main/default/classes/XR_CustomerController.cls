public with sharing class XR_CustomerController
{
	XR_CustomerHandler handler;
	List<Account> accList {get; set;}
	//fetch account id if sync initiated from detail page button
	public XR_CustomerController(ApexPages.StandardController controller){
		String accId = ApexPages.CurrentPage().getparameters().get('id');
		init(new Set<Id> {accId});
	}
	//fetch the List of accountids to be synced to xero, from List View
	public XR_CustomerController(ApexPages.StandardSetController controller) {
		Set<Id> accountIdSet = new Set<Id>();
		for (Account acc : (List<Account>)controller.getSelected()) {
			accountIdSet.add(acc.Id);
		}
		init(accountIdSet);
	}
	//initialize the accounts captured from constructors into class level list
	private void init(Set<Id> accIds){
		this.accList = [SELECT Id, Name, Xero_Id__c,Xero_Sync_Details__c,Xero_Sync_Status__c
		                FROM Account WHERE Id IN: accIds];
		handler = new XR_CustomerHandler();
	}
	//push the accounts in this.accList to Xero
	public PageReference doPush(){
		if(this.accList.size() > 0) {
			handler.doPush(accList);
		}
		//if sync took place from detail page, redirect back to that detail page
		if(accList.size() == 1) {
			//pagereference will take place from vfPage, refreshing the record
			return new PageReference('/' + accList[0].Id);
			//return null;
		}
		//if sync took place from list view button, redirect back to account object
		else{
			return new PageReference('/' + Account.sObjectType.getDescribe().getKeyPrefix());
		}
	}
}
