/**
 * @File Name          : VG_XeroData.cls
 * @Description        :
 * @Author             : Gulshan
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    06/01/2020      Gulshan                Initial Version
 **/
public with sharing class XR_Data
{
	public class ValidationErrors
	{
		public String message {get; set;}
	}
	public class Phones
	{
		public String phoneType {get; set;}
		public String phoneNumber {get; set;}
		public String phoneAreaCode {get; set;}
		public String phoneCountryCode {get; set;}
	}
	public class Addresses
	{
		public String addressType {get; set;}
		public String city {get; set;}
		public String region {get; set;}
		public String postalCode {get; set;}
		public String country {get; set;}
	}
	public class Prepayments
	{}

	public class Contact
	{
		public String contactID {get; set;}
		public String contactStatus {get; set;}
		public String name {get; set;}
		public String emailAddress {get; set;}
		public String bankAccountDetails {get; set;}
		public List<Addresses> addresses {get; set;}
		public List<Phones> phones {get; set;}
		public String updatedDateUTC {get; set;}
		public List<Prepayments> contactGroups {get; set;}
		public String isSupplier {get; set;}
		public String isCustomer {get; set;}
		public String defaultCurrency {get; set;}
		public List<Prepayments> salesTrackingCategories {get; set;}
		public List<Prepayments> purchasesTrackingCategories {get; set;}
		public List<Prepayments> contactPersons {get; set;}
		public Boolean hasValidationErrors {get; set;}
		public List<ValidationErrors> validationErrors {get; set;}
	}
	public class Invoice
	{
		public String type {get; set;}
		public String invoiceID {get; set;}
		public String invoiceNumber {get; set;}
		public String reference {get; set;}
		public List<Prepayments> creditNotes {get; set;}
		public List<Prepayments> payments {get; set;}
		public List<Prepayments> prepayments {get; set;}
		public List<Prepayments> overpayments {get; set;}
		public Decimal amountDue {get; set;}
		public Decimal amountPaid {get; set;}
		public Decimal amountCredited {get; set;}
		public Decimal cISDeduction {get; set;}
		public Boolean sentToContact {get; set;}
		public Decimal currencyRate {get; set;}
		public Boolean isDiscounted {get; set;}
		public Boolean hasErrors {get; set;}
		public Contact contact {get; set;}
		public String dateString {get; set;}
		public String date1 {get; set;}
		public String dueDateString {get; set;}
		public String dueDate {get; set;}
		public String expectedPaymentDate {get; set;}
		public String plannedPaymentDate {get; set;}
		public String fullyPaidOnDate {get; set;}
		public String expectedPaymentDateString {get; set;}
		public String plannedPaymentDateString {get; set;}
		public String fullyPaidOnDateString {get; set;}
		public String status {get; set;}
		public String lineAmountTypes {get; set;}
		public List<LineItem> lineItems {get; set;}
		public Decimal subTotal {get; set;}
		public Decimal totalTax {get; set;}
		public Decimal total {get; set;}
		public Decimal totalDiscount {get; set;}
		public String updatedDateUTC {get; set;}
		public String currencyCode {get; set;}
		public List<ValidationErrors> validationErrors {get; set;}
		public Boolean hasAttachments {get; set;}
		public String url {get; set;}
	}
	public class CreditNote
	{
		public String type {get; set;}
		public Contact contact {get; set;}
		public String date1 {get; set;}
		public String status {get; set;}
		public String lineAmountTypes {get; set;}
		public List<LineItem> lineItems {get; set;}
		public Decimal subTotal {get; set;}
		public Decimal totalTax {get; set;}
		public Decimal total {get; set;}
		public String updatedDateUTC {get; set;}
		public String currencyCode {get; set;}
		public String fullyPaidOnDate {get; set;}
		public String creditNoteID {get; set;}
		public String creditNoteNumber {get; set;}
		public String reference {get; set;}
		public Boolean sentToContact {get; set;}
		public Decimal currencyRate {get; set;}
		public Decimal remainingCredit {get; set;}
		public List<Allocation> allocations {get; set;}
		public Boolean hasAttachments {get; set;}
		public List<ValidationErrors> validationErrors {get; set;}
		public Decimal appliedAmount {get; set;}
		public Boolean hasErrors {get; set;}
	}
	public class Allocation
	{
		public List<ValidationErrors> validationErrors {get; set;}
		public Boolean hasErrors {get; set;}
		public Decimal amount {get; set;}
		public Invoice invoice {get; set;}
		public CreditNote creditNote {get; set;}
		public String date1 {get; set;}
		public String updatedDateUTC {get; set;}
	}
	public class LineItem
	{
		public Decimal unitAmount {get; set;}
		public String description {get; set;}
		public String taxType {get; set;}
		public Decimal taxAmount {get; set;}
		public String accountCode {get; set;}
		public Decimal lineAmount {get; set;}
		public List<Prepayments> tracking {get; set;}
		public Decimal discountRate {get; set;}
		public Decimal discountAmount {get; set;}
		public Decimal quantity {get; set;}
		public String lineItemID {get; set;}
		public String itemCode {get; set;}
	}
	public class XeroResponse
	{
		public String id {get; set;}
		public String status {get; set;}
		public String providerName {get; set;}
		public String dateTimeUTC {get; set;}
		public List<Invoice> invoices {get; set;}
		public List<Contact> contacts {get; set;}
		public List<Item> items {get; set;}
		public List<CreditNote> creditNotes {get; set;}
		public Allocation allocation {get; set;}
	}
	//wrapper for update from xero from webhook starts here
	public class WebhookResponse
	{
		public List<Events> events;
		public Integer firstEventSequence;
		public Integer lastEventSequence;
		public String entropy;
	}
	public class Events
	{
		public String resourceUrl;
		public String resourceId;
		public String eventDateUtc;
		public String eventType;
		public String eventCategory;
		public String tenantId;
		public String tenantType;
	}
	//wrapper used when sending product from salesforce to xero
	public class Item
	{
		public String itemID;
		public String code;
		public String name;
		public boolean isSold;
		public boolean isPurchased;
		public String description;
		public String purchaseDescription;
		public SalesDetails salesDetails;
		public boolean isTrackedAsInventory;
		public String inventoryAssetAccountCode;
		public Decimal quantityOnHand;
		public String updatedDateUTC;
		public List<ValidationErrors> validationErrors {get; set;}
	}
	public class SalesDetails
	{
		public Decimal unitPrice;
		public String accountCode;
		public String taxType;
		public List<ValidationErrors> validationErrors {get; set;}
	}
	//
	// When items (products) syncs successfully, the response is catched in this wrapper
	//
	public class SuccessProductResponse
	{
		public String id;
		public String status;
		public String providerName;
		public String dateTimeUTC;
		public List<Items> items;
	}
	public class Items
	{
		public String itemID;
		public String code;
		public String description;
		public String updatedDateUTC;
		public PurchaseDetails purchaseDetails;
		public SalesDetails salesDetails;
		public String name;
		public Boolean isTrackedAsInventory;
		public Boolean isSold;
		public Boolean isPurchased;
	}
	public class PurchaseDetails
	{
		public String taxType {get; set;}
		public List<ValidationErrors> validationErrors {get; set;}
	}
	//when product sync has error (xero to salesforce response)
	//separate wrappers used because xero sends success and errors in different jsons
	public class ResponseWithError
	{
		public Integer errorNumber {get; set;}
		public String type_Z {get; set;}
		// in json: Type
		public String message {get; set;}
		public List<Elements> elements {get; set;}
	}
	public class Elements
	{
		public String itemID {get; set;}
		public String code {get; set;}
		public String description {get; set;}
		public String updatedDateUTC {get; set;}
		public PurchaseDetails purchaseDetails {get; set;}
		public SalesDetails salesDetails {get; set;}
		public String name {get; set;}
		public Boolean isTrackedAsInventory {get; set;}
		public Boolean isSold {get; set;}
		public Boolean isPurchased {get; set;}
		public List<ValidationErrors> validationErrors {get; set;}
	}
}