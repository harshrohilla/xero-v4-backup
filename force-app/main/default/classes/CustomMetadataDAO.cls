/**
 * @File Name          : CustomMetadataDAO.cls
 * @Description        : Utility class to support LogUtils Test Classes
 * @Author             : Crossdoor
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    06/01/2020      Gulshan                Initial Version
 */
public inherited sharing class CustomMetadataDAO 
{
	@testVisible
	static private Map<String,List<sObject> > metadataCoverageRecordMap = new Map<String,List<sObject> >();
    /**
	 * getMetadataCoverageRecord get Metadata Coverage
	 * @param  query query 
	 * @return       return List<sObject>
	 */ 
	public List<SObject> getMetadataCoverageRecord(String query) {
		if ( !metadataCoverageRecordMap.containsKey( query ) ) {
			metadataCoverageRecordMap.put( query, Database.query( query ) );
		}
		return metadataCoverageRecordMap.get( query );
	}
}