@isTest
/**
 * Class to test Invoice Sync from Xero
 */
public with sharing class XR_InvoiceTest
{
	@TestSetup
	public static void makeData(){
		//account with Xero Id
		Account account = new Account(Name = 'Xero Account');
		account.Xero_Id__c = '67b8620b-e7df-4bc3-9c44-af03c2ab11ae';
		insert account;
		//account without xero id
		Account accountWithMissingXeroId = new Account(Name = 'Xero Account Missing Id');
		accountWithMissingXeroId.Xero_Id__c = '';
		insert accountWithMissingXeroId;
		//opportunity with Account having xero id
		Opportunity opportunity = new Opportunity(AccountId = account.Id,
		                                          Name = 'Test Invoice',
		                                          Xero_Invoice_Type__c = 'ACCREC',
		                                          StageName = 'Closed Won',
		                                          CloseDate = System.today()
		                                          );
		insert opportunity;
		//opportunity with account's Xero_Id missing
		Opportunity opportunity2 = new Opportunity(AccountId = accountWithMissingXeroId.Id,
		                                           Name = 'Test Invoice2',
		                                           Xero_Invoice_Type__c = 'ACCREC',
		                                           StageName = 'Closed Won',
		                                           CloseDate = System.today()
		                                           );
		insert opportunity2;
		//standard pricebook
		List<PricebookEntry> standardPrice = DataFactoryTest.getProductsPriceBookEntry('Sample Product',500,'XERO01',1);
		insert standardPrice;
		//xero custom setting record
		Xero_Settings__c cs = new Xero_Settings__c();
		cs.Webhook_Id__c = 'QjPGOyzdFl9ecVUSb1Vyxs1gKHu+ET+rSMFzzyOPzGfOCcgGdtdGMhXl5npjk/GRlDB5Q1Wfatvur4LOh1mIbw==';
		cs.SetupOwnerId = UserInfo.getOrganizationId();
		insert cs;
		//create user to use in System.runAs()
		User user = DataFactoryTest.getUsers(1,null)[0];
	}
	/**
	 * testPushSingleInvoice Invoice record created and pushed to xero is correctly updated back with the response
	 */
	static testmethod void testPushSingleInvoice(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			//account with Xero ID
			Account account = [SELECT Id FROM Account WHERE Name = 'Xero Account'];
			//pricebook entry for a product to be made available to opportunity line item
			PricebookEntry priceBook = [SELECT Id FROM PricebookEntry LIMIT 1];
			Opportunity opportunity = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Invoice' LIMIT 1];
			List<Opportunity> invList = new List<Opportunity>();
			invList.add(opportunity);
			List<OpportunityLineItem> lineItemList = new List<OpportunityLineItem>();

			OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = opportunity.Id,
			                                                    PricebookEntryId = priceBook.Id
			                                                    );
			line1.Quantity = 1;
			line1.UnitPrice = 1800;
			line1.Description = 'Onsite project management';
			line1.Xero_Account_Code__c = '200';
			insert line1;

			Test.startTest();
			//mock response
			XR_MockDataTest.prepareMockDataToPushInvoice(invList);
			//provide the opportunityId to controller in order to start SYNC
			PageReference pageRef = Page.xr_invoice_sync;
			//set page
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id',opportunity.id);

			ApexPages.StandardController stController = new ApexPages.StandardController(opportunity);
			XR_InvoiceController controller = new XR_InvoiceController(stController);
			//push the opportunity
			controller.doPush();
			Test.stopTest();
			//fetch the updated records to assert
			Opportunity updOpp = [SELECT Account.Xero_Id__c,
			               Account.Name,
			               Xero_Id__c,
			               CloseDate,
			               Xero_Expected_Payment_Date__c,
			               Xero_Fully_Paid_On_Date__c,
			               Id,
			               Xero_Last_Sync_Date__c,
			               Name,
			               Xero_Planned_Payment_Date__c,
			               Xero_Invoice_Status__c,
			               Xero_Sync_Details__c,
			               Xero_Sync_Status__c,
			               Amount,
			               Xero_Invoice_Type__c,
			               Xero_Account_Id__c,
			               Xero_Invoice_Number__c,
			               Invoice_Due_Date__c,
				       (
					       SELECT Xero_Account_Code__c,
					       Discount,
					       Id,
					       OpportunityId,
					       TotalPrice,
					       Name,
					       Quantity,
					       Xero_Tax_Amount__c,
					       Xero_Tax_Type__c,
					       UnitPrice,
					       Xero_Id__c
					       FROM OpportunityLineItems
			               )
			               FROM Opportunity
			               WHERE Id =: opportunity.Id];
            System.debug('updOpp'+updOpp);
			// Invoice Fields
			System.assertEquals(null, updOpp.Xero_Sync_Details__c, 'Invalid Invoice Sync Details');
			System.assertEquals('243216c5-369e-4056-ac67-05388f86dc81',updOpp.Xero_Id__c,'Xero Id didnot sync');
			System.assertEquals('ACCREC', updOpp.Xero_Invoice_Type__c, 'Invalid Invoice Type');

			//Line Items Fields
			System.assertEquals('52208ff9-528a-4985-a9ad-b2b1d4210e38',updOpp.OpportunityLineItems[0].Xero_Id__c,'Line Item Id incorrect');
			System.assertEquals(1.0000, updOpp.OpportunityLineItems[0].Quantity, 'Invalid Invoice Line Item Quantity');
			System.assertEquals(1800.00, updOpp.OpportunityLineItems[0].UnitPrice, 'Invalid Invoice Line Item Unit Amount');
			System.assertEquals('200', updOpp.OpportunityLineItems[0].Xero_Account_Code__c, 'Invalid Invoice Line Item Account Code');
			System.assertEquals('Test Invoice Sample Product', updOpp.OpportunityLineItems[0].Name, 'Invalid Invoice Line Item Description');
			System.assertEquals(1800.00, updOpp.OpportunityLineItems[0].TotalPrice, 'Invalid Invoice Line Amount');
		}
	}/**
	 * testPushInvoiceList push bulk invoices to test the bulkify-ablity of code
	 */
	static testMethod void testPushInvoiceList(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			List<Opportunity> invoiceToInsert = new List<Opportunity>();
			List<OpportunityLineItem> invLineItemToInsert = new List<OpportunityLineItem>();

			Account account = [SELECT Id FROM Account WHERE Name = 'Xero Account'];
			Product2 product = [SELECT Id,Name FROM Product2 WHERE Name = 'Sample Product'];
			PricebookEntry priceBook = [SELECT Id FROM PricebookEntry LIMIT 1];

			Opportunity invoice1 = new Opportunity(AccountId = account.Id,
			                                       Name = 'Test Invoice',
			                                       Xero_Invoice_Type__c = 'ACCREC',
			                                       CloseDate = System.today(),
			                                       StageName = 'Closed Won'
			                                       );
			Opportunity invoice2 = invoice1.clone(false,false,false,false);

			invoiceToInsert.add(invoice2);
			invoiceToInsert.add(invoice1);

			insert invoiceToInsert;

			OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = invoice1.Id,
			                                                    PricebookEntryId = priceBook.Id
			                                                    );
			line1.Quantity = 1;
			line1.UnitPrice = 1800;
			line1.Description = 'Onsite project management';
			line1.Xero_Account_Code__c = '200';
			invLineItemToInsert.add(line1);
			OpportunityLineItem line2 = line1.clone(false,false,false,false);
			OpportunityLineItem line3 = line1.clone(false,false,false,false);
			invLineItemToInsert.add(line2);
			invLineItemToInsert.add(line3);


			OpportunityLineItem line22 = line1.clone(false,false,false,false);
			line22.OpportunityId = invoice2.Id;
			invLineItemToInsert.add(line22);

			OpportunityLineItem line33 = line22.clone(false,false,false,false);
			invLineItemToInsert.add(line33);

			insert invLineItemToInsert;

			Test.startTest();

			XR_MockDataTest.prepareMockDataToPushInvoice(invoiceToInsert);

			PageReference pageRef = Page.xr_invoice_sync;
			Test.setCurrentPage(pageRef);

			ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(invoiceToInsert);
			stdSetController.setSelected(invoiceToInsert);
			XR_InvoiceController ext = new XR_InvoiceController(stdSetController);

			ext.doPush();

			Test.stopTest();

			list<Opportunity> updatedInvoiceList = [SELECT Account.Xero_Id__c,
			                                        Account.Name,
			                                        Xero_Id__c,
			                                        CloseDate,
			                                        Xero_Expected_Payment_Date__c,
			                                        Xero_Fully_Paid_On_Date__c,
			                                        Id,
			                                        Xero_Last_Sync_Date__c,
			                                        Name,
			                                        Xero_Planned_Payment_Date__c,
			                                        Xero_Invoice_Status__c,
			                                        Xero_Sync_Details__c,
			                                        Xero_Sync_Status__c,
			                                        Amount,
			                                        Xero_Invoice_Type__c,
			                                        Xero_Account_Id__c,
			                                        Xero_Invoice_Number__c,
			                                        Invoice_Due_Date__c,
								(
									SELECT Xero_Account_Code__c,
									Discount,
									Id,
									OpportunityId,
									TotalPrice,
									Name,
									Quantity,
									Xero_Tax_Amount__c,
									Xero_Tax_Type__c,
									UnitPrice,
									Xero_Id__c
									FROM OpportunityLineItems
			                                        )
			                                        FROM Opportunity WHERE Account.Name = 'Xero Account'];

			//check invoice 1
			System.assertEquals(null, updatedInvoiceList[1].Xero_Sync_Details__c, 'Invalid Invoice Sync Details');
			System.assertEquals('ACCREC', updatedInvoiceList[1].Xero_Invoice_Type__c, 'Invalid Invoice Type');
			System.assertEquals('1d8c6e1a-2351-467b-b187-c2adec5303c8', updatedInvoiceList[1].Xero_ID__c, 'Invalid Xero Invoice Id');
			System.assertEquals('67b8620b-e7df-4bc3-9c44-af03c2ab11ae', updatedInvoiceList[1].Xero_Account_Id__c, 'Invalid Invoice Xero_Account_Id__c');

			//check invoice 2
			System.assertEquals(null, updatedInvoiceList[2].Xero_Sync_Details__c, 'Invalid Invoice Sync Details');
			System.assertEquals('ACCREC', updatedInvoiceList[2].Xero_Invoice_Type__c, 'Invalid Invoice Type');
			System.assertEquals('2bd5f5ee-fde5-4550-bc9e-5dd8f208668b', updatedInvoiceList[2].Xero_ID__c, 'Invalid Xero Invoice Id');
			System.assertEquals('67b8620b-e7df-4bc3-9c44-af03c2ab11ae', updatedInvoiceList[2].Xero_Account_Id__c, 'Invalid Invoice Xero_Account_Id__c');
		}
	}

	/**
	 * testTenantId Tenant Id id correctly fetched from callout
	 */
	static testMethod void testTenantId(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			Test.startTest();
			List<Opportunity> invList = new List<Opportunity>();

			XR_MockDataTest.prepareMockDataToPushInvoice(invList);

			XR_InvoiceHelper help =  new XR_InvoiceHelper();
			string tenantId = help.getTennantId();
			Test.stopTest();

			System.assertEquals('6d683406-923d-4015-85e0-546106b85db6', tenantId, 'Invalid Tenant Id');
		}
	}
	/**
	 * testGetAccount Test the SYNC of accounts (customers)
	 */
	static testMethod void testGetAccount(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			Account account = [Select Id from Account where Name = 'Xero Account'];
			Set<String> accountNameSet = new Set<String>();
			accountNameSet.add('Xero Account');
			Opportunity opportunity = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Invoice' LIMIT 1];

			Test.startTest();
			XR_MockDataTest.prepareMockDataForGetCustomers();
			//set Account Name and Xero ID in a map to upsert
			//getCustomerMapFromXero() method returns a map<accountName,Account_XeroID>
			Map<String, String> accountNameMap = XR_CustomerHandler.getCustomerMapFromXero(accountNameSet);
			Test.stopTest();
			//test if Xero Id is updated in account or not, if yes then Account was successfully synced
			System.assertEquals('bd2270c3-8706-4c11-9cfb-000b551c3f51', accountNameMap.get('Xero Account'), 'invalid response');
		}
	}
	/**
	 * testWebhook test the incoming Invoice data through webhook from XERO
	 */
	static testMethod void testWebhook(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			//create a complete invoice to be updated from Xero through webhook sync
			Account account = [Select Id from Account where Name = 'Xero Account'];
			Opportunity invoice = [SELECT Id, Name FROM Opportunity LIMIT 1];
			invoice.Xero_Id__c = '2bd5f5ee-fde5-4550-bc9e-5dd8f208668b';
			invoice.Xero_Invoice_Status__c = 'DRAFT';
			invoice.Xero_Invoice_Type__c = 'ACCREC';
			update invoice;

			Test.startTest();
			//create a request immitating request from Xero via webhook
			RestRequest request = new RestRequest();
			request.requestUri = '/invoice';
			request.resourcePath = 'resourcePath=/services/apexrest/invoice/*';
			request.httpMethod = 'POST';
			request.addHeader('x-xero-signature','YJwAT3zi0Ukb+4ComKkYT1Wasp9pZqDBIPzvX8QNrMk=');
			request.addHeader('Content-Type','application/json');
			request.addHeader('charset','UTF-8');
			request.requestBody = blob.valueOf(XR_MockDataTest.responseFromWebhook());

			RestContext.request = request;
			String encodedPayload = 'YJwAT3zi0Ukb+4ComKkYT1Wasp9pZqDBIPzvX8QNrMk=';
			String xeroSignature = 'YJwAT3zi0Ukb+4ComKkYT1Wasp9pZqDBIPzvX8QNrMk=';

			RestResponse response = new RestResponse();
			RestContext.response = response;
			//call webhook method
			XR_InvoiceRESTHandler.doPost();
			//mock request
			XR_MockDataTest.prepareMockDataToGetInvoiceFromXeroId();

			Test.stopTest();
			//fetch updated opportunity to assert
			Opportunity opportunity = [SELECT Account.Xero_Id__c,
			                           Account.Name,
			                           Xero_Id__c,
			                           CloseDate,
			                           Xero_Expected_Payment_Date__c,
			                           Xero_Fully_Paid_On_Date__c,
			                           Id,
			                           Xero_Last_Sync_Date__c,
			                           Name,
			                           Xero_Planned_Payment_Date__c,
			                           Xero_Invoice_Status__c,
			                           Xero_Sync_Details__c,
			                           Xero_Sync_Status__c,
			                           Amount,
			                           Xero_Invoice_Type__c,
			                           Xero_Account_Id__c,
			                           Xero_Invoice_Number__c,
			                           Invoice_Due_Date__c,
						   (
							   SELECT Xero_Account_Code__c,
							   Discount,
							   Id,
							   OpportunityId,
							   TotalPrice,
							   Name,
							   Quantity,
							   Xero_Tax_Amount__c,
							   Xero_Tax_Type__c,
							   UnitPrice,
							   Xero_Id__c
							   FROM OpportunityLineItems
			                           )
			                           FROM Opportunity
			                           WHERE Account.Name = 'Xero Account'];

			//Status should get updated to PAID
			System.assertEquals('PAID', opportunity.Xero_Invoice_Status__c, 'Invalid Status');
			System.assertEquals('Success',opportunity.Xero_Sync_Status__c, 'Sync status incorrect');
		}
	}
	/**
	 * To test an Opportunity WITH Missing Xero fields should Fail and Opportunity should be updated back with Fail results.
	 * In this test case, Account Name is not present in Xero and XeroID in blank in Account. Ideally it should fail
	 */
	static testMethod void missingXeroFieldsPush(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			//Account which is never synced with xero
			Account account = [SELECT Id FROM Account WHERE Name = 'Xero Account Missing Id'];
			PricebookEntry priceBook = [SELECT Id FROM PricebookEntry LIMIT 1];
			//Opportunity has unsynced account
			Opportunity opportunity = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Invoice2' LIMIT 1];
			List<OpportunityLineItem> lineItemList = new List<OpportunityLineItem>();

			OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = opportunity.Id,
			                                                    PricebookEntryId = priceBook.Id
			                                                    );
			line1.Quantity = 1;
			line1.UnitPrice = 1800;
			line1.Description = 'Onsite project management';
			line1.Xero_Account_Code__c = '200';
			insert line1;

			Test.startTest();
			XR_MockDataTest.prepareMockDataForNoCustomerFound();

			PageReference pageRef = Page.xr_invoice_sync;
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id',opportunity.id);

			ApexPages.StandardController stController = new ApexPages.StandardController(opportunity);
			XR_InvoiceController controller = new XR_InvoiceController(stController);
			//push the opportunity with opportunity line item
			controller.doPush();
			Test.stopTest();
			//fetch back updated opportunity to assert
			Opportunity opportunity2 = [SELECT Account.Xero_Id__c,
			                            Account.Name,
			                            Xero_Id__c,
			                            CloseDate,
			                            Xero_Expected_Payment_Date__c,
			                            Xero_Fully_Paid_On_Date__c,
			                            Id,
			                            Xero_Last_Sync_Date__c,
			                            Name,
			                            Xero_Planned_Payment_Date__c,
			                            Xero_Invoice_Status__c,
			                            Xero_Sync_Details__c,
			                            Xero_Sync_Status__c,
			                            Amount,
			                            Xero_Invoice_Type__c,
			                            Xero_Account_Id__c,
			                            Xero_Invoice_Number__c,
			                            Invoice_Due_Date__c,
						    (
							    SELECT Xero_Account_Code__c,
							    Discount,
							    Id,
							    OpportunityId,
							    TotalPrice,
							    Name,
							    Quantity,
							    Xero_Tax_Amount__c,
							    Xero_Tax_Type__c,
							    UnitPrice,
							    Xero_Id__c
							    FROM OpportunityLineItems
			                            )
			                            FROM Opportunity
			                            WHERE Account.Name = 'Xero Account Missing Id'];
			//asserts showing opportunity sync failed
			System.assertEquals('Missing Account in Xero with this Name.',opportunity2.Xero_Sync_Details__c,'Sync Details Incorrect');
			System.assertEquals('Failed', opportunity2.Xero_Sync_Status__c, 'Sync Status Incorrect');
		}
	}
	/**
	 * accountXeroIdMissingButAccPresentInXero Account DOESNT have Xero Id i.e. Account is NOT Synced with Xero.
	 *             Create an opportunity and sync it, It should update the Xero Id on Account and update Opportunity with Xero Details.
	 */
	static testMethod void accountXeroIdMissingButAccPresentInXero(){
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			Account account = new Account(Name = 'Xero Account2');
			account.Xero_Id__c = '';
			insert account;
			Product2 product = [SELECT Id,Name FROM Product2 WHERE Name = 'Sample Product'];
			PricebookEntry priceBook = [SELECT Id FROM PricebookEntry LIMIT 1];
			Opportunity opportunity = new Opportunity(AccountId = account.Id,
			                                          Name = 'Test Invoice',
			                                          Xero_Invoice_Type__c = 'ACCREC',
			                                          StageName = 'Closed Won',
			                                          CloseDate = System.today()
			                                          );
			insert opportunity;
			List<OpportunityLineItem> lineItemList = new List<OpportunityLineItem>();

			OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = opportunity.Id,
			                                                    PricebookEntryId = priceBook.Id,
			                                                    Quantity = 1,
			                                                    UnitPrice = 1800,
			                                                    Description = 'Onsite project management',
			                                                    Xero_Account_Code__c = '200'
			                                                    );

			insert line1;

			Test.startTest();
			XR_MockDataTest.mockForAccountXeroIdMissingButPresentInXero();

			PageReference pageRef = Page.xr_invoice_sync;
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id',opportunity.id);

			ApexPages.StandardController stController = new ApexPages.StandardController(opportunity);
			XR_InvoiceController controller = new XR_InvoiceController(stController);
			controller.doPush();
			Test.stopTest();

			Opportunity opportunity2 = [SELECT Account.Xero_Id__c,
			                            Account.Name,
			                            Xero_Id__c,
			                            CloseDate,
			                            Xero_Expected_Payment_Date__c,
			                            Xero_Fully_Paid_On_Date__c,
			                            Id,
			                            Xero_Last_Sync_Date__c,
			                            Name,
			                            Xero_Planned_Payment_Date__c,
			                            Xero_Invoice_Status__c,
			                            Xero_Sync_Details__c,
			                            Xero_Sync_Status__c,
			                            Amount,
			                            Xero_Invoice_Type__c,
			                            Xero_Account_Id__c,
			                            Xero_Invoice_Number__c,
			                            Invoice_Due_Date__c,
						    (
							    SELECT Xero_Account_Code__c,
							    Discount,
							    Id,
							    OpportunityId,
							    TotalPrice,
							    Name,
							    Quantity,
							    Xero_Tax_Amount__c,
							    Xero_Tax_Type__c,
							    UnitPrice,
							    Xero_Id__c
							    FROM OpportunityLineItems
			                            )
			                            FROM Opportunity
			                            WHERE Account.Name = 'Xero Account2'];

			System.assertEquals(null,opportunity2.Xero_Sync_Details__c,'Sync Details Incorrect');
			//Xero id must get populated in account
			System.assertEquals('67b8620b-e7df-4bc3-9c44-af03c2ab11aH',opportunity2.Account.Xero_Id__c,'Account Xero id not populated');
			System.assertEquals('AUTHORISED',opportunity2.Xero_Invoice_Status__c, 'Invoice Status incorrect');
			System.assertEquals('Success',opportunity2.Xero_Sync_Status__c, 'Sync status incorrect');
			System.assertEquals('OIT00546',opportunity2.Xero_Invoice_Number__c,'Xero Invoice Number Incorrect');
		}
	}
	/**
	 * testAutoSyncCustomer there is a custom setting 'Auto Insert Contact' which when
	 * Checked ->
	 *  If the Account Xero Id already exists, the Invoice is created with that Contact in Xero.
	 *  If the Account Xero Id does not exist, the Customer is created in Xero and Xero Id is updated back in Salesforce.
	 *  Un Checked ->
	 *  If the Account Xero Id already exists, the Invoice is created with that Contact in Xero ELSE
	 *  Check if that Account with the same name exists in Xero before inserting an Invoice, if not Add an error message on Invoice that a Customer record in Xero doesn't exist.
	 *  This testMethod tests when its CHECKED
	 */
	static testMethod void testAutoSyncCustomerChecked(){
		//here the account should be automatically created in xero and xero id would be populated back to account
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			//account with Xero ID
			Account account = [SELECT Id, Xero_Id__c FROM Account WHERE Name = 'Xero Account Missing Id'];

			//before sync
			System.assertEquals(null,account.Xero_Id__c,'Account Xero Id was not null prior to sync');

			//pricebook entry for a product to be made available to opportunity line item
			PricebookEntry priceBook = [SELECT Id FROM PricebookEntry LIMIT 1];
			Opportunity opportunity = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Invoice' LIMIT 1];
			List<Opportunity> invList = new List<Opportunity>();
			invList.add(opportunity);
			List<OpportunityLineItem> lineItemList = new List<OpportunityLineItem>();

			OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = opportunity.Id,
			                                                    PricebookEntryId = priceBook.Id
			                                                    );
			line1.Quantity = 1;
			line1.UnitPrice = 1800;
			line1.Description = 'Onsite project management';
			line1.Xero_Account_Code__c = '200';
			insert line1;

			//xero custom setting Auto_Insert_Contact__c is CHECKED
			Xero_Settings__c cset = [SELECT Id,Auto_Insert_Contact__c,SetupOwnerId FROM Xero_Settings__c LIMIT 1];
			cset.Auto_Insert_Contact__c = True;
			update cset;

			Test.startTest();
			//mock response
			XR_MockDataTest.prepareMockDataToPushInvoice(invList);
			//provide the opportunityId to controller in order to start SYNC
			PageReference pageRef = Page.xr_invoice_sync;
			//set page
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id',opportunity.id);

			ApexPages.StandardController stController = new ApexPages.StandardController(opportunity);
			XR_InvoiceController controller = new XR_InvoiceController(stController);
			//push the opportunity
			controller.doPush();
			Test.stopTest();

			//fetch the updated records to assert
			List<opportunity> updatedOppList = [SELECT Account.Xero_Id__c,
			                                    Account.Name,
			                                    Xero_Id__c,
			                                    CloseDate,
			                                    Xero_Expected_Payment_Date__c,
			                                    Xero_Fully_Paid_On_Date__c,
			                                    Id,
			                                    Xero_Last_Sync_Date__c,
			                                    Name,
			                                    Xero_Planned_Payment_Date__c,
			                                    Xero_Invoice_Status__c,
			                                    Xero_Sync_Details__c,
			                                    Xero_Sync_Status__c,
			                                    Amount,
			                                    Xero_Invoice_Type__c,
			                                    Xero_Account_Id__c,
			                                    Xero_Invoice_Number__c,
			                                    Invoice_Due_Date__c
			                                    FROM Opportunity
			                                    WHERE Id =: opportunity.Id];
			//on sync, account should also get synced and xero id should be populated back
			System.assertEquals('67b8620b-e7df-4bc3-9c44-af03c2ab11ae',updatedOppList[0].Xero_Account_Id__c,'Account Xero Id failed to sync');
		}
	}
	/**
	 * testAutoSyncCustomerChecked custom setting 'Auto Insert Contact Test
	 * This testMethod tests when its UNCHECKED
	 */
	static testMethod void testAutoSyncCustomerUnchecked(){
		//here the account wont be automatically created in xero and xero id wont be populated back to account
		User user = [SELECT Id FROM User LIMIT 1];
		System.runAs(user){
			//Account which is never synced with xero
			Account account = [SELECT Id FROM Account WHERE Name = 'Xero Account Missing Id'];
			PricebookEntry priceBook = [SELECT Id FROM PricebookEntry LIMIT 1];
			//Opportunity has unsynced account
			Opportunity opportunity = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test Invoice2' LIMIT 1];
			List<OpportunityLineItem> lineItemList = new List<OpportunityLineItem>();

			OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = opportunity.Id,
			                                                    PricebookEntryId = priceBook.Id
			                                                    );
			line1.Quantity = 1;
			line1.UnitPrice = 1800;
			line1.Description = 'Onsite project management';
			line1.Xero_Account_Code__c = '200';
			insert line1;
			//xero custom setting Auto_Insert_Contact__c is UNCHECKED
			Xero_Settings__c cset = [SELECT Id,Auto_Insert_Contact__c,SetupOwnerId FROM Xero_Settings__c LIMIT 1];
			cset.Auto_Insert_Contact__c = False;
			update cset;

			Test.startTest();
			XR_MockDataTest.prepareMockDataForNoCustomerFound();

			PageReference pageRef = Page.xr_invoice_sync;
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id',opportunity.id);

			ApexPages.StandardController stController = new ApexPages.StandardController(opportunity);
			XR_InvoiceController controller = new XR_InvoiceController(stController);
			//push the opportunity with opportunity line item
			controller.doPush();
			Test.stopTest();
			//fetch back updated opportunity to assert
			Opportunity opportunity2 = [SELECT Account.Xero_Id__c,
			                            Account.Name,
			                            Xero_Id__c,
			                            CloseDate,
			                            Xero_Expected_Payment_Date__c,
			                            Xero_Fully_Paid_On_Date__c,
			                            Id,
			                            Xero_Last_Sync_Date__c,
			                            Name,
			                            Xero_Planned_Payment_Date__c,
			                            Xero_Invoice_Status__c,
			                            Xero_Sync_Details__c,
			                            Xero_Sync_Status__c,
			                            Amount,
			                            Xero_Invoice_Type__c,
			                            Xero_Account_Id__c,
			                            Xero_Invoice_Number__c,
			                            Invoice_Due_Date__c
			                            FROM Opportunity
			                            WHERE Account.Name = 'Xero Account Missing Id'];
			//asserts showing opportunity sync failed
			System.assertEquals('Missing Account in Xero with this Name.',opportunity2.Xero_Sync_Details__c,'Sync Details Incorrect');
			System.assertEquals('Failed', opportunity2.Xero_Sync_Status__c, 'Sync Status Incorrect');
		}
	}

	/**
	 * coverUnusedWrappers these unsused wrappers will be removed at end of build
	 */
	static testMethod void coverUnusedWrappers(){
		XR_Data wrap = new XR_Data();
		test.startTest();
		XR_Data.ValidationErrors wrap1 = new XR_Data.ValidationErrors();
		wrap1.Message = 'mm';

		XR_Data.Phones wrap2 = new XR_Data.Phones();
		wrap2.PhoneAreaCode = '123';
		wrap2.PhoneCountryCode = '142';
		wrap2.PhoneNumber = '65555555';
		wrap2.PhoneType = '545';

		XR_Data.Addresses wrap3 = new XR_Data.Addresses();
		wrap3.AddressType = 'wwewe';
		wrap3.City = 'wwww';
		wrap3.Country = 'UK';
		wrap3.PostalCode = '1234';
		wrap3.Region = 'wwww';

		XR_Data.Contact wrap4 = new XR_Data.Contact();
		wrap4.ContactStatus = 'status';
		wrap4.EmailAddress = 'ww';
		wrap4.BankAccountDetails = 'www';
		wrap4.Addresses = new List<XR_Data.Addresses>();
		wrap4.Phones = new List<XR_Data.Phones>();
		wrap4.UpdatedDateUTC = 'www';
		wrap4.ContactGroups = new List<XR_Data.Prepayments>();
		wrap4.DefaultCurrency = 'ww';
		wrap4.SalesTrackingCategories = new List<XR_Data.Prepayments>();
		wrap4.PurchasesTrackingCategories = new List<XR_Data.Prepayments>();
		wrap4.ContactPersons = new List<XR_Data.Prepayments>();
		wrap4.HasValidationErrors = true;
		wrap4.ValidationErrors = new List<XR_Data.ValidationErrors>();

		XR_Data.Invoice wrap5 = new XR_Data.Invoice();
		wrap5.Reference = 'dddd';
		wrap5.CreditNotes = new List<XR_Data.Prepayments>();
		wrap5.Payments = new List<XR_Data.Prepayments>();
		wrap5.Prepayments = new List<XR_Data.Prepayments>();
		wrap5.Overpayments = new List<XR_Data.Prepayments>();
		wrap5.AmountDue = 45;
		wrap5.AmountPaid = 55;
		wrap5.AmountCredited = 66;
		wrap5.CISDeduction = 91;
		wrap5.CurrencyRate = 1;
		wrap5.IsDiscounted = false;
		wrap5.DateString = '3';
		wrap5.DueDateString = '33';
		wrap5.ExpectedPaymentDateString = 'dd';
		wrap5.PlannedPaymentDateString = 'ee';
		wrap5.FullyPaidOnDateString = 'ww';
		wrap5.LineAmountTypes = 'ss';
		wrap5.SubTotal = 33;
		wrap5.TotalTax = 90;
		wrap5.TotalDiscount = 33;
		wrap5.UpdatedDateUTC = 'ss';
		wrap5.ValidationErrors = new List<XR_Data.ValidationErrors>();
		wrap5.HasAttachments = false;

		XR_Data.CreditNote wrap6 = new XR_Data.CreditNote();
		wrap6.Type = 'ww';
		wrap6.Contact = new XR_data.Contact();
		wrap6.Date1 = 'ss';
		wrap6.Status = 'ss';
		wrap6.LineAmountTypes = 'ww';
		wrap6.LineItems = new List<XR_Data.LineItem>();
		wrap6.SubTotal = 99;
		wrap6.TotalTax = 88;
		wrap6.Total = 90;
		wrap6.UpdatedDateUTC = 'ww';
		wrap6.CurrencyCode = 'wer';
		wrap6.FullyPaidOnDate = 'ww';
		wrap6.CreditNoteID = 'ww';
		wrap6.CreditNoteNumber = 'ww';
		wrap6.Reference = 'ww';
		wrap6.SentToContact = false;
		wrap6.CurrencyRate = 7;
		wrap6.RemainingCredit = 90;
		wrap6.Allocations = new List<XR_Data.Allocation>();
		wrap6.HasAttachments = false;
		wrap6.ValidationErrors = new List<XR_Data.ValidationErrors>();
		wrap6.AppliedAmount = 22;
		wrap6.HasErrors = false;

		XR_Data.Elements wrap7 = new XR_Data.Elements();
		wrap7.ItemID = 'ddd';
		wrap7.Code = 'ddd';
		wrap7.Description = 'sss';
		wrap7.UpdatedDateUTC = 'sss';
		wrap7.Name = 'dd';
		wrap7.IsTrackedAsInventory = true;
		wrap7.IsSold = true;
		wrap7.IsPurchased = false;
		test.stopTest();
		//to meet PMD standards
		System.assertEquals('true','true','true');
	}
}