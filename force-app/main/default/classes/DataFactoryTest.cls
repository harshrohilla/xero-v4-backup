/**
 * An implementation of a Data Factory to be used by Test classes
 *
 *
 * @date    06-01-2020
 * @author  Vanguard
 */
@isTest
public without sharing class DataFactoryTest
{
	/**
	 * getAccounts            Create test account records
	 * @param  samepleRecord Sample Account record which will be cloned , if given
	 * @param  numOfRecords  Number of account records to create
	 * @param  performDML    If these accounts should be inserted or returned without inserting in Salesforce
	 * @return               List of Account records
	 */
	public static List<Account> getAccounts(Account samepleRecord, Integer numOfRecords, Boolean performDML ){
		List<Account> recordList = new List<Account>();
		for(Integer i = 0; i < numOfRecords; i++) {
			Account record;
			if(samepleRecord != null) {
				record = samepleRecord.clone(false, false, false, false);
			}else {
				record = new Account();
			}

			record.Name = record.Name == null ? 'Test Account - '+ i : record.Name;

			recordList.add(record);
		}
		if(performDML) {
			insert recordList;
		}
		return recordList;
	}
	/**
	 * getContacts            Create test Contact records
	 * @param  samepleRecord Sample Contact record which will be cloned , if given
	 * @param  numOfRecords  Number of Contact records to create
	 * @param  performDML    If these Contact should be inserted or returned without inserting in Salesforce
	 * @return               List of Contact records
	 */
	public static List<Contact> getContacts(Contact samepleRecord, Integer numOfRecords, Boolean performDML ){
		List<Contact> recordList = new List<Contact>();
		for(Integer i = 0; i < numOfRecords; i++) {
			Contact record;
			if(samepleRecord != null) {
				record = samepleRecord.clone(false, false, false, false);
			}else {
				record = new Contact();
			}

			record.LastName = record.LastName == null ? 'Contact - '+ i : record.LastName;

			recordList.add(record);
		}
		if(performDML) {
			insert recordList;
		}
		return recordList;
	}
	/**
	 * getOpportunities            Create test Opportunity records
	 * @param  samepleRecord Sample Opportunity record which will be cloned , if given
	 * @param  numOfRecords  Number of Opportunity records to create
	 * @param  performDML    If these Opportunity should be inserted or returned without inserting in Salesforce
	 * @return               List of Opportunity records
	 */
	public static List<Opportunity> getOpportunities(Opportunity samepleRecord, Integer numOfRecords, Boolean performDML ){
		List<Opportunity> recordList = new List<Opportunity>();
		for(Integer i = 0; i < numOfRecords; i++) {
			Opportunity record;
			if(samepleRecord != null) {
				record = samepleRecord.clone(false, false, false, false);
			}else {
				record = new Opportunity();
			}

			record.Name = record.Name == null ? 'Test Opportunity - '+ i : record.Name;
			record.StageName = record.StageName == null ? 'Prospecting' : record.StageName;
			record.CloseDate = record.CloseDate == null ? System.Today() : record.CloseDate;

			recordList.add(record);
		}
		if(performDML) {
			insert recordList;
		}
		return recordList;
	}

	/**
	 * getUsers             description
	 * @param  numOfRecords Number of users to be created
	 * @param  profileName  profile name to be used while creating user, if null Standard Profile will be used
	 * @return              List of Users
	 */
	public static List<User> getUsers(Integer numOfRecords, String profileName){

		// This code runs as the system user
		List<User> userList = null;

		String pName = profileName == null ? 'Standard User' : profileName;

		List<Profile> profileList = [SELECT Id FROM Profile WHERE Name =: pName];

		if(profileList != null && profileList.size() > 0) {
			userList = new List<User>();
			for(Integer count = 0; count < numOfRecords; count++) {
				String uniqueUserName = profileName + '_' + count + '@testorg.com';
				User user = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
				                     EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
				                     LocaleSidKey = 'en_US', ProfileId = profileList[0].Id,
				                     TimeZoneSidKey = 'America/Los_Angeles',
				                     UserName = uniqueUserName);
				userList.add(user);
			}
		}
		return userList;
	}/**
	 * getProductsPriceBookEntry returns pricebookentry of product data supplied, doesnt do DML
	 * @param  name         name of product, default value set if passed null
	 * @param  price        price of product, default value set if passed null
	 * @param  sku          sku of product, default value set if passed null
	 * @param  numOfRecords numOfRecords of product requiured, default value 1 if passed null
	 * @return              returns a list of PricebookEntry of the quantity supplied in param
	 */
	public static List<PricebookEntry> getProductsPriceBookEntry(String name, Integer price, String sku, Integer numOfRecords){
		String productName = name == null ? 'Sample Product' : name;
		String productSKU = sku == null ? 'test001' : sku;
		Integer numOfProducts = numOfRecords == null ? 1 : numOfRecords;
		Integer prodPrice = price == null ? 100 : price;

		List<Product2> prodList = new List<Product2>();

		for (Integer i = 0; i < numOfProducts; i++) {
			Product2 product = new Product2(Name = productName,
			                                IsActive = True,
			                                ProductCode = '200',
			                                Description = 'productDescription',
			                                StockKeepingUnit = productSKU+i);
			prodList.add(product);
		}
		insert prodList;

		Id pricebookId = Test.getStandardPricebookId();

		List<PricebookEntry> priceBookList = new List<PricebookEntry>();

		for (Integer j = 0; j < prodList.size(); j++) {
			PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId,
			                                                  Product2Id = prodList[j].Id,
			                                                  UnitPrice = 10000,
			                                                  IsActive = true);
			priceBookList.add(standardPrice);
		}
		return priceBookList;
	}
	/**
	 * getProduct use this to get only product with no price associated. doesnt to DML
	 * @param  name         name of product, can be null
	 * @param  sku          sku 
	 * @param  numOfRecords number of records required
	 * @return              returns List of products
	 */ 
	public static List<Product2> getProduct(String name, String sku, Integer numOfRecords) {
		String productName = name == null ? 'Sample Product' : name;
		String productSKU = sku == null ? 'test001' : sku;
		Integer numOfProducts = numOfRecords == null ? 1 : numOfRecords;
		List<Product2> prodList = new List<Product2>();
		for (Integer i = 0; i < numOfProducts; i++) {
			Product2 product = new Product2(Name = productName,
			                                IsActive = True,
			                                ProductCode = '200',
			                                Description = 'productDescription',
			                                StockKeepingUnit = productSKU+i);
			prodList.add(product);
		}
		return prodList;
	}
    /**
	 * getPricebookEntryOfProduct use this to assign price to a product, doesnt require a product to be passed
	 * @param  prod  product record to which price needs to be assigned
	 * @param  price price of product
	 * @return       returns a record of PricebookEntry
	 */ 
	public static PricebookEntry getPricebookEntryOfProduct(Product2 prod, Integer price){
		Product2 product = prod == null ?  getProduct('Sample Product','TEST007',1)[0] : prod;
		Integer priceOfProduct = price == null ? 1000 : price;

		Id pricebookId = Test.getStandardPricebookId();
		PricebookEntry pricebookEntry = new PricebookEntry(Pricebook2Id = pricebookId,
		                                                                 Product2Id = product.Id,
		                                                                 UnitPrice = priceOfProduct,
		                                                                 IsActive = true);
		return pricebookEntry;
	}
}