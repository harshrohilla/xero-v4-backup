/**
 * @File Name          : HTTPHandler.cls
 * @Description        :
 * @Author             : Gulshan
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    06/01/2020      Gulshan                Initial Version
 */
public Inherited sharing class HTTPHandler
{
/**
 * setupRequest setup a HTTPRequest with provided endpoint and headermap
 * @param  endpoint  request endpoint 
 * @param  headerMap  map of headers, map<String,String>
 * @return            returns a HTTPRequest with endpoint and headerMap added to it
 */ 
private HttpRequest setupRequest(String endpoint, Map<String, String> headerMap){
	HttpRequest req = new HttpRequest();
	req.setEndpoint(endpoint);

	if(headerMap != null && headerMap.size() > 0) {
		for(String key: headerMap.keySet()) {
			if(headerMap.get(key) != null) {
				req.setHeader(key, headerMap.get(key));
			}
		}
	}
	return req;
}
/**
 * doGet hits the given endpoint with HTTP GET method and returns a response
 * @param  endpoint  request endpoint
 * @param  headerMap headerMap 
 * @return           response from callout
 */ 
public Response doGet(String endpoint, Map<String, String> headerMap){
	Response resp = new Response();
	try{
		HttpRequest req = setupRequest(endpoint, headerMap);
		req.setMethod('GET');

		Http http = new Http();
		LogUtils.Log(LoggingLevel.Debug, 'Get Request', req.getEndpoint());
		HTTPResponse res = http.send(req);
		resp.body = res.getBody();
		resp.status = 'Success';        
		LogUtils.Log(LoggingLevel.Debug, 'Get Response', res.getBody());
	}
	catch(Exception e)
	{
		resp.status = 'Failed';
		resp.message = e.getMessage();
		LogUtils.Log('Get Request', e);
	}
	return resp;
}
/**
 * doPost hits to given endpoint with HTTP POST method and returns a response
 * @param  endpoint  endpoint 
 * @param  body      requestBody to be POSTed
 * @param  headerMap headerMap 
 * @return           response from callout
 */ 
public Response doPost(String endpoint, String body, Map<String, String> headerMap){
	Response resp = new Response();
	try{
		HttpRequest req = setupRequest(endpoint, headerMap);
		req.setMethod('POST');
		req.setBody(body);
		LogUtils.Log(LoggingLevel.Debug, 'Post Request:  ' + req.getEndpoint(), req.getBody());
		Http http = new Http();
		HTTPResponse res = http.send(req);
		resp.body = res.getBody();
		resp.status = 'Success';		
		LogUtils.Log(LoggingLevel.Debug, 'POST Response', res.getBody());
	}
	catch(Exception e)
	{
		resp.status = 'Failed';
		resp.message = e.getMessage();
		LogUtils.Log('Post Request', e);
	}
	return resp;
}
/** 
 * doPut hits to given endpoint with HTTP PUT method and returns a response
 * @param  endpoint  endpoint 
 * @param  body      request body to be PUT to the provided endpoint
 * @param  headerMap headerMap 
 * @return           returns Reponse from callout
 */ 
public Response doPut(String endpoint, String body, Map<String, String> headerMap){
	Response resp = new Response();
	try{
		HttpRequest req = setupRequest(endpoint, headerMap);
		req.setMethod('PUT');
		req.setBody(body);
		LogUtils.Log(LoggingLevel.Debug, 'PUT Request:  ' + req.getEndpoint(), req.getBody());
		Http http = new Http();
		HTTPResponse res = http.send(req);
		resp.body = res.getBody();
		resp.status = 'Success';
		LogUtils.Log(LoggingLevel.Debug, 'PUT Response', res.getBody());
	}
	catch(Exception e)
	{
		resp.status = 'Failed';
		resp.message = e.getMessage();
		LogUtils.Log('PUT Request', e);
	}
	return resp;
}

public class Response
{
public String status {get;
	              set;}
public String message {get;
	               set;}
public String body {get;
	            set;}
}
}