/**
 * Class for creating mock data to test callouts
 */
public class XR_MultiRequestMock implements HttpCalloutMock
{
	Map<String, HttpCalloutMock> requests;

	public XR_MultiRequestMock(Map<String, HttpCalloutMock> requests) {
		this.requests = requests;
	}
	/**
	 * respond returns reponse corresponding to provided HTTPRequest
	 * @param  req HTTPRequest
	 * @return     return HTTPResponse
	 */
	public HTTPResponse respond(HTTPRequest req) {
		HttpCalloutMock mock = requests.get(req.getEndpoint());
		if (mock != null) {
			return mock.respond(req);
		} else {
			return null;
		}
	}
	/**
	 * addRequestMock add Endpoint and mock data into requests map
	 * @param  url  endpoint
	 * @param  mock 
	 */
	public void addRequestMock(String url, HttpCalloutMock mock) {
		requests.put(url, mock);
	}
}