@isTest
/**
 * test class for Account Sync Functionality of Xero - Salesforce integration
 * there are no negative testMethods in this class because an account gets synced on basis of name only.
 * so only negative scenario possible is if an account without name is pushed to xero which is practically impossible from salesforce's end
 * because Name is a required field on Account.
 * Other can be is duplicate accounts are pushed to xero, but xero simply updates them on top on other when same name accounts are received.
 */
public with sharing class XR_CustomerTest
{
	@TestSetup
	public static void makeData(){
		//account with Xero Id
		Account account = new Account(Name = 'Xero Account');
		account.Xero_Id__c = '67b8620b-e7df-4bc3-9c44-af03c2ab11ae';
		insert account;

		//account without xero id
		Account accountWithMissingXeroId = new Account(Name = 'Xero Account Missing Id');
		accountWithMissingXeroId.Xero_Id__c = '';
		insert accountWithMissingXeroId;

		//xero settings
		Xero_Settings__c cs = new Xero_Settings__c();
		cs.Webhook_Id__c = 'QjPGOyzdFl9ecVUSb1Vyxs1gKHu+ET+rSMFzzyOPzGfOCcgGdtdGMhXl5npjk/GRlDB5Q1Wfatvur4LOh1mIbw==';
		cs.SetupOwnerId = UserInfo.getOrganizationId();
		insert cs;

		//create user to use in System.runAs()
		User user = DataFactoryTest.getUsers(1,null)[0];
	}
	/**
	 * pushSingleAccount test a single invoice pushed to xero that was not present in xero
	 */
	static testMethod void pushSingleAccountAbsentInXero(){
		User user = [SELECT Id FROM USER LIMIT 1];
		Account account = [SELECT Id, Name,Xero_Id__c FROM Account WHERE Name =: 'Xero Account Missing Id' LIMIT 1];

		//asserting account before sync
		System.assertEquals(null,account.Xero_Id__c,'Xero Id not correct');

		System.runAs(user){
			Test.startTest();
			//mock response
			XR_MockDataCustomerTest.mockDataPushSingleProduct();
			//provide the accountId to controller in order to start SYNC
			PageReference pageRef = Page.XR_AccountDetailPageButton;
			//set page
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id',account.id);

			ApexPages.StandardController stController = new ApexPages.StandardController(account);
			XR_CustomerController controller = new XR_CustomerController(stController);
			//push the account
			controller.doPush();
			Test.stopTest();
			//fetch back to upsert
			Account updAccount = [SELECT Id, Name, Xero_Id__c,Xero_Sync_Status__c
			                      FROM Account WHERE Name =: 'Xero Account Missing Id'
			                                                LIMIT 1];

			System.assertEquals('eb518be4-26f3-4e52-af92-ce5ac3e24f09',updAccount.Xero_Id__c,'Xero Id not synced');
            System.assertEquals('Xero Account Missing Id',updAccount.Name,'Account Name incorrect after sync');
            System.assertEquals('Success',updAccount.Xero_Sync_Status__c,'Sync Status incorrect after sync');
		}
	}
	/**
	 * pushBulkAccounts test a list of accounts pushed to xero, all of these accounts were absent in xero
	 */
	static testMethod void pushBulkAccounts(){
		User user = [SELECT Id FROM USER LIMIT 1];
		Account account = [SELECT Id, Name,Xero_Id__c FROM Account WHERE Name =: 'Xero Account Missing Id' LIMIT 1];
		Account account2 = account.clone(false,false,false,false);
		account2.Name = 'Xero Account Missing Id 2';
		insert account2;
		//prepare account list to be passed to standardSetController
		List<Account> accList = new List<Account>();
		accList.add(account);
		accList.add(account2);

		System.runAs(user){
			//asserting account before sync
			System.assertEquals(null,account.Xero_Id__c,'Xero Id incorrect');
			System.assertEquals(null,account2.Xero_Id__c,'Xero Id incorrect');

			Test.startTest();
			//mock response
			XR_MockDataCustomerTest.mockDataPushMultipleProducts();
			//provide the accountId to controller in order to start SYNC
			PageReference pageRef = Page.XR_AccountListViewButton;
			Test.setCurrentPage(pageRef);
			ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(accList);
			stdSetController.setSelected(accList);
			XR_CustomerController ext = new XR_CustomerController(stdSetController);
			//push the accounts
			ext.doPush();
			Test.stopTest();

			List<Account> updList = [SELECT Id, Name, Xero_Id__c,Xero_Sync_Status__c
			                         FROM Account
			                         WHERE Id IN:accList
			                         LIMIT 2];

			System.assertEquals('ba6814c9-2670-42ad-acf0-a1d9ef465856',updList[0].Xero_Id__c,'Xero Id not synced');
            System.assertEquals('Xero Account Missing Id',updList[0].Name,'Name incorrect');
            System.assertEquals('Success',updList[0].Xero_Sync_Status__c,'Sync Status incorrect after sync');
			System.assertEquals('8b4d19c3-8fc9-475e-b962-165eac2b1974',updList[1].Xero_Id__c,'Xero Id not synced');
            System.assertEquals('Xero Account Missing Id 2',updList[1].Name,'Name incorrect');
            System.assertEquals('Success',updList[1].Xero_Sync_Status__c,'Sync Status incorrect after sync');
		}
	}
	/**
	 * accPresentInXeroButXeroIdMissing push an account whose xeroId was missing but the account was present in xero
	 *                                  ideally it should just update the xeroId in salesforce
	 */
	static testMethod void accPresentInXeroButXeroIdMissing(){
		User user = [SELECT Id FROM USER LIMIT 1];
		System.runAs(user){
			Account account = [SELECT Id, Name,Xero_Id__c FROM Account WHERE Name =: 'Xero Account Missing Id' LIMIT 1];

			//asserting account before sync
			System.assertEquals(null,account.Xero_Id__c,'Xero Id incorrect');

			System.runAs(user){
				Test.startTest();
				//mock response
				XR_MockDataCustomerTest.mockDataPushSingleProductPresentInXero();
				//provide the accountId to controller in order to start SYNC
				PageReference pageRef = Page.XR_AccountDetailPageButton;
				//set page
				Test.setCurrentPage(pageRef);
				ApexPages.currentPage().getParameters().put('id',account.id);

				ApexPages.StandardController stController = new ApexPages.StandardController(account);
				XR_CustomerController controller = new XR_CustomerController(stController);
				//push the account
				controller.doPush();
				Test.stopTest();
				//fetch back to assert
				Account updAccount = [SELECT Id, Name, Xero_Id__c,Xero_Sync_Status__c
				                      FROM Account WHERE Name =: 'Xero Account Missing Id'
				                                                LIMIT 1];

				System.assertEquals('eb518be4-26f3-4e52-af92-ce5ac3e24f09',updAccount.Xero_Id__c,'Xero Id not synced');
                System.assertEquals('Xero Account Missing Id',updAccount.Name,'Account Name incorrect after sync');
                System.assertEquals('Success',updAccount.Xero_Sync_Status__c,'Sync Status incorrect after sync');
			}
		}
	}
}