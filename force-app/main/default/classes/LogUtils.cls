/*
 * @-author Gulshan Middha
 * @date 17 Dec 2019
 * @description This is an utility class to log messages from Apex code
 */
public without sharing class LogUtils
{
	/**
	 * Custom Metadata details, used while flushing the logs
	 */
	private static Map<String, Log_Setting__mdt> loggingRules;
	/**
	 * Temprory storage of the logs till the time they are inserted or processed.
	 */
	private static Map<String, List<System_Log__c> > logMap;
	/**
	 * Active users in Salesforce Org, used while sending out emails
	 */
	private static Map<String, User> activeUserMap;

	//TODO
	//public static boolean sendImmediate = true;

	// Static code to retrieve the current custom setting on first call
	static {
		// Retrieve the current LoggingLevel from the custom settings
		try {
			loggingRules = new Map<String, Log_Setting__mdt>();
			logMap = new Map<String, List<System_Log__c> > ();

			for (Log_Setting__mdt r : [SELECT Id, Active__c, Echo__c, Send_Email_To__c, Log_To_Object__c, Level__c from Log_Setting__mdt]) {
				if (r.Active__c) {
					loggingRules.put(r.Level__c, r);
				}
			}

			activeUserMap = getActiveUserMap();
		}

		catch (Exception e)
		{
			// Output the exception message
			System.debug(LoggingLevel.DEBUG, e.getMessage());
			System.debug(LoggingLevel.DEBUG, 'Unable to retrieve log level custom setting. Using ERROR as default.');
		}
	}
	/**
	 * log description
	 * @param  level     Debug Log Level
	 * @param  reference reference for this Log
	 * @param  message   message
	 */
	public static void log(LoggingLevel level, String reference, String message) {
		pushLog(level, reference, message, null);
	}
	/**
	 * log description
	 * @param  level           Debug Log Level
	 * @param  reference       reference for this Log
	 * @param  message         message
	 * @param  relatedRecordId Salesforce Id of the record
	 */
	public static void log(LoggingLevel level, String reference, String message, Id relatedRecordId) {
		pushLog(level, reference, message, relatedRecordId);
	}
	/**
	 * log description
	 * @param  reference       reference for this Log
	 * @param  ex              Exception object
	 * @param  relatedRecordId Salesforce Id of the record
	 */
	public static void log(String reference, Exception ex, Id relatedRecordId) {
		pushLog(LoggingLevel.ERROR, reference, ex.getMessage(), relatedRecordId);
	}
	/**
	 * log description      Log Exceptions
	 * @param  reference    reference for this Log
	 * @param  ex           Exception
	 */
	public static void log(String reference, Exception ex) {
		pushLog(LoggingLevel.ERROR, reference, ex.getMessage(), null);
	}
	/**
	 * pushLog description
	 * @param  level           Debug Log Level
	 * @param  reference       reference for this Log
	 * @param  message         Detailed Message
	 * @param  relatedRecordId Salesforce Id of the record
	 */
	private static void pushLog(LoggingLevel level, String reference, String message, Id relatedRecordId) {
		System_Log__c log = new System_Log__c(
			Level__c             = level.name(),
			Reference__c         = reference,
			Message__c           = message,
			Class__c             = StackTraceUtils.getClassName(2),
			Method__c            = StackTraceUtils.getMethod(2),
			Stack_Trace__c       = StackTraceUtils.getStackTrace(2),
			Related_Record_Id__c = relatedRecordId
			);
		if(logMap.containsKey(level.name())) {
			List<System_Log__c> logList = logMap.get(level.name());
			logList.add(log);
		}else {
			logMap.put(level.name(), new List<System_Log__c> {log});
		}

		if(loggingRules.containsKey(String.valueOf(level)) && (loggingRules.get(String.valueOf(level)).Echo__c)){
			System.debug('\n\n' + log);
		}
	}
	/**
	 * flush Insert records and send emails for all the Log records added in Map till this point
	 *      It also removes the logs from map and starts afresh.
	 */
	public static void flush(){
		flushAllToObject();
		flushAllToEmail();
		logMap = new Map<String, List<System_Log__c> >();
	}
	/**
	 * flushAllToObject Insert all the log records in cache/Map into a custom object
	 */
	private static void flushAllToObject(){
		if(logMap.size() > 0 ) {
			List<System_Log__c> insertLogList = new List<System_Log__c>();
			for (String level : loggingRules.keySet()) {

				Log_Setting__mdt rule = loggingRules.get(level);

				if (rule.Log_To_Object__c && logMap.containsKey(rule.Level__c)) {
					insertLogList.addAll(logMap.get(rule.Level__c));
				}
			}

			if (!insertLogList.isEmpty() && (Limits.getDMLStatements() <= Limits.getLimitDMLStatements())) {
				try{
					List<Database.SaveResult> results = Database.insert(insertLogList, false);
					for (Database.SaveResult result : results) {
						if (!result.success) {
							System.debug(LoggingLevel.DEBUG, '\n\nERROR LogUtils: Debug log not saved: ' + result.errors[0].message);
						}
					}
				}
				catch(Exception e)
				{
					System.debug('\n\nError inserting System Log records : exception : '+e);
				}
			}
		}
	}
	/**
	 * getEmailBody Generate the HTML body for the Email
	 * @param       logRecords Records added to the Email body
	 * @return      Email HTML Body with records in a table
	 */
	private static String getEmailBody(List<System_Log__c> logRecords){

		String html = '<table border="1" cellspacing="0" cellpadding="5" style="width: 80%;font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;">' +
		              '<tr style="text-align:left; border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;">' +
		              '<th>Level</th><th>Class</th><th>Method</th><th>Related Record</th><th>Reference</th><th>Message</th><th>Stack Trace</th>' +
		              '</tr>';

		if(logRecords != null && logRecords.size() > 0) {
			for(System_Log__c log: logRecords) {
				html += '<tr>' +
				        '<td>' + log.Level__c +
				        '</td><td>' + log.Class__c +
				        '</td><td>' + log.Method__c +
				        '</td><td>' + log.Related_Record__c +
				        '</td><td>' + log.Reference__c +
				        '</td><td>' + log.Message__c +
				        '</td><td>' + log.Stack_Trace__c +
				        '</td>' +
				        '</tr>';
			}
			html += '</table>';
			return html;
		}

		return '';
	}
	/**
	 * flushAllToEmail Sends out emails to users mentioned in Custom Setting.
	 * This method consolidate all records per Level and sends one email per Log Level
	 */
	private static void flushAllToEmail(){
		if(logMap.size() > 0 ) {
			List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

			for (String level : loggingRules.keySet()) {

				Log_Setting__mdt rule = loggingRules.get(level);

				if (rule.Send_Email_To__c != null && logMap.containsKey(rule.Level__c)) {
					List<User> sendToUserId = new List<User>();
					Set<String> sendToUserAddress = new Set<String>();

					for (String address: rule.Send_Email_To__c.split(',')) {
						if (activeUserMap.containsKey(address.trim())) {
							sendToUserId.add(activeUserMap.get(address.trim()));
						} else {
							sendToUserAddress.add(address.trim());
						}
					}
					for (User sendToUser: sendToUserId) {
						Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
						emailMessage.subject = 'Apex Logs for Level - ' + rule.Level__c;
						String html = getEmailBody(logMap.get(rule.Level__c));
						emailMessage.setHtmlBody(html);
						emailMessage.setTargetObjectId(sendToUser.Id);
						sendToUserAddress.remove(sendToUser.email);
						emailMessage.setSaveAsActivity(false);
						mails.add(emailMessage);
					}
					if(sendToUserAddress.size() > 0) {
						Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
						emailMessage.subject = 'Apex Logs for Level - ' + rule.Level__c;
						String html = getEmailBody(logMap.get(rule.Level__c));
						emailMessage.setHtmlBody(html);
						emailMessage.toAddresses = new List<String>(sendToUserAddress);
						mails.add(emailMessage);
					}
				}
			}
			Integer amountAwayFromGovLimit = (Limits.getLimitEmailInvocations() - Limits.getEmailInvocations()) + mails.size();
			if(amountAwayFromGovLimit > 0) {

				List<Messaging.SendEmailResult> results = Messaging.sendEmail(mails, false);
				for (Messaging.SendEmailResult result : results) {
					if (result.success) {
						System.debug(LoggingLevel.DEBUG, '\n\nLogUtils: Email was sent successfully.');
					} else {
						System.debug(LoggingLevel.ERROR, '\n\nLogUtils: Email failed to send: ' + result.errors[0].message);
					}
				}
			}
		}
	}
	/**
	 * getActiveUserMap Get all the active users in the Org
	 * @return   Map of Active users with email Ids as Key. Returns only one user, in case of duplicate emails
	 */
	private static Map<String, User> getActiveUserMap(){
		Map<String, User> userMap = new Map<String, User>();
		for(User u: [Select Id, Email from User where isActive = true]) {
			userMap.put(u.Email, u);
		}
		return userMap;
	}
}