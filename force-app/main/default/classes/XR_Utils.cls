public without sharing class XR_Utils
{
	/**
	 * getBoolean get true/false based on null/not null
	 * @param     boolean value
	 * @return    true/false
	 */
	public static Boolean getBoolean(Boolean b){
		return b == null ? false : b;
	}
	/**
	 * getDate get date value from string
	 * @param  string of date
	 * @return return corresponding date or null
	 */
	public static Date getDate(String str){
		if(str != null && str.contains('T')) {
			try{
				String year = str.substring(0,4);
				String month = str.substring(5,7);
				String day = str.substring(8,10);
				return Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
			}
			catch(Exception e)
			{
				LogUtils.log('Xero Mapping Handler', e);
			}
		}
		return null;
	}
	/**
	 * getDate  get string value of date
	 * @param   any date
	 * @return   return date string or null
	 */
	public static String getDate(Date d){
		if(d != null) {
			return d.year() + '-' + d.month() + '-' + d.day();
		}
		return '';
	}
    //check if person account is enabled in org
	public static boolean getIsPerson(){
		return Schema.sObjectType.Account.fields.getMap().containsKey( 'isPersonAccount' );
	}

	public static Integer checkIntentToRecieve(RestRequest req){
		// Retrieve the Xero signature from the headers
		String xeroSignature = req.headers.get('x-xero-signature');
		// Retrieve the Xero payload body
		String xeroPayload = req.requestBody.toString();
		// Verify the signature using 'hmacSHA256'. Webhook key stored in a Custom Setting
		Blob signedPayload = Crypto.generateMac('hmacSHA256', Blob.valueOf(xeroPayload), Blob.valueOf(Xero_Settings__c.getInstance().Webhook_Id__c));
		// Once we have the signed payload, encode it using base64 to convert back to a string
		String encodedPayload = EncodingUtil.base64Encode(signedPayload);
		// Return status code based on whether signed payload matches or not
		return encodedPayload == xeroSignature ? 200 : 401;
	}
	/**
	 * fetchXeroSettings get xero settings from custom setting to be used in all classes
	 * @return   return xeroSetting custom setting
	 */
	public static Xero_Settings__c fetchXeroSettings(){
		Xero_Settings__c xeroCustomSetting = Xero_Settings__c.getInstance();
		return xeroCustomSetting;
	}
	/**
	 * getOppFromSalesforce used to get required fields values of an opp from salesforce to help in inserting opp received through webhook
	 * @param  xeroIdSet set of xeroIds corresponding to whom opportunities are to be retrieved
	 * @return           return list of opportunities required fields for upsert
	 */
	public static List<Opportunity> getOppFromSalesforce(Set<String> xeroIdSet){
		List<Opportunity> oppFromSalesforce = [SELECT Id, Xero_Id__c, Name, CloseDate, StageName, Xero_Invoice_Number__c,
		                                       AccountId, Account.Name, Account.Xero_Id__c
		                                       FROM Opportunity
		                                       WHERE Xero_Id__c IN: xeroIdSet];
		System.debug('oppFromSalesforce'+oppFromSalesforce);
		return oppFromSalesforce;
	}
	/**
	 * getAccountsFromSalesforce used to get Account on basis of its XeroId From Salesforce
	 * @param  xeroIdSet set of xeroIds
	 * @return           returns a list of Accounts found
	 */
	public static Account getAccountsFromSalesforce(String xeroIdSet){
		try {
			Account accFromSalesforce = [SELECT Id, Name, Xero_Id__c
			                             FROM Account
			                             WHERE Xero_Id__c =: xeroIdSet LIMIT 1];
			return accFromSalesforce;
		}
		catch (Exception e)
		{
			return null;
		}
	}
	/**
	 * upsertByXeroId takes a list of sobject and sobject name, then upserts them in salesforce on basis of xeroId
	 * @param  objAPIName API name of the object
	 * @param  lst        List of records to be upserted
	 */
	public static void upsertByXeroId(String objAPIName, List<SObject> lst){
		Set<String> invoiceIds = new Set<String>();
		for(SObject obj: lst) {
			if(obj.get('Xero_Id__c') != null) {
				invoiceIds.add((String)obj.get('Xero_Id__c'));
			}
		}
		if(!invoiceIds.isEmpty()) {
			Map<String, Id> objMap = new Map<String, Id>();
			//to escape single quote characters and henece to prevent SOQL injection
			String.escapeSingleQuotes(objAPIName);
			//for(Sobject obj: Database.query('Select Id, Xero_Id__c from '+objAPIName+' Where Xero_Id__c IN: invoiceIds')) {
			for(Sobject obj: Database.query('Select Id, Xero_Id__c from Opportunity Where Xero_Id__c IN: invoiceIds')) {
				objMap.put((String)obj.get('Xero_Id__c'), obj.Id);
			}
			System.debug('objMap'+objMap);
			List<SObject> insertList = new List<SObject>();
			List<SObject> updateList = new List<SObject>();

			for(SObject obj: lst) {
				if(objMap.containsKey((String)obj.get('Xero_Id__c'))) {
					obj.put('Id', objMap.get((String)obj.get('Xero_Id__c')));
					updateList.add(obj);
				}else {
					insertList.add(obj);
				}
			}
			System.debug('updateList'+updateList);
			System.debug('insertList'+insertList);
			List<Database.SaveResult> updateResults = Database.update(updateList);
			if (updateResults != null) {
				for(Database.SaveResult result: updateResults) {
					if (!result.success) {
						LogUtils.log(LoggingLevel.ERROR, 'upsertByXeroId', 'Update ' + objAPIName + ' '+ result.getErrors(), result.getId());
					}
				}
			}
			List<Database.SaveResult> insertResults = Database.insert(insertList);
			if (insertResults != null) {
				for(Database.SaveResult result: insertResults) {
					if (!result.success) {
						LogUtils.log(LoggingLevel.ERROR, 'upsertByXeroId', 'Insert ' + objAPIName + ' '+ result.getErrors(), result.getId());
					}
				}
			}
		}
	}
}