/**
 * @File Name          : XR_InvoiceRESTHandler.cls
 * @Description        : A class containing webhook logic, having an API exposed to receive updates in form of payloads from xero
 * @Author             : Gulshan
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 */
@restResource (urlMapping = '/invoice/*')	//https://xerocd-developer-edition.ap17.force.com/xero/services/apexrest/invoice
global without sharing class XR_InvoiceRESTHandler
{
/**
 * doPost take payload from xero, validate it through check intent to receive, if validated send the reponse body to invoice handler to continue sync
 *        else if validation fails, log it to system logs and halt the sync 
 */ 
	@HttpPost
	global static void doPost () {
		//Validate the payload if its from a valid source
		RestContext.response.statusCode = XR_Utils.checkIntentToRecieve(RestContext.request);
		//if validated, it will respond in StatusCode == 200, else 401/501
		if (RestContext.response.statusCode == 200) {
			//if data came from webhook, update it accordingly in salesforce
			XR_InvoiceHandler.updateInvoice(RestContext.request.requestBody.toString());
		}else {
			//if validation fails, log it in system logs and halt
			LogUtils.log(LoggingLevel.DEBUG, 'Webhook', 'Webhook Authentication failed. Auto Update from Xero affected.', null);
		}
		//flush logs (Do DML and send notification email)
		LogUtils.flush();
	}

}