public with sharing class XR_InvoiceAutoSyncViaProcess
{
	@InvocableMethod
	public static void doPush(List<Id> oppIds) {
		if(oppIds.size() > 0) {
			pushToXero(oppIds);
		}
	}
	@future(callout = true)
	public static void pushToXero(List<Id> oppIds){
		List<Opportunity> oppList = [SELECT
		                             Id,
		                             AccountId,
		                             Account.Name,
		                             Account.Xero_Id__c,
		                             Xero_Account_Id__c,
		                             Xero_Expected_Payment_Date__c,
		                             Xero_Fully_Paid_On_Date__c,
		                             Xero_Id__c,
		                             Xero_Invoice_Number__c,
		                             Xero_Invoice_Status__c,
		                             Xero_Invoice_Type__c,
		                             Xero_Invoice_URL__c,
		                             Xero_Last_Sync_Date__c,
		                             Xero_Planned_Payment_Date__c,
		                             Xero_Sync_Details__c,
		                             Xero_Sync_Status__c,
		                             HasOpportunityLineItem,
		                             CloseDate,
		                             Amount,
		                             Invoice_Due_Date__c,
					     (
						     SELECT
						     Description,
						     Id,
						     Quantity,
						     UnitPrice,
						     Xero_Account_Code__c,
						     Xero_Id__c,
						     Xero_Tax_Amount__c,
						     Xero_Tax_Type__c,
						     Discount,
						     SubTotal,
						     TotalPrice,
						     Name
						     FROM OpportunityLineItems
		                             )
		                             FROM Opportunity
		                             WHERE Id IN : oppIds];

		XR_InvoiceHandler handler = new XR_InvoiceHandler();
		handler.doPush(oppList);
	}
}