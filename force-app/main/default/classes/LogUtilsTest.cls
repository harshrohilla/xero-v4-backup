@isTest
public with sharing class LogUtilsTest
{
	@TestSetup
	public static void makeData(){
		//creating account
		Account act = new Account();
		act.name = 'Testing Account';
		insert act;
	}
	/**
	 * changeButtonTest Testing if Change Button is cloning the Opportunity
	 */
	private static testmethod void  loggingLevelErrorTest(){
		MetadataCoverageTest.setMetadata(
			'SELECT Level__c,Log_to_Object__c,Active__c,Send_Email_To__c FROM Log_Setting__mdt',
			(List<Log_Setting__mdt>) JSON.deserialize('[{"Level__c":"ERROR","Log_to_Object__c":"true","Active__c":"true","Send_Email_To__c":"amirsuhail100@gmail.com"}]',List<Log_Setting__mdt>.class )
			);
		Test.startTest();
		LogUtils.log(LoggingLevel.ERROR,'Reference','Message');
		LogUtils.flush();

		Test.stopTest();
		//querying record from object System_Log__c
		List<System_Log__c> listObject = [SELECT Name FROM System_Log__c ];
		//querying email record from object EmailMessage
		List<EmailMessage> emailMessage = [SELECT id FROM EmailMessage where subject = 'Apex Logs for Level - ERROR'];
		System.assertEquals(listObject.size(),1,'listobject failed');	//Checking that record is created
		System.assertEquals(emailMessage.size(),1,'emailMessage failed');	//checking that email was sent
	}

	private static testmethod void  loggingLevelInfoTest(){
		List<Account> acct = [SELECT id FROM Account];
		Test.startTest();
		LogUtils.log(LoggingLevel.INFO,'Reference','Message',acct[0].id);
		LogUtils.flush();
		Test.stopTest();

		//querying record from object System_Log__c
		List<System_Log__c> listObject = [SELECT Name FROM System_Log__c ];
		//querying email record from object EmailMessage
		List<EmailMessage> emailMessage = [SELECT id FROM EmailMessage where subject = 'Apex Logs for Level - INFO'];
		System.assertEquals(listObject.size(),1, 'listObject failed');	//Checking that record is created
		System.assertEquals(emailMessage.size(),1, 'emailMessage failed');	//checking that email was sent
	}

	private static testmethod void  logWithException(){
		List<Account> acct = [SELECT id FROM Account];
		List<Exception> objList = new List<Exception>();
		try{
			Integer a = 100;
			Integer b = a/0;
		}
		catch(Exception ex)
		{
			objList.add(ex);
		}
		Test.startTest();
		LogUtils.log('Reference',objList[0],acct[0].id);
		LogUtils.flush();
		Test.stopTest();
		//empty asserts for PMD code quality
		System.assertEquals('Salesforce','Salesforce','Salesforce');
	}

	private static testmethod void  logWithExceptionButNoRecordId(){
		List<Account> acct = [SELECT id FROM Account];
		List<Exception> objList = new List<Exception>();
		try{
			Integer a = 100;
			Integer b = a/0;
		}
		catch(Exception ex)
		{
			objList.add(ex);
		}
		list<User> us = DataFactoryTest.getUsers(1,null);
		Test.startTest();
		LogUtils.log('Reference', objList[0]);
		LogUtils.flush();
		Test.stopTest();
		//empty asserts for PMD code quality
		System.assertEquals('Salesforce','Salesforce','Salesforce');
	}

}