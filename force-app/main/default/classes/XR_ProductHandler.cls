/**
 * @File Name          : XR_ProductHandler.cls
 * @Description        : Handler class for Products sync to Xero
 * @Author             : Harsh
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    13/04/2020      Harsh                Initial Version
 */
public inherited sharing class XR_ProductHandler
{
	XR_ProductHelper helper;
	public XR_ProductHandler() {
		helper = new XR_ProductHelper();
	}/**
	 * doPush this method takes a list of products to be PUSHed to xero, sends them to helper where everything, from callout to mapping is handled,
	 *         takes updated list of Products from it and updates them back to Salesforce
	 * @param  prodList List of Salesforce Products to be synced with xero, taken from Detail Page for Single product or List View for bulk sync
	 */
	public void doPush(List<Product2> prodList){
		List<Product2> validProductsList = new List<Product2>();
		List<Product2> inValidProductsList = new List<Product2>();
		//check if product has a pricebookentry related or not
		for(Product2 product : prodList) {
			if (product.PricebookEntries != null && product.PricebookEntries.size()>0) {
				validProductsList.add(product);
			}else {
				product.Xero_Sync_Status__c = XR_Const.failed;
				product.Xero_Sync_Details__c = XR_Const.priceBookEntryNotFound;
				inValidProductsList.add(product);
			}
		}
		//sends the products to helper and receive an updated List of products to be synced back in salesforce
		if(validProductsList.size() > 0) {
			List<Product2> productsList = helper.postProduct(validProductsList);

			//database method used to iterate through errors if any and log them in custom logs
			if (productsList.size() > 0) {
				List<Database.SaveResult> updateProducts = Database.update(productsList, false);
				for(Database.SaveResult result: updateProducts) {
					if (!result.success) {
						//if any exception occurs, log them to System Logs (custom)
						LogUtils.log(LoggingLevel.DEBUG, 'doPush', 'Products SYNC failed. '+result.getErrors(), result.getId());
					}
				}
			}
		}
		//update invalid products back in salesforce
		if(inValidProductsList.size() > 0) {
			List<Database.SaveResult> updateInvalidProducts = Database.update(inValidProductsList, false);
			for(Database.SaveResult results: updateInvalidProducts) {
				if (!results.success) {
					//if any exception occurs, log them to System Logs (custom)
					LogUtils.log(LoggingLevel.DEBUG, 'doPush', 'Products SYNC failed. '+results.getErrors(), results.getId());
				}
			}
		}

	}
}