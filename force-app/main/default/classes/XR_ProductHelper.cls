/**
 * @File Name          : XR_ProductHelper.cls
 * @Description        : Helper class for Products sync to Xero
 * @Author             : Harsh
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    13/04/2020      Harsh                Initial Version
 */
public inherited sharing class XR_ProductHelper extends XR_AuthHandler
{
	/**
	 * postProduct pushes products provided to Xero and returns response as updated product records list
	 * @param  prodList List of products that are to be Synced to Xero
	 * @return          List or products updated after Syncing to Xero
	 */
	public List<Product2> postProduct(List<Product2> prodList){
		List<XR_Data.Item> productWrapperList = new List<XR_Data.Item>();
		if(prodList != null && prodList.size() > 0) {
			for (Product2 prod : prodList) {
				//map the products into Wrappers to be serialized into JSON
				XR_Data.Item productWrapper = XR_MappingHandler.mapProductsSfToXero(prod);
				productWrapperList.add(productWrapper);
			}
		}
		Map<String, List<XR_Data.Item> > itemsMap = new Map<String, List<XR_Data.Item> >();
		//Final request body map to be serialzed
		itemsMap.put('Items', productWrapperList);
		//XR_Data.Item productWrapper = XR_MappingHandler.mapProductsSfToXero(prodList[0]);
		//preparing header map to be sent with request
		Map<String,String> headerMap = new Map<String,String>();
		//calling authentication helper class XR_AuthHandler.apxc to authenticate with Xero and provide tenantId (TOKEN) to be sent into header
		headerMap.put('xero-tenant-id', XR_AuthHandler.tenantId);
		//serilize the reqest body into JSON object
		String requestBody = JSON.serializePretty(itemsMap);
		//return the prepared request body, header map and products list to successor method for callout
		return postProductCallout(requestBody, headerMap, prodList);
	}
	/**
	 * postProduct_Callout takes requestBody, headerMap and products list and perform a callout to Xero Items API
	 * @param  requestBody body cotaining list of Products to be Synced to Xero in JSON format
	 * @param  headerMap   header map
	 * @param  prodList    list of products, used as an updated list to update in salesforce after callout is completed
	 * @return             return description
	 */
	public List<Product2> postProductCallout(String requestBody, Map<String,String> headerMap,list<Product2> prodList){
		//fetching endpoint from Constants Class XR_Const.apxc, to keep helper free from harcoded values
		String endpoint = 'callout:' + XR_Const.productEndpoint;
		//call HTTPHandler.class to send request
		HTTPHandler handler = new HTTPHandler();
		HTTPHandler.Response response = handler.doPost(endpoint, requestBody, headerMap);
		//returns the list of products to be updated and response from xero callout to successor class
		return handleResponse(prodList, response);
	}
	//if response is error response
	public List<Product2> handleResponse(list<Product2> prodList, HTTPHandler.Response response){
		//initialize wrapper class to store result in
		ProductResultWrapper wrapper = new ProductResultWrapper();
		//the respnse can be of two types: success or error
		//if the response is errror, it will contain an ErrorNumber object which can be used to distinguish it from success
		if(response != null && response.body != null) {
			//if the reponse is error
			if(response.body.contains('"ErrorNumber":')) {
				//deserialize the response in wrappers created to handle error reponse
				XR_Data.responseWithError errorResponseWrapper = (XR_Data.responseWithError)JSON.deserialize(response.body,XR_Data.responseWithError.class);
				//handling bulkified response, adding elements(products) into list to update in salesforce accordingly
				list<XR_Data.Elements> productsList = errorResponseWrapper.Elements;
				//fethcing products one by one to update to salesforce in FCFS manner
				for(Integer i = 0; i < productsList.size(); i++) {
					Product2 prod;
					if(prodList.size() < i+1) {
						prod = new Product2();
					}else {
						prod = prodList[i];
					}
					/*
					 *  it will map Response wrapper to Salesforce Product record
					 *  first parameter is : product salesforce record on Index 0 of productList
					 *  second parameter is : the failed Item(product) at index i, in wrapper format to fetch validation errors from
					 *  third parameter is : the success item at index i, in wrapper format to fetch XeroId from, null here
					 * @return this method will return the Product record mapped with error details from Xero which will be updated after callouts
					 */
					prod = XR_MappingHandler.mapProductsXeroToSf(prod, productsList[i], null);
					wrapper.productList.add(prod);
				}
			}
			//else if response is success reponse
			else{
				//deserialize the response in wrappers created to handle success reponse
				XR_Data.successProductResponse successResponseWrapper = (XR_Data.successProductResponse)JSON.deserialize(response.body,XR_Data.successProductResponse.class);
				//handling bulkified response, adding elements(products) into list to update in salesforce accordingly
				list<XR_Data.Items> productsList = successResponseWrapper.Items;
				//fethcing products one by one to update to salesforce in FCFS manner
				for(Integer i = 0; i < productsList.size(); i++) {
					Product2 prod;
					if(prodList.size() < i+1) {
						prod = new Product2();
					}else {
						prod = prodList[i];
					}
					/*
					 *  it will map Response wrapper to Salesforce Product record
					 *  first parameter is : product salesforce record on Index 0 of productList
					 *  second parameter is : the failed Item(product) at index i, in wrapper format to fetch validation errors from, null here
					 *  third parameter is : the success item at index i, in wrapper format to fetch XeroId from
					 * @return this method will return the Product record mapped with success details from Xero which will be updated after callouts
					 */
					prod = XR_MappingHandler.mapProductsXeroToSf(prod ,null, productsList[i]);
					wrapper.productList.add(prod);
				}
			}
		}
		//return the List<Product2> which are now filled with error/success results and ready to be pushed back in salesforce
		return wrapper.productList;
	}

	/**
	 * wrapper to store result
	 */
	public class ProductResultWrapper
	{
		public List<Product2> productList {get;
			                           set;
		}
		public ProductResultWrapper(){
			productList = new List<Product2>();
		}
	}
}