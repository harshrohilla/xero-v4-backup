public with sharing class XR_CustomerHelper extends XR_AuthHandler
{
	/**
	 * getCustomers get customer details from xero and return wrapper of it
	 * @param       set of account name from related accounts accountNameSet
	 * @return      return list of wrapper containing account details from xero
	 */
	public List<XR_Data.Contact> getCustomersByName(Set<String> accountNameSet){
		if(accountNameSet != null && accountNameSet.size() > 0) {
			//handle the endpoint and embed account names in it
			String whereClause = '';//'IsCustomer==true AND (';
			for(String accountName: accountNameSet) {
				whereClause += 'Name=="' + accountName + '" OR ';
			}
			whereClause = whereClause.removeEnd(' OR ');
			//whereClause += ')';
			//end of endpoint handling
			String endpoint = 'callout:' + XR_Const.customerEndpoint + '?where=' + EncodingUtil.urlEncode(whereClause, 'UTF-8');
			return getCustomers(endpoint);
		}else {
			//if response was null because of any exception, return null and halt process here and log the details in system logs
			LogUtils.log(LoggingLevel.DEBUG, 'getCustomerByName', 'No Account Names passsed: ' + accountNameSet);
			return null;
		}
	}
	public List<XR_Data.Contact> getCustomers(String endPoint){
		//manage header map and put tenant id from getTenantId for callout
		Map<String,String> headerMap = new Map<String,String>();
		headerMap.put('xero-tenant-id', XR_AuthHandler.tenantId);
		if(headerMap.get('xero-tenant-id') != '' || headerMap.get('xero-tenant-id') != null) {
			HTTPHandler handler = new HTTPHandler();
			//doGet callout to fetch contact details from xero as response
			HTTPHandler.Response response = handler.doGet(endpoint, headerMap);
			//provide the response to handleContactResponse class for deserialization and further events
			System.debug('response : '+response);
			return handleContactResponse(response);
		}
		else {
			return null;
		}
	}
	/**
	 * handleContactResponse take response from get response callout, desrialize it and place in wrapper
	 * @param  response      response from callout
	 * @return               return wrapper containing customer details
	 */
	private List<XR_Data.Contact> handleContactResponse(HTTPHandler.Response response){

		if(response != null && response.body != null) {
			//desrialize reponse
			XR_Data.XeroResponse responseWrapper = (XR_Data.XeroResponse)JSON.deserialize(response.body, XR_Data.XeroResponse.class);
			List<XR_Data.Contact> contactWrapperList = responseWrapper.Contacts;
			System.debug('contactWrapperList '+contactWrapperList);
			return contactWrapperList;
		}
		return null;
	}
	/**
	 * doPush this method will take a list of accounts, push them to xero, map response back to accounts and return the list of accounts
	 * @param  accList list of accounts to be pushed to xero
	 * @return         return the list of account mapped with response from xero
	 */
	public List<Account> doPush(List<Account> accList){
		//this wrapper list will carry mapped requests
		List<XR_Data.Contact> contactWrapperList = new List<XR_Data.Contact>();
		//if the account list is not empty
		if (accList.size() > 0) {
			for (Account acc : accList) {
				XR_Data.Contact con = XR_MappingHandler.mapContactSFDCToXero(acc);
				contactWrapperList.add(con);
			}
			//prepare body
			Map<String, List<XR_Data.Contact> > contactMap = new Map<String, List<XR_Data.Contact> >();
			contactMap.put('Contacts', contactWrapperList);
			//prepare header map for callout
			Map<String,String> headerMap = new Map<String,String>();
			headerMap.put('xero-tenant-id', XR_AuthHandler.tenantId);
			//serialize request body for sending in callout
			String requestBody = JSON.serializePretty(contactMap);
			//call HTTP handler
			HTTPHandler handler = new HTTPHandler();
			HTTPHandler.Response response = handler.doPost('callout:'+XR_Const.customerEndpoint, requestBody, headerMap);
			return handlePushResponse(response,accList);
		}
		return null;
	}
	/**
	 * handlePushResponse this method will handle response fetched from pushing Accounts to Xero
	 * @param  response response from xero
	 * @param  accList  List of accounts those were pushed initially to xero, required to handle mappings
	 * @return          return List of accounts mapped with data from xero
	 */
	public List<Account> handlePushResponse(HTTPHandler.Response response, List<Account> accList){
		AccountsResultWrapper wrapper = new AccountsResultWrapper();
		//if response was a success
		if(response.body.contains('"Status": "OK",')) {
			//deserialize the response
			XR_Data.XeroResponse responseWrapper = (XR_Data.XeroResponse)JSON.deserialize(response.body,XR_Data.XeroResponse.class);
			//fetch contacts from response
			list<XR_Data.Contact> contactsList = responseWrapper.Contacts;
			//map accounts with data fetched from response
			for(Integer i = 0; i < contactsList.size(); i++) {
				Account account;
				if(accList.size() < i+1) {
					account = new Account();
				}else {
					account = accList[i];
				}
				//account mapped
				account = XR_MappingHandler.mapContactXeroToSFDC(account,contactsList[i]);
				wrapper.accountList.add(account);
			}
			return wrapper.accountList;
		}else {
			//if there was any problem in sync
			LogUtils.log(LoggingLevel.ERROR, 'Account Sync Error', 'Accounts failed to Sync to Xero. Contact Administrator');
			for(Account acc : accList) {
				acc.Xero_Sync_Status__c = XR_Const.failed;
				acc.Xero_Sync_Details__c = XR_Const.unknownException;
				wrapper.accountList.add(acc);
			}
			return wrapper.accountList;
		}
	}
	/**
	 * wrapper to store result
	 */
	public class AccountsResultWrapper
	{
		public List<Account> accountList {get;
			                          set;
		}
		public AccountsResultWrapper(){
			accountList = new List<Account>();
		}
	}
}