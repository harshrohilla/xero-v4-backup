/**
 * @File Name          : XeroMappingHandler.cls
 * @Description        :
 * @Author             : Gulshan
 * @Group              :
 * @Last Modified By   :
 * @Last Modified On   :
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    06/01/2020      Gulshan                Initial Version
 */
public with sharing class XR_MappingHandler
{
	/**
	 * mapContact_SFDCToXero handles mapping of contact details when syncing from sf to xero
	 * @param                account record for mapping values
	 * @return               return contact wrapper with values from salesforce contact
	 */
	public static XR_Data.Contact mapContactSFDCToXero(Account account){
		XR_Data.Contact con = new XR_Data.Contact();
		con.ContactID = account.Xero_Id__c;
		con.Name = account.Name;
		con.IsCustomer = XR_Const.isCustomer;
		con.IsSupplier = XR_Const.IsSupplier;
		return con;
	}
	/**
	 * mapContact_XeroToSFDC  handles mapping of contact details when fetching from xero and updating to sf
	 * @param  account        account record which is to be updated for fields
	 * @param  contactWrapper contactWrapper containing deserialzed values from xero
	 * @return                return account record to be updated in sf
	 */
	public static Account mapContactXeroToSFDC(Account account, XR_Data.Contact contactWrapper){
		account = account == null ? new Account() : account;
		//if personAccount, map accordingly
		if(account.isPersonAccount == True) {
			//if there is a space, it means the Name contains two or more words to fill in salesforce
			if (contactWrapper.Name.contains(' ')) {
				account.FirstName = contactWrapper.Name.Substring(0,contactWrapper.Name.indexOf(' '));
				account.LastName = contactWrapper.Name.Substring(contactWrapper.Name.indexOf(' '),contactWrapper.Name.length());
			}else {
				//if there is no space, it means its a single word name and should go in LastName to avoid requiredField error
				account.LastName = contactWrapper.Name;
			}
		}else {
			//if its not a person account, simply fill the Name field
			account.Name = contactWrapper.Name;
		}
		account.Xero_Id__c = contactWrapper.ContactID;
		account.Xero_Sync_Status__c = XR_Const.success;
		account.Xero_Sync_Details__c = '';
		return account;
	}
	/**
	 * mapInvoice_XeroToSFDC handles mapping of invoice details when fetching from xero and updating to sf
	 * @param  invoice        invoice record to be fetched from xero
	 * @param  invoiceWrapper invoiceWrapper wrapper containing invoice details from xero
	 * @return                return invoice record to be updated in salesforce
	 */
	public static Opportunity mapInvoiceXeroToSFDC(Opportunity opp, XR_Data.Invoice invoiceWrapper){
		if(invoiceWrapper.HasErrors != null && invoiceWrapper.HasErrors == true) {
			opp.Xero_Sync_Status__c = XR_Const.opportunitySyncFailed;
			String errorMsg = '';
			for(XR_Data.ValidationErrors vError: invoiceWrapper.ValidationErrors) {
				errorMsg = errorMsg + '\n\n' + vError.Message;
			}
			opp.Xero_Sync_Details__c = errorMsg;
		}else {
			opp.Xero_ID__c = invoiceWrapper.InvoiceID;
			opp.Xero_Invoice_Number__c = invoiceWrapper.InvoiceNumber;
			opp.Xero_Sync_Status__c = 'Success';
			opp.Xero_Sync_Details__c = null;
			opp.Xero_Invoice_Status__c = invoiceWrapper.Status;
			opp.Xero_Invoice_Url__c = invoiceWrapper.Url;
			opp.Xero_Sent_To_Contact__c = invoiceWrapper.SentToContact;
		}
		return opp;
	}
	/**
	 * mapInvoice_SFDCToXero handle mapping of invoices when syncing from sf to xero
	 * @param                invoice record to be synced to xero
	 * @return               return wrapper of invoice to be serialized and pushed to xero
	 */
	public static XR_Data.Invoice mapInvoiceSFDCToXero(Opportunity opp){
		XR_Data.Invoice invoiceWrapper = new XR_Data.Invoice();
		invoiceWrapper.InvoiceID = opp.Xero_Id__c;
		invoiceWrapper.InvoiceNumber = opp.Xero_Invoice_Number__c;
		invoiceWrapper.Status = opp.Xero_Invoice_Status__c;
		invoiceWrapper.CurrencyCode = XR_Const.currencyISOCode;
		invoiceWrapper.Date1 = XR_Utils.getDate(opp.CloseDate);
		invoiceWrapper.DueDate = XR_Utils.getDate(opp.Invoice_Due_Date__c);
		//invoiceWrapper.AmountCredited = opp.Amount_Credited__c;
		//invoiceWrapper.AmountDue = opp.Amount;
		//invoiceWrapper.AmountPaid = opp.Amount;
		//invoiceWrapper.CISDeduction = opp.CIS_Deduction__c;
		//invoiceWrapper.CurrencyRate = opp.Currency_Rate__c;
		invoiceWrapper.ExpectedPaymentDate = XR_Utils.getDate(opp.Xero_Expected_Payment_Date__c);
		invoiceWrapper.FullyPaidOnDate = XR_Utils.getDate(opp.Xero_Fully_Paid_On_Date__c);
		//invoiceWrapper.HasAttachments = opp.Has_Attachments__c;
		//invoiceWrapper.LineAmountTypes = opp.Line_Amount_Types__c;
		invoiceWrapper.PlannedPaymentDate = XR_Utils.getDate(opp.Xero_Planned_Payment_Date__c);
		//invoiceWrapper.Reference = opp.Reference__c;
		//invoiceWrapper.SentToContact = opp.Sent_To_Contact__c;
		//invoiceWrapper.SubTotal = opp.Sub_Total__c;
		invoiceWrapper.Total = opp.Amount;
		//invoiceWrapper.TotalTax = opp.Total_Tax__c;
		invoiceWrapper.Type = opp.Xero_Invoice_Type__c;
		//invoiceWrapper.Url = opp.Url__c;
		//invoiceWrapper.TotalDiscount = opp.Total_Discount__c;
		return invoiceWrapper;
	}

	/**
	 * mapLineItem_SFDCToXero handle mapping of invoice line items when syncing from sf to xero
	 * @param                 invoice line item record to be synced to xero
	 * @return                return wrapper of invoice line item to be serialized and pushed to xero
	 */
	public static XR_Data.LineItem mapLineItemSFDCToXero(Sobject line){
		XR_Data.LineItem lineWrapper = new XR_Data.LineItem();
		lineWrapper.Quantity = (Decimal)line.get('Quantity');
		lineWrapper.UnitAmount = (Decimal)line.get('UnitPrice');
		lineWrapper.AccountCode = (String)line.get('Xero_Account_Code__c');
		lineWrapper.Description = (String)line.get('Name');
		//lineWrapper.ItemCode = (String)line.get('Item_Code__c');
		//lineWrapper.DiscountAmount = (Decimal)line.get('Discount_Amount__c');
		lineWrapper.DiscountRate = (Decimal)line.get('Discount');
		lineWrapper.LineAmount = (Decimal)line.get('TotalPrice');
		lineWrapper.TaxType = (String)line.get('Xero_Tax_Type__c');
		lineWrapper.LineItemID = (String)line.get('Xero_Id__c');
		return lineWrapper;
	}
	/**
	 * mapLineItem_XeroToSFDC handle mapping of invoice line items when fetching from xero in sf
	 * @param  line        line item record
	 * @param  lineWrapper lineWrapper containing line item record data
	 * @return             return line item record to be updated in sf
	 */
	public static SObject mapLineItemXeroToSFDC(SObject line, XR_Data.LineItem lineWrapper){
		line.put('Quantity',  lineWrapper.Quantity);
		line.put('UnitPrice',  lineWrapper.UnitAmount);
		line.put('Xero_Account_Code__c',  lineWrapper.AccountCode);
		line.put('Description',  lineWrapper.Description);
		//line.put('Item_Code__c',  lineWrapper.ItemCode);
		//line.put('Discount_Amount__c',  lineWrapper.DiscountAmount);
		line.put('Discount',  lineWrapper.DiscountRate);
		//line.put('Line_Amount__c',  lineWrapper.LineAmount);
		line.put('Xero_Tax_Amount__c',  lineWrapper.TaxAmount);
		line.put('Xero_Tax_Type__c',  lineWrapper.TaxType);
		line.put('Xero_Id__c', lineWrapper.LineItemID);
		return line;
	}/**
	 * mapProductsSfToXero mapping of product when pushed from salesforce
	 * @param  product to be mapped
	 * @return         product wrapper
	 */
	public static XR_Data.Item mapProductsSfToXero(Product2 product){

		XR_Data.Item item = new XR_Data.Item();
		item.code = product.StockKeepingUnit;
		item.name = product.Name;
		item.isSold = product.Xero_Is_Sold__c;
		item.isPurchased = product.Xero_Is_Purchased__c;
		item.description = product.Description;
		//item.purchaseDescription = product
		//item.isTrackedAsInventory = product.Xero_Is_Tracked_As_Inventory__c;
		//item.inventoryAssetAccountCode = product
		//item.quantityOnHand = product.Xero_Quantity_On_Hand__c;
		//item.updatedDateUTC =

		XR_Data.SalesDetails salesDetails = new XR_Data.SalesDetails();
		//check custom setting to sync price or not
		Xero_Settings__c xeroCustomSetting = XR_Utils.fetchXeroSettings();
		//if false, keep price = 0 when syncing to xero
		if (xeroCustomSetting.Sync_Price__c == True) {
			salesDetails.unitPrice = product.PricebookEntries[0].UnitPrice;
		}else {
			salesDetails.unitPrice = 0;
		}
		salesDetails.accountCode = product.Account_Code__c;
		salesDetails.taxType = product.Tax_Type__c;

		//XR_Data.PurchaseDetails purchaseDetails = new XR_Data.PurchaseDetails();
		//purchaseDetails.unitPrice = null;
		//purchaseDetails.COGSAccountCode = null;
		//purchaseDetails.TaxType = product.Tax_Type__c;

		//XR_Data.ValidationErrors valError = new XR_Data.ValidationErrors();
		//valError.Message =

		item.SalesDetails = salesDetails;
		//item.PurchaseDetails = purchaseDetails;

		return item;
	}
	//map product sync response from xero to salesforce
	//two types of items are taken in params because xero sends different formats of json for error and success
	public static Product2 mapProductsXeroToSf(Product2 product, XR_Data.Elements errorItem,XR_Data.Items successItem){
		//if sync fails
		if(errorItem != null) {
			product.Xero_Id__c = '';
			product.Xero_Sync_Status__c = XR_Const.failed;
			String errorMsg = '';
			for(XR_Data.ValidationErrors vError : errorItem.ValidationErrors) {
				errorMsg = errorMsg + '\n' + vError.Message;
			}
			product.Xero_Sync_Details__c = errorMsg;
			//if sync succeeds
		} else{
			product.Xero_Id__c = successItem.ItemID;
			product.Xero_Sync_Status__c = XR_Const.success;
			product.Xero_Sync_Details__c = '';
		}
		return product;
	}
}